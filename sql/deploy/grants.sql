grant select,update,insert on saturn.SZRSORM to MUWS_GEN_RL;

grant select,update,insert,delete on saturn.SZRSORA to MUWS_GEN_RL;

grant select,update,insert on saturn.SZRSORB to MUWS_GEN_RL;

grant select,update,insert,delete on saturn.SZRSORR to MUWS_GEN_RL;

grant select,update,insert,delete on saturn.SZRSORD to MUWS_GEN_RL;

grant select on saturn.smrprle to MUWS_GEN_RL;

grant select on saturn.scrlevl to MUWS_GEN_RL;

grant select on saturn.stvattr to MUWS_GEN_RL;

grant execute on baninst1.fz_get_previous_term to MUWS_GEN_RL;

grant execute on baninst1.fz_get_term to MUWS_GEN_RL;

grant execute on baninst1.fz_get_next_term to MUWS_GEN_RL;

grant select on saturn.szrsorm to uis_developer_saturn_q;

grant select on saturn.szrsora to uis_developer_saturn_q;

grant select on saturn.szrsorb to uis_developer_saturn_q;

grant select on saturn.szrsorr to uis_developer_saturn_q;