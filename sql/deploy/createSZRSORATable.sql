BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE SZRSORA';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/


-- drop sequence SATURN.SZRSORA_seq;
--
-- create sequence SATURN.SZRSORA_seq;

CREATE TABLE SATURN.SZRSORA
 (
   "SZRSORA_PIDM" NUMBER(8,0) NOT NULL ENABLE,
   "SZRSORA_TERM_CODE" VARCHAR2(6 CHAR) NOT NULL ENABLE,
   "SZRSORA_ACTION_TRIGGER" VARCHAR2(20 CHAR),
   "SZRSORA_ACTION_DATE" DATE,
   "SZRSORA_ACTION" VARCHAR2(10 CHAR),
   "SZRSORA_MESSAGE_ID" VARCHAR2(30 CHAR)
);

COMMENT ON COLUMN "SATURN"."SZRSORA"."SZRSORA_PIDM" IS 'Internal identification number of person.';
COMMENT ON COLUMN "SATURN"."SZRSORA"."SZRSORA_TERM_CODE" IS 'This field identifies the term code referenced in the Catalog, Recruiting, Admissions, Gen. Student, Registration, Student Billing and Acad. Hist. Modules. Reqd. value: 999999 - End of Time.';
COMMENT ON COLUMN "SATURN"."SZRSORA"."SZRSORA_ACTION" IS 'This field identifies the next action for the student if the student has not provided the state information for this term.For example sending an email or notifying through WAS.';
COMMENT ON COLUMN "SATURN"."SZRSORA"."SZRSORA_ACTION_TRIGGER" IS 'This field identifies the trigger which caused teh next action.';
COMMENT ON COLUMN "SATURN"."SZRSORA"."SZRSORA_ACTION_DATE" IS 'This field identifies the date at which next action is taken for this student';
COMMENT ON COLUMN "SATURN"."SZRSORA"."SZRSORA_MESSAGE_ID" IS 'This field identifies the message ID returned after the email notification is sent to student ';

CREATE INDEX "SATURN"."SZRSORA_PIDM_INDEX" ON "SATURN"."SZRSORA" ("SZRSORA_PIDM");
CREATE INDEX "SATURN"."SZRSORA_TERM_CODE_INDEX" ON "SATURN"."SZRSORA" ("SZRSORA_TERM_CODE");

CREATE OR REPLACE PUBLIC SYNONYM SZRSORA for SATURN.SZRSORA;


/