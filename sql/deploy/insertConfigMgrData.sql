  set define off;
  SET SQLBLANKLINES ON;

  delete from cm_config where config_application = 'StateOfResidence'
                              and config_key not in ('wasClientUpdateUrl', 'wasClientGetUrl');


--   insert into cm_config (config_application, config_key,
--         config_data_type, config_data_structure, config_category,
--         config_desc, config_value, activity_date, activity_user)
--     values ('StateOfResidence', 'wasClientUpdateUrl',
--         'text', 'scalar', 'Internal Config',
--         'Base URL for updating review status in WAS for messaging',
--         'http://ws/sofr/updateWAS.php?extapp=StateOfResidenceInfo&ss=bob',
--         sysdate, 'doej');

-- Need to add this to local config for get profile to work
--   insert into cm_config (config_application, config_key,
--         config_data_type, config_data_structure, config_category,
--         config_desc, config_value, activity_date, activity_user)
--     values ('StateOfResidence', 'wasClientGetUrl',
--         'text', 'scalar', 'Internal Config',
--         'Base URL for getting info required status in WASas',
--         'https://wastest.miamioh.edu/perl/was/extappdata?extapp=StateOfResidenceInfo&ss=state',
--         sysdate, 'doej');



  insert into cm_config (config_application, config_category, config_key,
                         config_value,
                         config_desc,
                         config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'main.appTitle',
         'Where will you be physically located while participating in classes?',
          '',
          'text', 'scalar', sysdate, 'doej');
  insert into cm_config (config_application, config_category, config_key,
                          config_value,
                          config_desc,
                          config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'main.introMessage',
        '<p>You are receiving this message because you have registered for one or more online course(s). Federal and state regulations provide that Miami University can only offer online courses in those states in which it is authorized to do so.	In order to comply with these requirements it is necessary for Miami to determine where you will be physically located while taking the online course(s). Please select the state from the drop down list that accurately reflects where you will be physically located while participating in this course.</p>

<p>If you have any questions please contact us at:</p>

<p><strong>Oxford</strong>:  eLearningMiami@miamioh.edu or call 513-529-6068<br />
<strong>Regionals</strong>: Elearning@miamioh.edu or call 513-217-4003</p>

<p>Please be aware that Miami University is not authorized to offer all of our programs in every state. For additional information about state authorization and the programs that Miami University is authorized to offer outside of the state of Ohio please visit our state <a href="https://miamioh.edu/academics/elearning/admission/state-authorization/index.html">authorization webpage.</p>
',
        '',
        'text', 'scalar', sysdate, 'doej');
  insert into cm_config (config_application, config_category, config_key,
                          config_value,
                          config_desc,
                          config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'main.noEnrollmentsMsg',
        '<p>You do not have any qualified enrollments at this time.</p><p>If you recently enrolled in a qualifying course, we likely have not yet processed your enrollment. Please try again later.</p>',
        '',
        'text', 'scalar', sysdate, 'doej');
  insert into cm_config (config_application, config_category, config_key,
                          config_value,
                          config_desc,
                          config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'termLabel',
          'Term',
          '',
          'text', 'scalar', sysdate, 'doej');
  insert into cm_config (config_application, config_category, config_key,
                          config_value,
                          config_desc,
                          config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'courseLabel',
          'List of Courses Enrolled',
          '',
          'text', 'scalar', sysdate, 'doej');
  insert into cm_config (config_application, config_category, config_key,
                          config_value,
                          config_desc,
                          config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'countryLabel',
         'Please select your Country',
         '',
        'text', 'scalar', sysdate, 'doej');
  insert into cm_config (config_application, config_category, config_key,
                          config_value,
                          config_desc,
                          config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'stateLabel',
         'Please select your State',
          '',
          'text', 'scalar', sysdate, 'doej');
  insert into cm_config (config_application, config_category, config_key,
                            config_value,
                            config_desc,
                            config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'submitLabel',
         'Submit',
          '',
          'text', 'scalar', sysdate, 'doej');
  insert into cm_config (config_application, config_category, config_key,
                          config_value,
                          config_desc,
                          config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'ReadOnlycountryLabel',
         ' Selected Country',
         '',
        'text', 'scalar', sysdate, 'doej');
  insert into cm_config (config_application, config_category, config_key,
                          config_value,
                          config_desc,
                          config_data_type, config_data_structure, activity_date, activity_user)
   values ('StateOfResidence', 'StudentResidence', 'ReadOnlystateLabel',
         ' Selected State',
          '',
          'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                          config_value,
                          config_desc,
                          config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'SendNotification', 'email1.subject',
         'Action Required/Eligibility',
          '',
          'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                          config_value,
                          config_desc,
                          config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'SendNotification', 'email1.body',
         'Dear {firstName},

You are receiving this email because you have registered for one or more online course(s). Federal and state regulations provide that Miami University can only offer online courses in those states in which it is authorized to do so.  In order to comply with these requirements it is necessary for Miami to determine where you will be physically located while taking the online course(s). Please login to https://miamioh.edu/sofr where you will select the state from the drop down list where you will be physically located while participating in this course. Failure to submit this information before the start of the term will impact your ability to login to Miami services. If you have any questions please contact us at: eLearningMiami@miamioh.edu (Oxford) or eLearning@miamioh.edu (Regionals).

Please be aware that Miami University is not authorized to offer all of our programs in every state. For additional information about state authorization and the programs that Miami University is authorized to offer outside of the state of Ohio please visit our state authorization webpage at https://miamioh.edu/academics/elearning/admission/state-authorization/index.html.',
          '',
          'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                          config_value,
                          config_desc,
                          config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'SendNotification', 'email2.subject',
         'Reminder Action Required/Eligibility',
          '',
          'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                          config_value,
                          config_desc,
                          config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'SendNotification', 'email2.body',
         'This is a reminder to please login to https://miamioh.edu/sofr where you will select the state from the drop down list where you will be physically located while participating in this course.

Federal and state regulations provide that Miami University can only offer online courses in those states in which it is authorized to do so.  In order to comply with these requirements it is necessary for Miami to determine where you will be physically located while taking the online course(s). Failure to submit this information before the start of the term will impact your ability to login to Miami services. If you have any questions please contact us at: eLearningMiami@miamioh.edu (Oxford) or eLearning@miamioh.edu (Regionals).

Please be aware that Miami University is not authorized to offer all of our programs in every state. For additional information about state authorization and the programs that Miami University is authorized to offer outside of the state of Ohio please visit our state authorization webpage at https://miamioh.edu/academics/elearning/admission/state-authorization/index.html.',
          '',
          'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                          config_value,
                          config_desc,
                          config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'SendNotification', 'email.subject',
         'URGENT Action Required/Eligibility',
          '',
          'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                          config_value,
                          config_desc,
                          config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'SendNotification', 'email.body',
         'You are receiving this email because you have registered for one or more online course(s). Federal and state regulations provide that Miami University can only offer online courses in those states in which it is authorized to do so.  In order to comply with these requirements it is necessary for Miami to determine where you will be physically located while taking the online course(s). Based on the term start date please immediately login to https://miamioh.edu/sofr where you will select the state from the drop down list where you will be physically located while participating in this course. Failure to submit this information before the start of the term will impact your ability to login to Miami services. If you have any questions please contact us at: eLearningMiami@miamioh.edu (Oxford) or eLearning@miamioh.edu (Regionals).


Please be aware that Miami University is not authorized to offer all of our programs in every state. For additional information about state authorization and the programs that Miami University is authorized to offer outside of the state of Ohio please visit our state authorization webpage at https://miamioh.edu/academics/elearning/admission/state-authorization/index.html.',
          '',
          'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                          config_value,
                          config_desc,
                          config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'ActionTrigger', 'triggerValueBeforeTermStart',
         '-10 days',
          'This value is used to calculate when the WAS login message should be prompted before the term start date',
          'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                          config_value,
                          config_desc,
                          config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'ActionTrigger', 'triggerValueBetweenEmails',
         '+10 days',
          'This value is used to calculate the number of days between consecutive email notification sent to students.',
          'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                          config_value,
                          config_desc,
                          config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'ActionTrigger', 'triggerValueAfterTermStart',
         '+14 days',
          'This value is used to calculate the date from which students are not allowed to update the state of residence.',
          'text', 'scalar', sysdate, 'doej');
  insert into cm_config (config_application, config_category, config_key,
                          config_value,
                          config_desc,
                          config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'ActionTrigger', 'beginTerm',
         '201710',
          '<p>This value is used as begin term for the Student enrollment report to be generated for the Mentis.</p>',
          'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                          config_value,
                          config_desc,
                          config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'ruleFailureMessage',
         '<p>You have registered for an online course(s), and you have indicated that you will be physically taking the course in a state in which Miami University may not be authorized to offer the course. Please contact us at:</p>

<p><strong>Oxford</strong>:  eLearningMiami@miamioh.edu or call 513-529-6068<br />
<strong>Regionals</strong>: Elearning@miamioh.edu or call 513-217-4003</p>

<p>so that we can discuss how this will impact your ability to take this course.</p>',
          'This value is used to display when student has chosen a country/state not allowed to take the program/major.',
          'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                          config_value,
                          config_desc,
                          config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'popupMsg',
         'You are receiving this message because you have registered for one or more online course(s). Federal and state regulations provide that Miami University can only offer online courses in those states in which it is authorized to do so.',
          'This value is used in the pop up window displayed when student has chosen a country/state not allowed to take the program/major.',
          'text', 'scalar', sysdate, 'doej');
  insert into cm_config (config_application, config_category, config_key,
                            config_value,
                            config_desc,
                            config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'okLabel',
         'Ok',
          '',
          'text', 'scalar', sysdate, 'doej');
  insert into cm_config (config_application, config_category, config_key,
                          config_value,
                          config_desc,
                          config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'programDescriptionLabel',
          'Enrolled Program:',
          '',
          'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                          config_value,
                          config_desc,
                          config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'ruleFailureContactGeneral',
          'Click on below webpage link to get the list of contacts',
          '',
          'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                          config_value,
                          config_desc,
                          config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'ruleFailureContactRegionalCampus',
          'Please contact regional campus',
          '',
          'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                          config_value,
                          config_desc,
                          config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'ruleFailureContactNonRegionalCampus',
          'Please contact non regional campus',
          '',
          'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'BannerID.label',
        'Banner ID',
        '',
        'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'studentProfileInfo.label',
        'Student Profile Information',
        '',
        'text', 'scalar', sysdate, 'doej');
  insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'studentProfileInfo.profileCheckboxLabel',
        'Resolve:',
        '',
        'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'studentProfileInfo.profileCommentLabel',
        'Comment:',
        '',
        'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'studentProfileInfo.profileSubmitLabel',
        'Update Status',
        '',
        'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'studentProfileInfo.profileUniqueIDLabel',
        'Unique ID:',
        '',
        'text', 'scalar', sysdate, 'doej');


  insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'studentName',
        'Student Name:',
        '',
        'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'profileStatusReviewText',
        'Please review the student information and resolve it.',
        '',
        'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                         config_value,
                         config_desc,
                         config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'main.adminAppTitle',
         'State of Residence Admin App',
          '',
          'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                         config_value,
                         config_desc,
                         config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'profileTermLabel',
         'Term Code:',
          '',
          'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                         config_value,
                         config_desc,
                         config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'postRuleFormLabel',
         'Add New Rule Here:',
          '',
          'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                         config_value,
                         config_desc,
                         config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'ruletypeLabel',
         'RuleType',
          '',
          'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                         config_value,
                         config_desc,
                         config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'attributeLabel',
         'Attribute',
          '',
          'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                         config_value,
                         config_desc,
                         config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'allowedstatesLabel',
         'Allowed States',
          '',
          'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                         config_value,
                         config_desc,
                         config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'rulevalueLabel',
         'Rule Value',
          '',
          'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                         config_value,
                         config_desc,
                         config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'addLabel',
         'Add New',
          '',
          'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                         config_value,
                         config_desc,
                         config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'searchTermLabel',
         'UniqueID',
          '',
          'text', 'scalar', sysdate, 'doej');


  insert into cm_config (config_application, config_category, config_key,
                         config_value,
                         config_desc,
                         config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'adminStatusLabel',
         'Admin Status',
          '',
          'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                         config_value,
                         config_desc,
                         config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'adminInstructions',
         '<p>Rules currently apply only to program and/or major of the student. Also note that you must use the Banner codes associated with the programs and majors.</p>',
          '',
          'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                         config_value,
                         config_desc,
                         config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'SendNotification', 'email.fromAddress',
         'StateofResidence@miamioh.edu',
          '',
          'text', 'scalar', sysdate, 'doej');
  
  insert into cm_config (config_application, config_category, config_key,
                         config_value,
                         config_desc,
                         config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'SendNotification', 'email.devTeam',
         'duit-soldel42@miamioh.edu',
          '',
          'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                         config_value,
                         config_desc,
                         config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'SendNotification', 'confirmation.subject',
         'Online State of Residence Confirmation',
          '',
          'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                         config_value,
                         config_desc,
                         config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'SendNotification', 'confirmation.body',
         'Thank you for submitting your planned state of residence during the time you are taking on-line, hybrid or IDVL courses with Miami University. Your submission has been recorded.

{termInfo}

If your state of residence changes, please return to http://miamioh.edu/sofr to update your information.
',
          '',
          'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                         config_value,
                         config_desc,
                         config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'submitConfirmationMsg',
         'Thank you for submitting your data.',
          '',
          'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                         config_value,
                         config_desc,
                         config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'alreadySubmittedMsg',
         'Important: <em>You have already submitted data for this term. The submit button will only be available if you change your previous selections.</em>',
          '',
          'text', 'scalar', sysdate, 'doej');

  insert into cm_config (config_application, config_category, config_key,
                         config_value,
                         config_desc,
                         config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'termClosedMsg',
         'Important: <em>This term has passed the point at which you can change your state selection.</em>',
          '',
          'text', 'scalar', sysdate, 'doej');
  insert into cm_config (config_application, config_category, config_key,
                         config_value,
                         config_desc,
                         config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'ImportantText',
         'Important:',
          '',
          'text', 'scalar', sysdate, 'doej');
  insert into cm_config (config_application, config_category, config_key,
                         config_value,
                         config_desc,
                         config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'priorityRuleLabel',
         'Priority(based on Rules):',
          '',
          'text', 'scalar', sysdate, 'doej'); 
          
  insert into cm_config (config_application, config_category, config_key,
                         config_value,
                         config_desc,
                         config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'profilePriorityLabel',
         'Priority(stored in  database):',
          '',
          'text', 'scalar', sysdate, 'doej');      
            
 insert into cm_config (config_application, config_category, config_key,
                         config_value,
                         config_desc,
                         config_data_type, config_data_structure, activity_date, activity_user)
  values ('StateOfResidence', 'StudentResidence', 'WASStatusLabel',
         'WAS Status:',
          '',
          'text', 'scalar', sysdate, 'doej');
          
          
   




