BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE SZRSORM';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/

-- drop sequence SATURN.SZRSORM_seq;
--
-- create sequence SATURN.SZRSORM_seq;

CREATE TABLE SATURN.SZRSORM
 (
   "SZRSORM_PIDM" NUMBER(8,0) NOT NULL ENABLE,
   "SZRSORM_TERM_CODE" VARCHAR2(6 CHAR) NOT NULL ENABLE,
   "SZRSORM_STATUS" VARCHAR2(30 CHAR) NOT NULL ENABLE,
   "SZRSORM_STAT_CODE" VARCHAR2(3 CHAR),
   "SZRSORM_NATN_CODE" VARCHAR2(5 CHAR),
   "SZRSORM_LASTSEEN_DATE" DATE NOT NULL ENABLE,
   "SZRSORM_COMMENT" VARCHAR2(30 CHAR),
   "SZRSORM_PRIORITY" NUMBER(1) NOT NULL ENABLE,
   "SZRSORM_ADMIN_STATUS" VARCHAR2(10 CHAR),
   CONSTRAINT PK_SZRSORM PRIMARY KEY (SZRSORM_PIDM, SZRSORM_TERM_CODE)
);

COMMENT ON COLUMN "SATURN"."SZRSORM"."SZRSORM_PIDM" IS 'Internal identification number of person.';
COMMENT ON COLUMN "SATURN"."SZRSORM"."SZRSORM_TERM_CODE" IS 'This field identifies the term code referenced in the Catalog, Recruiting, Admissions, Gen. Student, Registration, Student Billing and Acad. Hist. Modules. Reqd. value: 999999 - End of Time.';
COMMENT ON COLUMN "SATURN"."SZRSORM"."SZRSORM_STATUS" IS 'This field identifies the status of student, if the student has been provided state/country information or not. It can take pending, completed, cancelled.';
COMMENT ON COLUMN "SATURN"."SZRSORM"."SZRSORM_STAT_CODE" IS 'This field maintains the state associated with the address of student when taking this term.';
COMMENT ON COLUMN "SATURN"."SZRSORM"."SZRSORM_NATN_CODE" IS 'This field maintains the nation/country associated with the address of student when taking this term.';
COMMENT ON COLUMN "SATURN"."SZRSORM"."SZRSORM_LASTSEEN_DATE" IS 'This field defines the most current date a record is added or changed. ';
COMMENT ON COLUMN "SATURN"."SZRSORM"."SZRSORM_PRIORITY" IS 'This field identifies if the student has to be contacted by Miami for the state of residence information based on priority(5 being low and 1 being high)';
COMMENT ON COLUMN "SATURN"."SZRSORM"."SZRSORM_ADMIN_STATUS" IS 'This field defines the status of the student, based on the Admin app for State of Residence project';

CREATE INDEX "SATURN"."SZRSORM_PIDM_INDEX" ON "SATURN"."SZRSORM" ("SZRSORM_PIDM");
CREATE INDEX "SATURN"."SZRSORM_TERM_CODE_INDEX" ON "SATURN"."SZRSORM" ("SZRSORM_TERM_CODE");


CREATE OR REPLACE PUBLIC SYNONYM SZRSORM for SATURN.SZRSORM;

/
-- create index SATURN.SZRSORM_pidm on index SATURN.SZRSORM (SZRSORM_PIDM);
--
-- create index SATURN.SZRSORM_termcode on SATURN.SZRSORM (SZRSORM_TERMCODE);
--
-- grant select,insert,update,delete on SATURN.SZRSORM to MUWS_GEN_RL;
-- grant select,insert,update,delete on SATURN.SZRSORM to MUWS_SEC_RL;
-- grant select on SATURN.SZRSORM to MUWS_GEN_RL;
-- grant select on SATURN.SZRSORM to MUWS_SEC_RL;
