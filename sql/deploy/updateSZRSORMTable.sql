set define off;
  SET SQLBLANKLINES ON;

--    Setting status to complete for all the students who have provided country/state information and status is still pending.
    update saturn.szrsorm set szrsorm_status = 'complete' where szrsorm_status = 'pending' and szrsorm_natn_code is not null;