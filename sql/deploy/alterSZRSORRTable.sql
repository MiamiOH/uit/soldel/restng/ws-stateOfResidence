ALTER TABLE SATURN.SZRSORR
    ADD SZRSORR_ACTIVITY_DATE DATE 
    ADD SZRSORR_USER_ID VARCHAR2 (30 CHAR);


COMMENT ON COLUMN "SATURN"."SZRSORR"."SZRSORR_ACTIVITY_DATE" IS 'This field identifies the date at which any activity is made to the row';
COMMENT ON COLUMN "SATURN"."SZRSORR"."SZRSORR_USER_ID" IS 'This field identifies the user who has made the change';