BEGIN
  EXECUTE IMMEDIATE 'DROP TABLE saturn.SZRSORD';
EXCEPTION
WHEN OTHERS THEN
  IF SQLCODE != -942 THEN
    RAISE;
  END IF;
END;
/
CREATE TABLE SATURN.SZRSORD AS
  (SELECT szrsorm_pidm       AS pidm,
      szrsorm_term_code      AS term_code,
      szrsorm_status         AS status,
      szrsorm_stat_code      AS state_code,
      szrsorm_natn_code      AS nation_code,
      szrsorm_lastseen_date  AS lastseen_date,
      szrsorm_priority       AS priority,
      stvterm_desc           AS term_desc,
      sfrstcr_crn            AS crn,
      scrlevl_levl_code      AS level_code,
      SFRSTCR_CREDIT_HR      AS credit,
      ssbsect_subj_code      AS course_subject,
      ssbsect_crse_numb      AS course_number,
      trim(ssbsect_seq_numb) AS course_section,
      scbcrse_title          AS course_title,
      szbuniq_banner_id      AS banner_id,
      szbuniq_unique_id      AS unique_id,
      spriden_first_name     AS first_name,
      spriden_last_name      AS last_name,
      ssrattr_attr_code      AS instructional_method,
      ssbsect_camp_code      AS campus_code,
      stvcamp_desc           AS campus_desc,
      sgbstdn_program_1      AS program_code_1,
      sgbstdn_program_2      AS program_code_2,
      sgbstdn_majr_code_1    AS major_code_1,
      sgbstdn_majr_code_2    AS major_code_2,
      sgbstdn_majr_code_1_2  AS major_code_1_2,
      sgbstdn_majr_code_2_2  AS major_code_2_2,
      szrsorm_admin_status   AS admin_status,
      szrsorm_comment        AS status_comment
    FROM szrsorm,
      sfrstcr,
      ssbsect,
      scbcrse,
      stvterm,
      szbuniq,
      spriden,
      ssrattr,
      stvcamp,
      sgbstdn,
      scrlevl
    WHERE sfrstcr_pidm    = szrsorm_pidm
    AND sfrstcr_term_code = szrsorm_term_code
    AND ssbsect_crn       = sfrstcr_crn
    AND ssbsect_term_code = szrsorm_term_code
    AND scbcrse_subj_code = ssbsect_subj_code
    AND scbcrse_crse_numb = ssbsect_crse_numb
    AND scrlevl_crse_numb = scbcrse_crse_numb
    AND scrlevl_subj_code = scbcrse_subj_code
    AND stvterm_code      = szrsorm_term_code
    AND szbuniq_pidm      = szrsorm_pidm
    AND spriden_pidm      = szrsorm_pidm
    AND ssrattr_term_code = szrsorm_term_code
    AND ssrattr_crn       = sfrstcr_crn
    AND stvcamp_code      = ssbsect_camp_code
    AND sgbstdn_pidm      = szrsorm_pidm
    and szrsorm_status = 'x'
  );
/
CREATE INDEX "SATURN"."SZRSORD_PIDM_INDEX" ON "SATURN"."SZRSORD"
  (
    "PIDM"
  );
CREATE INDEX "SATURN"."SZRSORD_TERM_CODE_INDEX" ON "SATURN"."SZRSORD"
  (
    "TERM_CODE"
  );
CREATE OR REPLACE PUBLIC SYNONYM SZRSORD FOR SATURN.SZRSORD;
  /