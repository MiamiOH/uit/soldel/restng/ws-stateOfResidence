-- Updates any addresses with spraddr_from_date of 01-DEC-2020 to 18-JAN-2021
-- This fixes a problem where spraddr_from_date was manually entered
UPDATE spraddr
SET spraddr_from_date = to_date('18-JAN-21', 'DD-MON-YY')
WHERE 
    trunc(spraddr_from_date) = trunc(to_date('01-DEC-20', 'DD-MON-YY'));

COMMIT;