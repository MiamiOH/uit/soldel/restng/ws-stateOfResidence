set define off;
  SET SQLBLANKLINES ON;

   delete from cm_config where config_key in ('wasClientUpdateUrl', 'wasClientGetUrl');
                              
   insert into cm_config (config_application, config_key,
         config_data_type, config_data_structure, config_category,
         config_desc, config_value, activity_date, activity_user)
     values ('StateOfResidence', 'wasClientUpdateUrl',
         'text', 'scalar', 'Internal Config',
         'Base URL for updating review status in WAS for messaging',
         'http://ws/sofr/updateWAS.php?extapp=StateOfResidenceInfo&ss=bob',
         sysdate, 'doej');

-- Need to add this to local config for get profile to work. Note that Was url is pointimg to test.
   insert into cm_config (config_application, config_key,
         config_data_type, config_data_structure, config_category,
         config_desc, config_value, activity_date, activity_user)
     values ('StateOfResidence', 'wasClientGetUrl',
         'text', 'scalar', 'Internal Config',
         'Base URL for getting info required status in WASas',
         'https://wastest.miamioh.edu/perl/was/extappdata?extapp=StateOfResidenceInfo&ss=state',
         sysdate, 'doej');