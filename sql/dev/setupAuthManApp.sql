declare

  v_authman_app_name varchar2(64) := null;
  v_authman_module_name varchar2(64) := null;
  v_authman_entity_name varchar2(64) := null;
  v_authman_grantkey varchar2(64) := null;

begin

  v_authman_app_name := 'WebServices';
  v_authman_entity_name := 'SOFR_WS_USER';

  v_authman_module_name := 'StateOfResidence-Action';
  v_authman_grantkey := 'update';
  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                         v_authman_grantkey, 'doej');

  v_authman_module_name := 'StateOfResidence-Enrollment';
  v_authman_grantkey := 'view';
  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                           v_authman_grantkey, 'doej');

  v_authman_module_name := 'StateOfResidence-Profile';
  v_authman_grantkey := 'view';
  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                             v_authman_grantkey, 'doej');

  v_authman_grantkey := 'update';
    createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                               v_authman_grantkey, 'doej');

  v_authman_module_name := 'StateOfResidence-Report';
  v_authman_grantkey := 'view';
  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                               v_authman_grantkey, 'doej');

  v_authman_grantkey := 'create';
  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                               v_authman_grantkey, 'doej');

  v_authman_grantkey := 'update';
  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                               v_authman_grantkey, 'doej');

  v_authman_module_name := 'StateOfResidence-Course';
  v_authman_grantkey := 'view';
  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                               v_authman_grantkey, 'doej');

  v_authman_module_name := 'StateOfResidence-Rule';
  v_authman_grantkey := 'view';
  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                               v_authman_grantkey, 'doej');
  v_authman_module_name := 'StateOfResidence-Rule';
  v_authman_grantkey := 'update';
  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                               v_authman_grantkey, 'doej');
  v_authman_module_name := 'StateOfResidence-Rule';
  v_authman_grantkey := 'create';
  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                               v_authman_grantkey, 'doej');
  v_authman_module_name := 'StateOfResidence-Rule';
  v_authman_grantkey := 'delete';
  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                               v_authman_grantkey, 'doej');

  v_authman_module_name := 'StateOfResidence-TermStatus';
  v_authman_grantkey := 'view';
  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                               v_authman_grantkey, 'doej');
  end;
  /