BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE stateResidence_test_update';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/
  CREATE TABLE stateResidence_test_update
   (
    stateResidence_uid varchar2(8),
    stateResidence_app varchar2(64),
    stateResidence_infoRequired varchar2(2),
    stateResidence_complete varchar2(2),
    stateResidence_completed_date varchar2(24)
   ) ;