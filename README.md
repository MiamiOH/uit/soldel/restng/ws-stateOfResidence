# State Of Residence RESTng Web Service

## Docker Setup

This is a RESTng 2.0 web service, so development follows the typical RESTng documented here: https://wstest.apps.miamioh.edu/api/docs/docker/projects/.

Your swagger url will be https://localhost/swagger-ui/ if you use the docker set up from the instructions.

On the RESTng docker container change to the /var/www/restng/config directory.
Create the following files:

- autoloaders.yaml
- datasources.yaml
- providers.yaml
- services.yaml

## Autoloader File
autoload: /var/www/restng/service/ws-stateOfResidence/vendor/autoload.php

## Providers File
resources: /var/www/restng/service/ws-stateOfResidence/resources.php

## Datasource File
````
MUWS_SEC_PROD:
  type: OCI8
  user: MUWS_SEC
  password: 
  database: DEVL
  port:
  connect_type:
  host:
````


## Services File
````
services:
  MU\BannerIdFactory:
    class: \MiamiOH\RESTng\Service\Extension\BannerIdFactory
    description: Provides Banner ID object factory.
    set:
      database:
        type: service
        name: APIDatabaseFactory
  MU\BannerUtil:
    class: \MiamiOH\RESTng\Service\Extension\BannerUtil
    description: Provides utility functions for working with Banner data.
    set:
      bannerIdFactory:
        type: service
        name: MU\BannerIdFactory
````