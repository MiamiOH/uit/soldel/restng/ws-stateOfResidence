<?php

namespace MiamiOH\StateOfResidenceWebService\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class AdminStatusResourceProvider extends ResourceProvider
{
    private $classPath = 'MiamiOH\StateOfResidenceWebService\Services';

    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'StateOfResidence.AdminStatus',
            'type' => 'object',
            'properties' => array(
                'pidm' => array(
                    'type' => 'string',
                ),
                'termCode' => array(
                    'type' => 'string',
                ),
                'priority' => array(
                    'type' => 'number',
                ),
                'adminStatus' => array(
                    'type' => 'string',
                ),
                'comment' => array(
                    'type' => 'string',
                ),
            ),
        ));

        $this->addDefinition(array(
            'name' => 'StateOfResidence.AdminStatus.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/StateOfResidence.AdminStatus'
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'StateOfResidence.AdminStatusService',
            'class' => $this->classPath.'\AdminStatusService',
            'description' => 'Update the information for admin status',
            'set' => array(
                'admin' => array('type' => 'service', 'name' => 'StateOfResidence.AdminStatus'),
                'bannerUtil' => array('type' => 'service', 'name' => 'MU\BannerUtil'),
            ),
        ));

        $this->addService(array(
            'name' => 'StateOfResidence.AdminStatus',
            'class' => $this->classPath.'\AdminStatus',
            'description' => 'Provide the information for Term',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'update',
            'name' => 'stateOfResidence.adminStatus.v1',
            'description' => 'Admin status information',
            'tags' => array('StateOfResidence'),
            'pattern' => '/stateOfResidence/adminStatus/:muid',
            'service' => 'StateOfResidence.AdminStatusService',
            'method' => 'updateAdminStatus',
            'params' => array(
                'muid' => array('description' => 'A student uniqueid', 'alternateKeys' => ['uniqueId', 'pidm']),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array(
                        'application' => 'WebServices',
                        'module' => 'StateOfResidence-AdminStatus',
                        'key' => 'update'
                    ),
                )
            ),
            'body' => array(
                'description' => 'Admin status information to be updated',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/StateOfResidence.AdminStatus.Collection',
                )
            ),
        ));
    }

    public function registerOrmConnections(): void
    {
    }
}
