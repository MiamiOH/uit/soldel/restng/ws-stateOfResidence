<?php

namespace MiamiOH\StateOfResidenceWebService\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;


class ConfigResourceProvider extends ResourceProvider
{

    private $classPath = 'MiamiOH\StateOfResidenceWebService\Services';

    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'stateOfResidence.Config.Item',
            'type' => 'object',
            'properties' => array(
                'key' => array(
                    'type' => 'string',
                ),
                'value' => array(
                    'type' => 'string',
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'stateOfResidence.Config.Category',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/stateOfResidence.Config.Item'
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'StateOfResidenceConfigService',
            'class' => $this->classPath.'\ConfigService',
            'description' => 'resources for the state pf residence config service',
            'set' => array(
                'config' => array('type' => 'service', 'name' => 'StateOfResidenceConfig'),
            ),
        ));

        $this->addService(array(
            'name' => 'StateOfResidenceConfig',
            'class' => $this->classPath.'\Config',
            'description' => 'REST resources for the state of residence config service',
            'set' => array(
                'configuration' => array('type' => 'service', 'name' => 'APIConfiguration'),
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
            ),
        ));
    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'stateOfResidence.config.v1',
            'description' => 'State of Residence configuration resource',
            'tags' => array('StateOfResidence'),
            'pattern' => '/stateOfResidence/config/v1',
            'service' => 'StateOfResidenceConfigService',
            'method' => 'getConfiguration',
            'options' => array(
                'category' => array(
                    'required' => true,
                    'enum' => [
                        'uiString',
                        'test',
                        'SendNotification',
                        'ActionTrigger',
                        'CurrentTerm',
                        'programDesc',
                        'majorDesc'
                    ],
                    'description' => 'The configuration category to fetch'
                ),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A list of configuration items',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/stateOfResidence.Config.Category',
                    )
                ),
            )
        ));
    }

    public function registerOrmConnections(): void
    {
    }
}
