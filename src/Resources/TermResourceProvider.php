<?php

namespace MiamiOH\StateOfResidenceWebService\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class TermResourceProvider extends ResourceProvider
{

    private $classPath = 'MiamiOH\StateOfResidenceWebService\Services';

    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'StateOfResidence.TermStatus',
            'type' => 'object',
            'properties' => array(
                'termCode' => array(
                    'type' => 'number',
                ),
                'status' => array(
                    'type' => 'boolean',
                ),
                'priority' => array(
                    'type' => 'string',
                ),
            ),
        ));

        $this->addDefinition(array(
            'name' => 'StateOfResidence.TermStatus.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/StateOfResidence.TermStatus'
            )
        ));
        
        $this->addDefinition(array(
            'name' => 'StateOfResidence.Term',
            'type' => 'object',
            'properties' => array(
                'pidm' => array(
                    'type' => 'integer',
                ),
                'termCode' => array(
                    'type' => 'string',
                ),
                'termDesc' => array(
                    'type' => 'string',
                ),
                'termStartDate' => array(
                    'type' => 'string',
                ),
                'termEndDate' => array(
                    'type' => 'string',
                ),
                'status' => array(
                    'type' => 'string',
                ),
                'state' => array(
                    'type' => 'string',
                ),
                'country' => array(
                    'type' => 'string',
                ),
                'lastSeenDate' => array(
                    'type' => 'string',
                ),
                'lastConfirmedDate' => array(
                    'type' => 'string',
                ),
                'readOnly' => array(
                    'type' => 'boolean',
                ),
                'adminStatus' => array(
                    'type' => 'string',
                ),
                'comment' => array(
                    'type' => 'string',
                ),
                'priority' => array(
                    'type' => 'integer',
                ),
                'message' => array(
                    'type' => 'string',
                ),
                'courseList' =>array(
                    'type'=> 'array',
                    'items' => array(
                        '$ref' => '#/definitions/StateOfResidence.Enrollment.Course'
                    )
                ),
            ),
        ));
        $this->addDefinition(array(
            'name' => 'StateOfResidence.Enrollment.Course',
            'type' => 'object',
            'properties' => array(
                'subjectCode' => array(
                    'type' => 'string',
                ),
                'courseNumber' => array(
                    'type' => 'number',
                ),
                'courseSection' => array(
                    'type' => 'string',
                ),
                'courseTitle' => array(
                    'type' => 'string'
                ),
                'campusDesc' => array(
                    'type' => 'string',
                ),
                'courseType' => array(
                    'type' => 'string',
                ),
                'courseTypeDesc' => array(
                    'type' => 'string',
                ),
            ),
        ));
        $this->addDefinition(array(
            'name' => 'StateOfResidence.Term.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/StateOfResidence.Term'
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'StateOfResidence.TermInfoService',
            'class' => $this->classPath.'\TermInfoService',
            'description' => 'Provide the information for Term',
            'set' => array(
                'term' => array('type' => 'service', 'name' => 'StateOfResidence.TermInfo'),
            ),
        ));

        $this->addService(array(
            'name' => 'StateOfResidence.TermInfo',
            'class' => $this->classPath.'\TermInfo',
            'description' => 'Provide the information for Term',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
                'config' => array('type' => 'service', 'name' => 'StateOfResidenceConfig'),
            ),
        ));
        $this->addService(array(
            'name' => 'StateOfResidence.TermStatusService',
            'class' => $this->classPath.'\TermStatusService',
            'description' => 'Term status for a student based on rules applied',
            'set' => array(
                'bannerUtil' => array('type' => 'service', 'name' => 'MU\BannerUtil'),
                'termStatus' => array('type' => 'service', 'name' => 'StateOfResidence.TermStatus'),
            ),
        ));

        $this->addService(array(
            'name' => 'StateOfResidence.TermStatus',
            'class' => $this->classPath.'\TermStatus',
            'description' => 'Term status for a student based on rules applied',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
                'config' => array('type' => 'service', 'name' => 'StateOfResidenceConfig'),
            ),
        ));
    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'stateOfResidence.term.v1.read',
            'description' => 'Term information',
            'tags' => array('StateOfResidence'),
            'pattern' => '/stateOfResidence/termInfo',
            'service' => 'StateOfResidence.TermInfoService',
            'method' => 'getTermInfo',
            'middleware' => array(),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'Term information is fetched',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/StateOfResidence.Term.Collection',
                    )
                ),
            )
        ));
        $this->addResource(array(
            'action' => 'read',
            'name' => 'stateOfResidence.term.status.v1.read',
            'description' => 'Term status for a student based on rules applied',
            'tags' => array('StateOfResidence'),
            'pattern' => '/stateOfResidence/termStatus/:muid',
            'service' => 'StateOfResidence.TermStatusService',
            'method' => 'getTermStatus',
            'params' => array(
                'muid' => array('description' => 'A student uniqueid', 'alternateKeys' => ['uniqueId', 'pidm']),
            ),
            'options' => array(
                'termCode' => array(
                    'required' => true,
                    'description' => 'term code for which to get the status'
                ),
                'country' => array(
                    'required' => true,
                    'description' => 'Country where student is residing'
                ),
                'state' => array(
                    'description' => 'State where student is residing(State can be null for international students)'
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array(
                        'type' => 'self',
                        'param' => 'muid',
                    ),
                    array(
                        'application' => 'WebServices',
                        'module' => 'StateOfResidence-TermStatus',
                        'key' => 'view'
                    ),
                )
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'Term status information is fetched for a student based on rules applied',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/StateOfResidence.TermStatus.Collection',
                    )
                ),
            )
        ));
    }

    public function registerOrmConnections(): void
    {
    }
}
