<?php

namespace MiamiOH\StateOfResidenceWebService\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class ProfileResourceProvider extends ResourceProvider
{

    private $classPath = 'MiamiOH\StateOfResidenceWebService\Services';

    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'StateOfResidence.Profile',
            'type' => 'object',
            'properties' => array(
                'pidm' => array(
                    'type' => 'number',
                ),
                'uniqueId' => array(
                    'type' => 'string',
                ),
                'bannerId' => array(
                    'type' => 'string',
                ),
                'firstName' => array(
                    'type' => 'string',
                ),
                'lastName' => array(
                    'type' => 'string',
                ),
                'programDesc' => array(
                    'type'=> 'array',
                    'items' => array(
                        'type' => 'string'
                    )
                ),
                'divisionCode' => array(
                    'type' => 'string',
                ),
                'wasInfoRequiredStatus' => array(
                    'type' => 'boolean',
                ),
                'term' =>array(
                    'type'=> 'array',
                    'items' => array(
                        '$ref' => '#/definitions/StateOfResidence.Term'
                    )
                ),
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'Profile',
            'class' => $this->classPath.'\Profile',
            'description' => 'Provide student profile for the registered term',
            'set' => array(
                'app' => array('type' => 'service', 'name' => 'APIApp'),
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
                'dataSourceFactory' => array('type' => 'service', 'name' => 'APIDataSourceFactory'),
                'action' => array('type' => 'service', 'name' => 'Action'),
                'config' => array('type' => 'service', 'name' => 'StateOfResidenceConfig'),
                'audit' => array('type' => 'service', 'name' => 'StateOfResidence.Audit'),
                'termStatus' => array('type' => 'service', 'name' => 'StateOfResidence.TermStatus'),
                'was' => array('type' => 'service', 'name' => 'ActionWAS'),
            ),
        ));

        $this->addService(array(
            'name' => 'StateOfResidence.ProfileService',
            'class' => $this->classPath.'\ProfileService',
            'description' => 'Provide student profile for the registered term',
            'set' => array(
                'bannerUtil' => array('type' => 'service', 'name' => 'MU\BannerUtil'),
                'profile' => array('type' => 'service', 'name' => 'Profile'),
            ),
        ));
        $this->addService(array(
            'name' => 'StateOfResidence.Audit',
            'class' => $this->classPath.'\Audit',
            'description' => 'Provide create audit Service.',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),

            ),
        ));
    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'stateOfResidence.profile.v1.read',
            'description' => 'Get student profile for registered term',
            'tags' => array('StateOfResidence'),
            'pattern' => '/stateOfResidence/profile/:muid',
            'service' => 'StateOfResidence.ProfileService',
            'method' => 'getProfile',
            'params' => array(
                'muid' => array('description' => 'A student uniqueid', 'alternateKeys' => ['uniqueId', 'pidm']),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array(
                        'type' => 'self',
                        'param' => 'muid',
                    ),
                    array(
                        'application' => 'WebServices',
                        'module' => 'StateOfResidence-Profile',
                        'key' => 'view'
                    ),
                )
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A profile',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/StateOfResidence.Profile',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'update',
            'name' => 'stateOfResidence.profile.v1.update',
            'description' => 'Update student profile with state of residence for the term',
            'tags' => array('StateOfResidence'),
            'pattern' => '/stateOfResidence/profile/:muid',
            'service' => 'StateOfResidence.ProfileService',
            'method' => 'updateProfile',
            'params' => array(
                'muid' => array('description' => 'A student uniqueid', 'alternateKeys' => ['uniqueId', 'pidm']),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array(
                        'type' => 'self',
                        'param' => 'muid',
                    ),
                    array(
                        'application' => 'WebServices',
                        'module' => 'StateOfResidence-Profile',
                        'key' => 'view'
                    ),
                )
            ),
            'body' => array(
                'description' => 'Student profile model to update',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/StateOfResidence.Profile'
                )
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A profile',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/StateOfResidence.Profile',
                    )
                ),
            )
        ));
    }

    public function registerOrmConnections(): void
    {
    }
}
