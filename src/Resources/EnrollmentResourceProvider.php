<?php

namespace MiamiOH\StateOfResidenceWebService\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class EnrollmentResourceProvider extends ResourceProvider
{

    private $classPath = 'MiamiOH\StateOfResidenceWebService\Services';

    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'StateOfResidence.Enrollment',
            'type' => 'object',
            'properties' => array(
                'pidm' => array(
                    'type' => 'number',
                ),
                'termCode' => array(
                    'type' => 'string',
                ),
                'lastSeenDate' => array(
                    'type' => 'string',
                ),
                'daysSinceSeen' => array(
                    'type' => 'integer',
                ),
                'confirmedtoday' => array(
                    'type' => 'integer',
                ),
                'status' => array(
                    'type' => 'string',
                ),
                'nextAction' => array(
                    'type' => 'string',
                ),
                'lastAction' => array(
                    'type' => 'string',
                ),
                'daysSinceAction' => array(
                    'type' => 'integer',
                )
            ),
        ));

        $this->addDefinition(array(
            'name' => 'StateOfResidence.Enrollment.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/StateOfResidence.Enrollment'
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'StateOfResidence.EnrollmentService',
            'class' => $this->classPath.'\EnrollmentService',
            'description' => 'Provide the list of enrolled students',
            'set' => array(
                'enrollmentService' => array('type' => 'service', 'name' => 'StateOfResidence.Enrollment'),
            ),
        ));

        $this->addService(array(
            'name' => 'StateOfResidence.Enrollment',
            'class' => $this->classPath.'\Enrollment',
            'description' => 'Provide the list of enrolled students',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
                'config' => array('type' => 'service', 'name' => 'StateOfResidenceConfig'),
            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'StateOfResidence.Enrollment.v1.read',
            'description' => 'Enrollment information of eligible students',
            'tags' => array('StateOfResidence'),
            'pattern' => '/stateOfResidence/enrollment/v1/current',
            'service' => 'StateOfResidence.EnrollmentService',
            'method' => 'getEnrollmentInfo',
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array(
                        'application' => 'WebServices',
                        'module' => 'StateOfResidence-Enrollment',
                        'key' => 'view'
                    ),
                ),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'List of students are fetched',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/StateOfResidence.Enrollment.Collection',
                    )
                ),
            )
        ));
        $this->addResource(array(
            'action' => 'read',
            'name' => 'stateOfResidence.tracked.v1.read',
            'description' => 'Tracking information of eligible students',
            'tags' => array('StateOfResidence'),
            'pattern' => '/stateOfResidence/enrollment/v1/tracked',
            'service' => 'StateOfResidence.EnrollmentService',
            'method' => 'getTrackedInfo',
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array(
                        'application' => 'WebServices',
                        'module' => 'StateOfResidence-Enrollment',
                        'key' => 'view'
                    ),
                ),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'List of students are fetched',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/StateOfResidence.Enrollment.Collection',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'update',
            'name' => 'stateOfResidence.confirmedDate.v1.update',
            'description' => 'Update confirmed date for enrollments',
            'tags' => array('StateOfResidence'),
            'pattern' => '/stateOfResidence/enrollment/v1/confirmedDate',
            'service' => 'StateOfResidence.EnrollmentService',
            'method' => 'updateConfirmedDate',
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array(
                        'application' => 'WebServices',
                        'module' => 'StateOfResidence-Enrollment',
                        'key' => 'view'
                    ),
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'stateOfResidence.pastPending.v1.read',
            'description' => 'Tracking information of eligible students',
            'tags' => array('StateOfResidence'),
            'pattern' => '/stateOfResidence/enrollment/v1/pastPending',
            'service' => 'StateOfResidence.EnrollmentService',
            'method' => 'getPastPendingInfo',
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array(
                        'application' => 'WebServices',
                        'module' => 'StateOfResidence-Enrollment',
                        'key' => 'view'
                    ),
                ),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'List of students are fetched',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/StateOfResidence.Enrollment.Collection',
                    )
                ),
            )
        ));
    }

    public function registerOrmConnections(): void
    {
    }
}
