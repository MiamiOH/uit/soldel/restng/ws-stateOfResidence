<?php

namespace MiamiOH\StateOfResidenceWebService\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class ReportResourceProvider extends ResourceProvider
{

    private $classPath = 'MiamiOH\StateOfResidenceWebService\Services';

    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'StateOfResidence.Enrollment.Report.Record',
            'type' => 'object',
            'properties' => array(
                'pidm' => array(
                    'type' => 'number',
                ),
                'bannerId' => array(
                    'type' => 'string',
                ),
                'uniqueId' => array(
                    'type' => 'string',
                ),
                'firstName' => array(
                    'type' => 'string',
                ),
                'lastName' => array(
                    'type' => 'string',
                ),
                'termCode' => array(
                    'type' => 'string',
                ),
                'termDesc' => array(
                    'type' => 'string',
                ),
                'termStartDate' => array(
                    'type' => 'string',
                ),
                'termEndDate' => array(
                    'type' => 'string',
                ),
                'status' => array(
                    'type' => 'string',
                ),
                'country' => array(
                    'type' => 'string',
                ),
                'state' => array(
                    'type' => 'string',
                ),
                'lastSeenDate' => array(
                    'type' => 'string',
                ),
                'priorityContact' => array(
                    'type' => 'number',
                ),
                'readOnly' => array(
                    'type' => 'string',
                ),
                'programCode1' => array(
                    'type' => 'string',
                ),
                'programCode2' => array(
                    'type' => 'string',
                ),
                'majorCode1' => array(
                    'type' => 'string',
                ),
                'majorCode2' => array(
                    'type' => 'string',
                ),
                'majorCode1_2' => array(
                    'type' => 'string',
                ),
                'majorCode2_2' => array(
                    'type' => 'string',
                ),
                'comment' => array(
                    'type' => 'string',
                ),
                'adminStatus' => array(
                    'type' => 'string',
                ),
                'courseList' => array(
                    'type' => 'array',
                    'items' => array(
                        '$ref' => '#/definitions/StateOfResidence.Course'
                    )
                ),
            ),
        ));

        $this->addDefinition(array(
            'name' => 'StateOfResidence.Enrollment.Report.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/StateOfResidence.Enrollment.Report.Record'
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'StateOfResidence.ReportService',
            'class' => $this->classPath.'\ReportService',
            'description' => 'Generate a report for the list of enrolled students with the state of residence',
            'set' => array(
                'report' => array('type' => 'service', 'name' => 'Report'),
            ),
        ));

        $this->addService(array(
            'name' => 'Report',
            'class' => $this->classPath.'\Report',
            'description' => 'Generate a report for the list of enrolled students with the state of residence',
            'set' => array(
                'app' => array('type' => 'service', 'name' => 'APIApp'),
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
                'config' => array('type' => 'service', 'name' => 'StateOfResidenceConfig'),
                'deferredCallManager' => array('type' => 'service', 'name' => 'APIDeferredCallManager'),
            ),
        ));
    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'stateOfResidence.enrollment.report.v1.read',
            'description' => 'Read report of enrolled students with the state of residence',
            'tags' => array('StateOfResidence'),
            'pattern' => '/stateOfResidence/enrollment/report/v1',
            'service' => 'StateOfResidence.ReportService',
            'method' => 'getEnrollmentReport',
            'isPageable' => true,
            'defaultPageLimit' => 200,
            'maxPageLimit' => 1000,
            'options' => array(
                'key' => array(
                    'required' => true,
                    'description' => 'key identifying the deferred call used to update the report data'
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array(
                        'application' => 'WebServices',
                        'module' => 'StateOfResidence-Report',
                        'key' => 'view'
                    ),
                ),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'Generate a report for the list of enrolled students with the state of residence',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/StateOfResidence.Enrollment.Report.Collection',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'create',
            'name' => 'stateOfResidence.enrollment.report.v1.create',
            'description' => 'Generate a report for the list of enrolled students with the state of residence',
            'tags' => array('StateOfResidence'),
            'pattern' => '/stateOfResidence/enrollment/report/v1',
            'service' => 'StateOfResidence.ReportService',
            'method' => 'createEnrollmentReport',
            'isPageable' => false,
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array(
                        'application' => 'WebServices',
                        'module' => 'StateOfResidence-Report',
                        'key' => 'create'
                    ),
                ),
            ),
            'responses' => array(
                App::API_CREATED => array(
                    'description' => 'Generate a report for the list of enrolled students with the state of residence',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/StateOfResidence.Enrollment.Report.Collection',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'update',
            'name' => 'stateOfResidence.enrollment.report.v1.update',
            'description' => 'Update the report table with new data',
            'tags' => array('StateOfResidence'),
            'pattern' => '/stateOfResidence/enrollment/report/v1',
            'service' => 'StateOfResidence.ReportService',
            'method' => 'updateEnrollmentReport',
            'isPageable' => false,
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array(
                        'application' => 'WebServices',
                        'module' => 'StateOfResidence-Report',
                        'key' => 'update'
                    ),
                ),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'Update the report table with new data',
                ),
            )
        ));
    }

    public function registerOrmConnections(): void
    {
    }
}
