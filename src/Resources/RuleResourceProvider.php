<?php

namespace MiamiOH\StateOfResidenceWebService\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class RuleResourceProvider extends ResourceProvider
{

    private $classPath = 'MiamiOH\StateOfResidenceWebService\Services';

    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'StateOfResidence.Rule',
            'type' => 'object',
            'properties' => array(
                'ruleID' => array(
                    'type' => 'number',
                ),
                'ruleType' => array(
                    'type' => 'string',
                ),
                'attribute' => array(
                    'type' => 'string',
                ),
                'allowedStates' => array(
                    'type' => 'string',
                ),
                'ruleValue' => array(
                    'type' => 'string',
                ),
            ),
        ));

        $this->addDefinition(array(
            'name' => 'StateOfResidence.Rule.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/StateOfResidence.Rule'
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'StateOfResidence.Rule',
            'class' => $this->classPath.'\Rule',
            'description' => 'List of rules from the rule table',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
            ),
        ));

        $this->addService(array(
            'name' => 'StateOfResidence.RuleService',
            'class' => $this->classPath.'\RuleService',
            'description' => 'List of rules from the rule table',
            'set' => array(
                'rule' => array('type' => 'service', 'name' => 'StateOfResidence.Rule'),
            ),
        ));
    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'stateOfResidence.Rule.v1.read',
            'description' => 'Get the list of rules for state of residence',
            'tags' => array('StateOfResidence'),
            'pattern' => '/stateOfResidence/rule',
            'service' => 'StateOfResidence.RuleService',
            'method' => 'getRuleList',
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array(
                        'application' => 'WebServices',
                        'module' => 'StateOfResidence-Rule',
                        'key' => 'view'
                    ),
                )
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A collection of course ',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/StateOfResidence.Rule.Collection',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'update',
            'name' => 'stateOfResidence.Rule.v1.update',
            'description' => 'Update the list of rules for state of residence',
            'tags' => array('StateOfResidence'),
            'pattern' => '/stateOfResidence/rule',
            'service' => 'StateOfResidence.RuleService',
            'method' => 'updateRuleList',
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array(
                        'application' => 'WebServices',
                        'module' => 'StateOfResidence-Rule',
                        'key' => 'update'
                    ),
                )
            ),
            'body' => array(
                'description' => 'Rule details to be deleted',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/StateOfResidence.Rule.Collection',
                )
            ),
        ));

        $this->addResource(array(
            'action' => 'update',
            'name' => 'stateOfResidence.Rule.v1.update.id',
            'description' => 'Update a rule for state of residence',
            'tags' => array('StateOfResidence'),
            'pattern' => '/stateOfResidence/rule/:id',
            'params' => array(
                'id' => array('description' => 'The id of the rule to be updated'),
            ),
            'service' => 'StateOfResidence.RuleService',
            'method' => 'updateRule',
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array(
                        'application' => 'WebServices',
                        'module' => 'StateOfResidence-Rule',
                        'key' => 'update'
                    ),
                )
            ),
            'body' => array(
                'description' => 'Rule to be updated',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/StateOfResidence.Rule',
                )
            ),
        ));

        $this->addResource(array(
            'action' => 'create',
            'name' => 'stateOfResidence.Rule.v1.create',
            'description' => 'Create the list of rules for state of residence',
            'tags' => array('StateOfResidence'),
            'pattern' => '/stateOfResidence/rule',
            'service' => 'StateOfResidence.RuleService',
            'method' => 'createRuleList',
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array(
                        'application' => 'WebServices',
                        'module' => 'StateOfResidence-Rule',
                        'key' => 'create'
                    ),
                )
            ),
            'body' => array(
                'description' => 'Rule details to be deleted',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/StateOfResidence.Rule.Collection',
                )
            ),
        ));

        $this->addResource(array(
            'action' => 'delete',
            'name' => 'stateOfResidence.Rule.v1.delete',
            'description' => 'Delete the rules for state of residence',
            'tags' => array('StateOfResidence'),
            'pattern' => '/stateOfResidence/rule',
            'service' => 'StateOfResidence.RuleService',
            'method' => 'deleteRuleList',
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array(
                        'application' => 'WebServices',
                        'module' => 'StateOfResidence-Rule',
                        'key' => 'delete'
                    ),
                )
            ),
            'body' => array(
                'description' => 'Rule details to be deleted',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/StateOfResidence.Rule.Collection',
                )
            ),
        ));

        $this->addResource(array(
            'action' => 'delete',
            'name' => 'stateOfResidence.Rule.v1.delete.id',
            'description' => 'Delete rule for state of residence',
            'tags' => array('StateOfResidence'),
            'pattern' => '/stateOfResidence/rule/:id',
            'params' => array(
                'id' => array('description' => 'The id of the rule to be deleted'),
            ),
            'service' => 'StateOfResidence.RuleService',
            'method' => 'deleteRule',
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array(
                        'application' => 'WebServices',
                        'module' => 'StateOfResidence-Rule',
                        'key' => 'delete'
                    ),
                )
            ),
        ));
    }

    public function registerOrmConnections(): void
    {
    }
}
