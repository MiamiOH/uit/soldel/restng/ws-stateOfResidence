<?php

namespace MiamiOH\StateOfResidenceWebService\Resources;

use MiamiOH\RESTng\App;

class ActionResourceProvider extends \MiamiOH\RESTng\Util\ResourceProvider
{
    private $serviceName = 'StateOfResidenceService';
    private $tag = "stateOfResidence";
    private $resourceRoot = "stateOfResidence";
    private $patternRoot = "/stateOfResidence";
    private $classPath = 'MiamiOH\StateOfResidenceWebService\Services';

    public function registerDefinitions(): void
    {
        $this->addTag(array(
            'name' => $this->tag,
            'description' => 'State of Residence Web Service'
        ));

        $this->addDefinition(array(
            'name' => 'StateOfResidence.Enrollment',
            'type' => 'object',
            'properties' => array(
                'pidm' => array(
                    'type' => 'number',
                ),
                'termCode' => array(
                    'type' => 'string',
                ),
                'lastSeenDate' => array(
                    'type' => 'string',
                ),
                'daysSinceSeen' => array(
                    'type' => 'integer',
                ),
                'confirmedtoday' => array(
                    'type' => 'integer',
                ),
                'status' => array(
                    'type' => 'string',
                ),
                'nextAction' => array(
                    'type' => 'string',
                ),
                'lastAction' => array(
                    'type' => 'string',
                ),
                'daysSinceAction' => array(
                    'type' => 'integer',
                )
            ),
        ));

        $this->addDefinition(array(
            'name' => 'StateOfResidence.Enrollment.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/StateOfResidence.Enrollment'
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'ActionWAS',
            'class' => $this->classPath.'\WAS',
            'description' => 'Provide methods to interact with WAS for updates',
            'set' => array(
                'configuration' => array('type' => 'service', 'name' => 'APIConfiguration'),
                'app' => array('type' => 'service', 'name' => 'APIApp'),
            ),
        ));

        $this->addService(array(
            'name' => 'Action',
            'class' => $this->classPath.'\Action',
            'description' => 'Perform specified action for collection of students/a student',
            'set' => array(
                'app' => array('type' => 'service', 'name' => 'APIApp'),
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
                'config' => array('type' => 'service', 'name' => 'StateOfResidenceConfig'),
                'was' => array('type' => 'service', 'name' => 'ActionWAS'),
            ),
        ));

        $this->addService(array(
            'name' => 'StateOfResidence.ActionService',
            'class' => $this->classPath.'\ActionService',
            'description' => 'Perform specified action for collection of students/a student',
            'set' => array(
                'bannerUtil' => array('type' => 'service', 'name' => 'MU\BannerUtil'),
                'action' => array('type' => 'service', 'name' => 'Action'),
            ),
        ));
    }

    public function registerResources(): void
    {
        // Resource GET CourseSection

        $this->addResource(array(
            'action' => 'update',
            'name' => 'StateOfResidence.Action.update',
            'description' => 'Update next action of student',
            'tags' => array('StateOfResidence'),
            'pattern' => '/stateOfResidence/action',
            'service' => 'StateOfResidence.ActionService',
            'method' => 'updateAction',
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array(
                        'application' => 'WebServices',
                        'module' => 'StateOfResidence-Action',
                        'key' => 'update'
                    ),
                ),
            ),
            'body' => array(
                'description' => 'Collection of models with next action for each student',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/StateOfResidence.Enrollment.Collection'
                )
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'Update next action of student',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/StateOfResidence.Enrollment.Collection',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'update',
            'name' => 'StateOfResidence.Action.update.action',
            'description' => 'Update next action for collection of students',
            'tags' => array('StateOfResidence'),
            'pattern' => '/stateOfResidence/action/:action',
            'service' => 'StateOfResidence.ActionService',
            'method' => 'updateActionCollection',
            'params' => array(
                'action' => array('description' => 'Next action for collection of students'),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array(
                        'application' => 'WebServices',
                        'module' => 'StateOfResidence-Action',
                        'key' => 'update'
                    ),
                ),
            ),
            'body' => array(
                'description' => 'Student action model',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/StateOfResidence.Enrollment.Collection'
                )
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'Update next action for collection of students',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/StateOfResidence.Enrollment.Collection',
                    )
                ),
            )
        ));


        // Resource GET CourseSection Object by GUID

    }

    public function registerOrmConnections(): void
    {
    }
}
