<?php

namespace MiamiOH\StateOfResidenceWebService\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class StubResourceProvider extends ResourceProvider
{

    private $classPath = 'MiamiOH\StateOfResidenceWebService\Services';

    public function registerDefinitions(): void
    {
    }

    public function registerServices(): void
    {

    }

    public function registerResources(): void
    {

    }

    public function registerOrmConnections(): void
    {
    }
}
