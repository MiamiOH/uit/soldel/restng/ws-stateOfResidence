<?php

namespace MiamiOH\StateOfResidenceWebService\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class CourseResourceProvider extends ResourceProvider
{

    private $classPath = 'MiamiOH\StateOfResidenceWebService\Services';

    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'StateOfResidence.Enrollment.Course',
            'type' => 'object',
            'properties' => array(
                'subjectCode' => array(
                    'type' => 'string',
                ),
                'courseNumber' => array(
                    'type' => 'number',
                ),
                'courseSection' => array(
                    'type' => 'string',
                ),
                'courseTitle' => array(
                    'type' => 'string'
                ),
                'campusDesc' => array(
                    'type' => 'string',
                ),
                'courseType' => array(
                    'type' => 'string',
                ),
                'courseTypeDesc' => array(
                    'type' => 'string',
                ),
            ),
        ));
        
        $this->addDefinition(array(
            'name' => 'StateOfResidence.Course.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/StateOfResidence.Course'
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'Enrollment.Course',
            'class' => $this->classPath.'\Course',
            'description' => 'Provide the list of courses to mentis',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
                'config' => array('type' => 'service', 'name' => 'StateOfResidenceConfig'),
            ),
        ));

        $this->addService(array(
            'name' => 'StateOfResidence.Enrollment.CourseService',
            'class' => $this->classPath.'\CourseService',
            'description' => 'Provide the list of courses to mentis ',
            'set' => array(
                'course' => array('type' => 'service', 'name' => 'Enrollment.Course'),
            ),
        ));
    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'stateOfResidence.Course.v1.read',
            'description' => 'Get the list of courses ',
            'tags' => array('StateOfResidence'),
            'pattern' => '/stateOfResidence/course',
            'service' => 'StateOfResidence.Enrollment.CourseService',
            'method' => 'getCourseList',
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array(
                        'application' => 'WebServices',
                        'module' => 'StateOfResidence-Course',
                        'key' => 'view'
                    ),
                )
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A collection of course ',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/StateOfResidence.Course.Collection',
                    )
                ),
            )
        ));
    }

    public function registerOrmConnections(): void
    {
    }
}
