<?php

namespace MiamiOH\StateOfResidenceWebService\Services;

use MiamiOH\RESTng\Service;

class Audit extends Service
{
    private $datasource_name = 'MUWS_GEN_PROD';
    private $dbh;


    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->datasource_name);
    }

    public function insertAuditTable($country, $state, $pidm, $termCode, $uniqueId)
    {

        $queryString = 'insert into SATURN.SZRSORB (szrsorb_natn_code,szrsorb_stat_code,szrsorb_pidm,szrsorb_term_code,szrsorb_uid,
        szrsorb_activity_date) values(?,?,?,?,?,sysdate)';
        $this->dbh->perform($queryString, $country, $state, $pidm, $termCode, $uniqueId);

        return true;

    }

}