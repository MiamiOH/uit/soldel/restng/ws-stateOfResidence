<?php
/**
 * Created by PhpStorm.
 * User: ravendpp
 * Date: 1/6/17
 * Time: 4:23 PM
 */

namespace MiamiOH\StateOfResidenceWebService\Services;

use MiamiOH\RESTng\Service;

class AdminStatus extends Service
{

    private $datasource_name = 'MUWS_GEN_PROD';

    private $dbh;

    /**
     * @param $database
     */

    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->datasource_name);
    }

    public function updateAdminStatus($pidm, $statusInfo)
    {


            $queryString = "update szrsorm set szrsorm_admin_status = ?, szrsorm_comment = ?, szrsorm_priority = ? where szrsorm_pidm = ? and szrsorm_term_code = ?";

            $params = array($statusInfo['adminStatus'],$statusInfo['comment'],$statusInfo['priority'], $pidm, $statusInfo['termCode']);

            $this->dbh->perform($queryString, $params);


        return true;
    }
}