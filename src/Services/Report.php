<?php

namespace MiamiOH\StateOfResidenceWebService\Services;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\DeferredCallManager;

class Report
{

    private $datasource_name = 'MUWS_GEN_PROD';

    /** @var App */
    private $app;

    /** @var Config */
    private $config;

    /** @var  DeferredCallManager $deferredCallManager*/
    private $deferredCallManager;

    private $dbh;

    private $sth;

    private $totalRecords;

    public function setApp(App $app): void
    {
        $this->app = $app;
    }

    /**
     * @param $database
     */
    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->datasource_name);
    }

    public function setDeferredCallManager($deferredCallManager)
    {
        /** @var  DeferredCallManager $deferredCallManager*/
        $this->deferredCallManager = $deferredCallManager;
    }

    public function setTotalRecords($totalRecords)
    {
        $this->totalRecords = $totalRecords;
    }

    public function getTotalRecords()
    {
        return $this->totalRecords;
    }

    /**
     * @param $config
     */
    public function setConfig(Config $config)
    {
        $this->config = $config;
    }

    public function createEnrollmentReport()
    {
        $callOptions = array(
            'deferred' => true,
            'key' => 'sofr.report.create.' . uniqid()
        );

        $callResponse = $this->app->callResource('stateOfResidence.enrollment.report.v1.update',
            [], $callOptions);

        return $callResponse->getPayload();
    }

    public function updateEnrollmentReport()
    {
        $previousTerm = $this->dbh->queryfirstcolumn('select fz_get_previous_term() from dual');

        $currentTerm = $this->dbh->queryfirstcolumn('select fz_get_term() from dual');

        // nextTerm
        $currentTermPlusOne = $this->dbh->queryfirstcolumn('select fz_get_next_term() from dual');

        // 2 terms after the current term
        $currentTermPlusTwo = $this->dbh->queryfirstcolumn('select fz_get_next_term(' . "'" . $currentTermPlusOne . "'" . ') from dual');

        $termCodes = [$previousTerm, $currentTerm, $currentTermPlusOne, $currentTermPlusTwo];

        $configData = $this->config->getConfiguration('CodeMap');
        $validCodes = array_keys( json_decode($configData['courseTypeDesc'], true) );

        // This query is to fetch registered term codes and course details for all the students in szrsorm table with status as 'pending' and 'complete'
        $queryString = '
            begin
            
            delete from szrsord;
            
            insert into szrsord (
                pidm,
                term_code,
                status,
                state_code,
                nation_code,
                lastseen_date,
                priority,
                term_desc,
                crn,
                level_code,
                credit,
                course_subject,
                course_number,
                course_section,
                course_title,
                banner_id,
                unique_id,
                first_name,
                last_name,
                instructional_method,
                campus_code,
                campus_desc,
                program_code_1,
                program_code_2,
                major_code_1,
                major_code_2,
                major_code_1_2,
                major_code_2_2,
                admin_status,
                status_comment
               )
            select
                szrsorm_pidm, 
                szrsorm_term_code, 
                szrsorm_status, 
                szrsorm_stat_code, 
                szrsorm_natn_code,
                szrsorm_lastseen_date, 
                szrsorm_priority, 
                stvterm_desc, 
                sfrstcr_crn,
                scrlevl_levl_code,
                SFRSTCR_CREDIT_HR,
                ssbsect_subj_code, 
                ssbsect_crse_numb,
                trim(ssbsect_seq_numb),
                scbcrse_title,
                szbuniq_banner_id, 
                szbuniq_unique_id,
                coalesce(spbpers_pref_first_name, spriden_first_name) as spriden_first_name, 
                spriden_last_name,
                ssrattr_attr_code,
                ssbsect_camp_code,
                stvcamp_desc,
                sgbstdn_program_1, 
                sgbstdn_program_2,
                sgbstdn_majr_code_1, 
                sgbstdn_majr_code_2,
                sgbstdn_majr_code_1_2, 
                sgbstdn_majr_code_2_2, 
                szrsorm_admin_status,
                szrsorm_comment
            from szrsorm, sfrstcr, ssbsect, scbcrse, stvterm, szbuniq, spriden, ssrattr, stvcamp, sgbstdn, scrlevl, spbpers
            where szrsorm_status IN (\'pending\', \'complete\') AND szrsorm_term_code in (\'' . implode("','", $termCodes) . '\')
                            and spriden_pidm = spbpers_pidm
                            and sfrstcr_pidm = szrsorm_pidm and sfrstcr_term_code = szrsorm_term_code
                            and ssbsect_crn = sfrstcr_crn and ssbsect_term_code = szrsorm_term_code
                            and scbcrse_subj_code = ssbsect_subj_code
                            and scbcrse_crse_numb = ssbsect_crse_numb
                            and scbcrse_eff_term = (
                              select max(scbcrse_eff_term) 
                              from scbcrse 
                              where scbcrse_subj_code = ssbsect_subj_code
                                  and scbcrse_crse_numb = ssbsect_crse_numb
                                  and scbcrse_eff_term <= ssbsect_term_code)
                            and scrlevl_crse_numb = scbcrse_crse_numb
                            and scrlevl_subj_code = scbcrse_subj_code
                            and scrlevl_eff_term = (
                              select max(scrlevl_eff_term) 
                              from scrlevl 
                              where scrlevl_crse_numb = scbcrse_crse_numb
                                  and scrlevl_subj_code = scbcrse_subj_code
                                  and scrlevl_eff_term <= ssbsect_term_code)
                            and stvterm_code = szrsorm_term_code
                            and szbuniq_pidm = szrsorm_pidm
                            and spriden_pidm = szrsorm_pidm and spriden_change_ind is NULL
                            and ssrattr_term_code = szrsorm_term_code
                            and ssrattr_crn = sfrstcr_crn
                            and ssrattr_attr_code in (\'' . implode("','", $validCodes) . '\')
                            and stvcamp_code = ssbsect_camp_code
                            and sgbstdn_pidm = szrsorm_pidm
                            and sgbstdn_term_code_eff = (
                              select max(sgbstdn_term_code_eff) 
                              from sgbstdn 
                              where sgbstdn_pidm = szrsorm_pidm)
            order by szrsorm_pidm, sfrstcr_crn;
            end;
           ';

        $sth = $this->dbh->prepare($queryString);
        $sth->execute();
    }

    /**
     * This method gets all the registered term and course details for all the students in szrsorm table with status as "pending"
     * and "complete"
     * @return array
     */
    public function getEnrollmentReport($key, $isPaged, $offset, $limit)
    {

        $call = $this->deferredCallManager->getCallByKey($key);

        if ($call === null) {
            throw new ReportErrorException('Deferred call ' . $key . ' not found');
        }

        if (in_array($call->status(), ['p', 'i'])) {
            throw new ReportPendingException();
        } elseif (in_array($call->status(), ['x', 'f'])) {
            throw new ReportErrorException();
        }


        // This query is to fetch registered term codes and course details for all the students in szrsorm table with status as 'pending' and 'complete'
        $queryString = 'select pidm,
                term_code,
                status,
                state_code,
                nation_code,
                lastseen_date,
                priority,
                term_desc,
                crn,
                level_code,
                credit,
                course_subject,
                course_number,
                course_section,
                course_title,
                banner_id,
                unique_id,
                first_name,
                last_name,
                instructional_method,
                campus_code,
                campus_desc,
                program_code_1,
                program_code_2,
                major_code_1,
                major_code_2,
                major_code_1_2,
                major_code_2_2,
                admin_status,
                status_comment
            from szrsord
            order by pidm, term_code, crn';

        if ($isPaged) {
            $minRowToFetch = $offset;
            $maxRowToFetch = $minRowToFetch + $limit - 1;
            // paged queries should include the total number of records
            $totalRecords = $this->dbh->queryfirstcolumn('
                    select count(*)
                        from szrsord
            ');

            $this->setTotalRecords($totalRecords);

            $queryString = "select pidm,
                term_code,
                status,
                state_code,
                nation_code,
                lastseen_date,
                priority,
                term_desc,
                crn,
                level_code,
                credit,
                course_subject,
                course_number,
                course_section,
                course_title,
                banner_id,
                unique_id,
                first_name,
                last_name,
                instructional_method,
                campus_code,
                campus_desc,
                program_code_1,
                program_code_2,
                major_code_1,
                major_code_2,
                major_code_1_2,
                major_code_2_2,
                admin_status,
                status_comment
                  from ( select /*+ FIRST_ROWS(n) */
                    a.*, ROWNUM rnum
                      from ( $queryString ) a
                      where ROWNUM <= :MAX_ROW_TO_FETCH )
                where rnum  >= :MIN_ROW_TO_FETCH";
            $this->sth = $this->dbh->prepare($queryString);

            $this->sth->bind_by_name('MAX_ROW_TO_FETCH', $maxRowToFetch);
            $this->sth->bind_by_name('MIN_ROW_TO_FETCH', $minRowToFetch);
        } else {
            $this->sth = $this->dbh->prepare($queryString);
        }

        $this->sth->execute();

        // Build a data structure: key is student pidm and values is collection of unique term codes and enrollment
        $results = [];
        while ($record = $this->sth->fetchrow_assoc()) {
            $results[] = $this->camelCaseKeys($record);
        }

        return $results;
    }

    public function camelCaseKeys($array, $arrayHolder = array())
    {
        if (!is_array($array)) {
            throw new \Exception('Argument to camelCaseKeys must be an array');
        }

        $camelCaseArray = !empty($arrayHolder) ? $arrayHolder : array();
        foreach ($array as $key => $val) {
            $newKey = $key;
            if (strpos($key, '_') !== false) {
                $newKey = @explode('_', $key);
                for ($i = 1; $i < count($newKey); $i++) {
                    $newKey[$i] = ucwords($newKey[$i]);
                }
                $newKey = @implode('', $newKey);
            }

            $camelCaseArray[$newKey] = $val;
        }

        return $camelCaseArray;
    }
}