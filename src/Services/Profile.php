<?php

namespace MiamiOH\StateOfResidenceWebService\Services;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Connector\DataSource;
use MiamiOH\RESTng\Connector\DataSourceFactory;
use MiamiOH\RESTng\Core\DB\DBH;
use MiamiOH\RESTng\Service;

class Profile extends Service
{
    private $datasource_name = 'MUWS_GEN_PROD';

    private $dbh;

    /** @var  DataSourceFactory $dsFactory */
    private $dsFactory;

    private $studentProfile;

    private $readDataQuerySql = '';

    private $readDataQueryParams = [];

    private $action;

    private $termStatus;

    private $triggerValueAfterTermStart;

    private $config;
    private $insertAudit;
    private $courseCodeMap;

    private $was;

    /**
     * @param $database
     */

    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->datasource_name);
    }

    /**
     * @param $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @param $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    public function setAudit($insertAudit)
    {

        $this->insertAudit = $insertAudit;
    }

    public function setDataSourceFactory($dsFactory)
    {
        /** @var  DataSourceFactory $dsFactory */
        $this->dsFactory = $dsFactory;
    }

    /**
     * @param $termStatus
     */
    public function setTermStatus($termStatus)
    {

        $this->termStatus = $termStatus;
    }

    public function setWas($was)
    {
        $this->was = $was;
    }

    /**
     * This method updates the student state/country information for the registered terms(current/future)
     * into the SZRSORM table
     * @param $pidm
     * @param $updateProfileInfo
     * @return bool
     */
    public function updateProfile($pidm, $updateProfileInfo)
    {
        $termList = $updateProfileInfo['term'];

        // Fetch the action trigger values from config manager

        $configData = $this->config->getConfiguration("ActionTrigger");

        $triggerValueAfterTermStart = $configData['triggerValueAfterTermStart'];

        $currentDate = date('Ymd');

        $configDataTermCode = $this->config->getConfiguration("CurrentTerm");

        $currentTermCode = $configDataTermCode[0];

        $queryStartDate = 'select STVTERM_START_DATE from STVTERM where STVTERM_CODE = ?';

        $startDateForTerm = $this->dbh->queryfirstcolumn($queryStartDate, $configDataTermCode);

        $startDateForTerm = new \DateTime($startDateForTerm);

        $triggerValueAfterTermStart = date_modify($startDateForTerm, $triggerValueAfterTermStart);

        $triggerValueAfterTermStart = date_format($triggerValueAfterTermStart, 'Ymd');

        $savedTermInfo = [];
        try {
            $this->dbh->auto_commit(false);
            foreach ($termList as $term) {
                if ($term['termCode'] <= $currentTermCode) {

                    if ($currentDate > $triggerValueAfterTermStart) {

                        throw new \Exception('Unable to update the student state of residence information');
                    }
                }

                $termStatusOptions = [
                    'termCode' => $term['termCode'],
                    'country' => $term['country'],
                    'state' => $term['state'],
                ];

                $termStatusRecord = $this->termStatus->getTermStatus($pidm, $termStatusOptions);

                $queryString = 'UPDATE SATURN.SZRSORM SET szrsorm_natn_code = ?, szrsorm_stat_code=?,
                    szrsorm_priority = ? WHERE szrsorm_pidm = ? AND szrsorm_term_code = ?';

                $params = array(
                    $term['country'],
                    $term['state'],
                    $termStatusRecord['priority'],
                    $pidm,
                    $term['termCode']
                );

                $this->dbh->perform($queryString, $params);


                $this->action->setDBH($this->dbh);

                // Call action "complete" to update the status and take care of allowing student to login to WAS if the student is set in WAS.
                $this->action->updateCompleteAction($pidm, $updateProfileInfo['uniqueId'], $term['termCode']);

                $this->dbh->commit();

                /* If the insert in audit fails it will not affect the update in profile */
                $this->insertAudit->insertAuditTable($term['country'], $term['state'], $pidm, $term['termCode'],
                    $updateProfileInfo['uniqueId']);

                $termString = $term['termDesc'] . ': ';
                if ($term['state']) {
                    $termString .= $term['state'] . ' (' . $term['country'] . ')';
                } else {
                    $termString .= $term['country'];
                }
                $savedTermInfo[] = $termString;
            }
        } catch (\Exception $e) {
            $this->dbh->rollback();
            throw $e;
        }

        // Send confirmation email to student
        $emailType = 'confirmation';
        $toAddress = $updateProfileInfo['uniqueId'] . '@miamioh.edu';
        $priority = 2;

        $configData = $this->config->getConfiguration("SendNotification");

        $fromAddress = isset($configData['email.fromAddress']) ? $configData['email.fromAddress'] : 'StateofResidence@miamioh.edu';
        // Get subject and body from ConfigMr based on email type(email1 or email2)
        $subject = $configData[$emailType . '.subject'];
        $body = $configData[$emailType . '.body'];

        $termInfoMsg = implode("\n", $savedTermInfo);

        $body = str_replace('{termInfo}', $termInfoMsg, $body);

        $dataSource = $this->dsFactory->getDataSource('SofR_WS_User');

        if (!($dataSource instanceof DataSource)) {
            throw new \Exception('Could not find data source IDVaultWS');
        }

        $user = $this->getApiUser();
        $localUserKey = $user->addUserByCredentials(array(
            'username' => $dataSource->getUser(),
            'password' => $dataSource->getPassword()
        ));

        if (!$localUserKey) {
            throw new \Exception('Unable to add local user when calling notification service. (' . $dataSource['user'] . ')');
        }

        $sendResponse = $this->callResource('notification.v1.email.message.create',
            array(
                'data' => array(
                    'dataType' => 'model',
                    'data' => array(
                        'toAddr' => $toAddress,
                        'fromAddr' => $fromAddress,
                        'priority' => $priority,
                        'subject' => $subject,
                        'body' => $body,
                    ),
                )
            ));

        $user->removeUser($localUserKey);

        if ($sendResponse->getStatus() !== App::API_CREATED) {
            throw new \Exception('Failed to send confirmation message: ' . print_r($sendResponse->getStatus(), true));
        }

        return true;


    }

    /**
     * @param $pidm
     */
    public function getProfile($pidm)
    {

        $configData = $this->config->getConfiguration("CurrentTerm");

        $currentTerm = $configData[0];


        $this->makeReadQuery($pidm);
        $identityRecords = $this->dbh->queryfirstrow_assoc($this->readDataQuerySql, $this->readDataQueryParams);

        $programDesc = $this->getProgramDescription($pidm);


        $queryStringResidence = '
            select szrsorm_pidm, szrsorm_term_code, stvterm_desc,szbuniq_unique_id,
                    to_char(stvterm_start_date, \'YYYY-MM-DD\') as stvterm_start_date,
                    to_char(stvterm_end_date, \'YYYY-MM-DD\') as stvterm_end_date,
                    szrsorm_status,szrsorm_stat_code,szrsorm_natn_code,szrsorm_priority,
                    szrsorm_lastseen_date, szrsorm_lastconfirmed_date, szrsorm_admin_status, szrsorm_comment
                from szrsorm inner join stvterm on (stvterm_code = szrsorm_term_code)
                inner join szbuniq on (szbuniq_pidm = szrsorm_pidm)
                where szrsorm_pidm = ? and szrsorm_term_code >= ?
                order by szrsorm_term_code
            ';

        $residenceRecords = $this->dbh->queryall_array($queryStringResidence, $pidm, $currentTerm);

        $modelResidence = [];
        for ($i = 0; $i < count($residenceRecords); $i++) {
            $modelResidence[$i] = $this->makeModelForResidence($residenceRecords[$i], $identityRecords['szbuniq_unique_id']);
        }

        $divisionCodes = $this->dbh->queryfirstrow_assoc('select sgbstdn_coll_code_1, sgbstdn_coll_code_2 from sgbstdn where sgbstdn_pidm = ?', $pidm);
        if($divisionCodes ===  DBH::DB_EMPTY_SET) {
            $divisionCodes = '';
        }

        // Get WAS info required status to display it in student profile admin app
        try {
	        $wasInfoRequiredStatus = $this->was->getWASStatus($identityRecords['szbuniq_unique_id']);
        } catch(\Exception $e) {
			// ToDo: We need to catch the exception and generate appropriate message.
			$wasInfoRequiredStatus = '';
        }

        $results = $this->makeModelFromRecord($identityRecords, $modelResidence, $programDesc, $divisionCodes, $wasInfoRequiredStatus);

        return $results;
    }

    /**
     * @param $pidm
     */
    public function makeReadQuery($pidm)
    {


        $this->readDataQuerySql = '
            select szbuniq_pidm, szbuniq_banner_id, szbuniq_unique_id,
                coalesce(spbpers_pref_first_name, spriden_first_name) as spriden_first_name,
                spriden_last_name
              from szbuniq
                inner join spriden on (spriden_pidm = szbuniq_pidm)
                inner join spbpers on (spriden_pidm = spbpers_pidm)
              where szbuniq_pidm = ?
                and spriden_change_ind is null
            ';


        $this->readDataQueryParams = $pidm;
    }

    /**
     * @param $identityRecord
     * @param $residenceRecord
     * @return mixed
     */

    public function makeModelFromRecord($identityRecord, $residenceRecord, $programDesc, $divisionCodes, $wasInfoRequiredStatus)
    {

        $model['pidm'] = $identityRecord['szbuniq_pidm'];
        $model['uniqueId'] = $identityRecord['szbuniq_unique_id'];
        $model['bannerId'] = $identityRecord['szbuniq_banner_id'];
        $model['firstName'] = $identityRecord['spriden_first_name'];
        $model['lastName'] = $identityRecord['spriden_last_name'];
        $model['programDesc'] = $programDesc;
        $model['wasInfoRequiredStatus'] = $wasInfoRequiredStatus;
        $model['term'] = $residenceRecord;

        // sgbstdn table will not have entry for some who is not associated with Miami as student. In such case $divisionCodes can be empty.
        // Front end checks if the code is not equal to 'RC'. so better to avoid sending empty codes(eg: RC| ) when implode is used.
        if($divisionCodes == '' || ($divisionCodes['sgbstdn_coll_code_1'] == '' &&  $divisionCodes['sgbstdn_coll_code_2'] == '')) {
            $model['divisionCode'] = '';
        } else {
            if($divisionCodes['sgbstdn_coll_code_1'] != '' && $divisionCodes['sgbstdn_coll_code_2'] != '') {
                $model['divisionCode'] = $divisionCodes['sgbstdn_coll_code_1'] . '|' . $divisionCodes['sgbstdn_coll_code_2'];
            } else {
                $model['divisionCode'] = ($divisionCodes['sgbstdn_coll_code_1'] != '') ? $divisionCodes['sgbstdn_coll_code_1'] : $divisionCodes['sgbstdn_coll_code_2'];
            }
        }

        return $model;
    }

    /**
     * @param $residenceRecord
     * @return mixed
     */
    public function makeModelForResidence($residenceRecord, $uniqueId)
    {

        $model['pidm'] = $residenceRecord['szrsorm_pidm'];
        $model['termCode'] = $residenceRecord['szrsorm_term_code'];
        $model['termDesc'] = $residenceRecord['stvterm_desc'];
        $model['uniqueId']=$residenceRecord['szbuniq_unique_id'];
        $model['termStartDate'] = $residenceRecord['stvterm_start_date'];
        $model['termEndDate'] = $residenceRecord['stvterm_end_date'];
        $model['status'] = $residenceRecord['szrsorm_status'];
        $model['state'] = $residenceRecord['szrsorm_stat_code'];
        $model['country'] = $residenceRecord['szrsorm_natn_code'];
        $model['lastSeenDate'] = $residenceRecord['szrsorm_lastseen_date'];
        $model['lastConfirmedDate'] = $residenceRecord['szrsorm_lastconfirmed_date'];
        $model['readOnly'] = false;
        $model['adminStatus']= $residenceRecord['szrsorm_admin_status'];
        $model['comment'] = $residenceRecord['szrsorm_comment'];
        $model['priority'] = $residenceRecord['szrsorm_priority'];


        // Fetch the action trigger values from config manager
        $configData = $this->config->getConfiguration("ActionTrigger");

        $triggerValueAfterTermStart = $configData["triggerValueAfterTermStart"];

        $currentDate = date('Ymd');

        $configDataTermCode = $this->config->getConfiguration("CurrentTerm");

        $currentTermCode = $configDataTermCode[0];

        $queryStartDate = 'select STVTERM_START_DATE from STVTERM where STVTERM_CODE = ?';

        $startDateForTerm = $this->dbh->queryfirstcolumn($queryStartDate, $configDataTermCode);

        $startDateForTerm = new \DateTime($startDateForTerm);

        $triggerValueAfterTermStart = date_modify($startDateForTerm, $triggerValueAfterTermStart);

        $triggerValueAfterTermStart = date_format($triggerValueAfterTermStart, 'Ymd');

        if ($model['termCode'] <= $currentTermCode) {
            if ($currentDate > $triggerValueAfterTermStart) {
                $model['readOnly'] = true;
            }
        }

        $configData = $this->config->getConfiguration('CodeMap');
        $validCodes = array_keys( json_decode($configData['courseTypeDesc'], true) );

        $queryStringTerm = 'select sfrstcr_pidm, sfrstcr_term_code, sfrstcr_crn, stvcamp_desc,
                        ssbsect_subj_code      as course_subject,
                        ssbsect_crse_numb      as course_number,
                        ssbsect_camp_code      as campus_code,
                        trim(ssbsect_seq_numb) as course_section,
                        scbcrse_title as course_title,
                        ssrattr_attr_code as course_type,
                        stvattr_desc as course_type_desc
                        from sfrstcr, ssrattr, stvrsts, ssbsect, scbcrse, stvcamp, stvattr
                        where sfrstcr_pidm = ?
                        and sfrstcr_term_code = ?
                        and sfrstcr_crn = ssrattr_crn
                        and ssbsect_camp_code = stvcamp_code
                        and sfrstcr_term_code = ssrattr_term_code
                        and sfrstcr_rsts_code = stvrsts_code
                        and ssrattr_attr_code = stvattr_code
                        and ssrattr_attr_code in(\''. implode("','", $validCodes) .'\')
                        and stvrsts_incl_sect_enrl = \'Y\'
                        and stvrsts_withdraw_ind = \'N\'
                        and ssbsect_crn = sfrstcr_crn
                        and sfrstcr_term_code = ssbsect_term_code
                        and scbcrse_subj_code = ssbsect_subj_code
                        and scbcrse_crse_numb = ssbsect_crse_numb
                        and scbcrse_eff_term = (select max(scbcrse_eff_term)
                        from scbcrse where scbcrse_subj_code = ssbsect_subj_code
                        and scbcrse_crse_numb = ssbsect_crse_numb
                        and scbcrse_eff_term <= ssbsect_term_code)';

        $courseListRecords = $this->dbh->queryall_array($queryStringTerm, $model['pidm'], $model['termCode']);

		$model['message'] = '';
        if ($model['status'] === 'pending' && empty($courseListRecords)) {
            $info = [
            	'pidm' => $model['pidm'],
            	'termCode' => $model['termCode']
            ];

            $this->action->setDBH($this->dbh);

            $configData = $this->config->getConfiguration("StudentResidence");
            $model['status'] = 'cancel';
            try {
	        	$this->action->updateCancelAction($uniqueId, $info);

                // Fetch the message values from config manager
                $model['message'] = $configData['termCancelledForNoEnrollment'];
            } catch(\Exception $e) {
                // ToDo: We need to catch the exception and generate appropriate message.
                $model['message'] = $configData['main.loginMessageRemovalFailed'];
            }
        }

        for ($j = 0; $j < count($courseListRecords); $j++) {
            $model['courseList'][$j] = $this->makeModelForCourseList($courseListRecords[$j]);
        }

        return $model;
    }

    /**
     * @param $termRecord
     * @return mixed
     */
    public function makeModelForCourseList($termRecord)
    {
        $model['subjectCode'] = $termRecord['course_subject'];
        $model['courseNumber'] = $termRecord['course_number'];
        $model['courseSection'] = $termRecord['course_section'];
        $model['courseTitle'] = $termRecord['course_title'];
        $model['campusDesc'] = $termRecord['stvcamp_desc'];
        $model['courseType'] = $termRecord['course_type'];
        $model['courseTypeDesc'] = $this->mapCourseTypeCode($termRecord['course_type'], $termRecord['course_type_desc']);
        return $model;
    }

    private function mapCourseTypeCode($code, $defaultDesc)
    {
        if (null === $this->courseCodeMap) {
            try {
                $configData = $this->config->getConfiguration('CodeMap');
                $this->courseCodeMap = empty($configData['courseTypeDesc']) ? [] : json_decode($configData['courseTypeDesc'], true);
            } catch (\Exception $e) {
                $this->courseCodeMap = [];
            }
        }

        if (isset($this->courseCodeMap[$code])) {
            return $this->courseCodeMap[$code];
        }

        return $defaultDesc;
    }

    /**
     * @param $pidm
     * @return string
     */
    public function getProgramDescription($pidm)
    {
        $queryString = '
           select smrprle_program_desc
                from sgbstdn, smrprle
                where sgbstdn_pidm = ?
                    and sgbstdn_term_code_eff = (
                                                    select max(sgbstdn_term_code_eff)
                                                        from sgbstdn
                                                        where sgbstdn_pidm = ?
                                                )
                    and smrprle_program in (sgbstdn_program_1, sgbstdn_program_2)';

        return $this->dbh->queryall_list($queryString, $pidm, $pidm);
    }

}
