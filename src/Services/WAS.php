<?php
namespace MiamiOH\StateOfResidenceWebService\Services;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Service;
use MiamiOH\RESTng\Util\Configuration;
use MiamiOH\RESTng\Util\Response;

class WAS extends Service
{
    private $config;
    /** @var  Configuration $configuration */
    private $configuration;

    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
        $this->config = $this->configuration->getConfiguration('StateOfResidence', 'Internal Config');
    }

    /**
     * @param $uid
     * @param array $options
     * @return null
     * @throws \Exception
     */
    public function updateWAS($uid, $options = [])
    {
        if (!$this->config['wasClientUpdateUrl']) {
            throw new \Exception('Missing wasClientUpdateUrl for WAS update');
        }
        if (!$uid) {
            throw new \Exception('Missing uid for WAS update');
        }

        if ($options) {

            $queryParams = [];
            if (isset($options['infoRequired'])) {
                $queryParams['infoRequired'] = $options['infoRequired'] ? 1 : 0;
            }
            if (isset($options['complete'])) {
                $queryParams['complete'] = $options['complete'] ? 1 : 0;
            }

            if ($queryParams) {
                $logged = false;
                try {
                    $this->log->debug('sending direct notification for WAS');
                    $this->wasNotification($uid, $queryParams);
                    $logged = true;
                } catch (WasUpdateException $e) {
                    $this->log->debug('notification failed, sending deferred notification');
                }

                if (!$logged) {
                    try {
                        $deferred = $this->wasNotification($uid, $queryParams, true);
                        $this->log->debug('deferred notification sent');
                        $this->sendWASNotificationFailure(
                            $e->getMessage(),
                            array_merge(['uniqueId' => $uid], $queryParams),
                            $deferred->getPayload()
                        );
                    } catch (\Exception $e) {
                        $this->log->error('Deferred call to update WAS failed: ' . $e->getMessage());
                    }
                }
            }

        }
    }

    /**
     * @param $uid
     * @param $data
     * @param bool $deferred
     * @return Response
     * @throws WasUpdateException
     */
    private function wasNotification($uid, $data, $deferred = false)
    {
        try {
            $results = $this->callResource(
                'notification.v1.was.message.create',
                [
                    'params' => ['appKey' => 'sofr', 'uniqueId' => $uid],
                    'data' => $data,
                ],
                [
                    'deferred' => $deferred
                ]
            );

            if (!$deferred && $results->getStatus() !== App::API_CREATED) {
                throw new WasUpdateException('Failed to update WAS status ' . $results->getStatus());
            }
            if ($deferred && $results->getStatus() !== App::API_ACCEPTED) {
                throw new WasUpdateException('Failed to create deferred update call ' . $results->getStatus());
            }
        } catch (WasUpdateException $e) {
            $this->log->error("Call to 'notification.v1.was.message.create' failed: " . $e->getMessage());
            throw new WasUpdateException($e->getMessage());
        }

        return $results;
    }

    public function sendWASNotificationFailure($message, $params, $deferredCall)
    {
        $fromAddrForWASFailure = isset($configData['email.fromAddress']) ? $this->config['email.fromAddress'] : 'StateofResidence@miamioh.edu';

        $devTeam = isset($this->config['email.devTeam']) ? $this->config['email.devTeam'] : 'duit-soldel42@miamioh.edu';

        $priority = 2;

        $subject = 'WAS update failure';

        $body = "Updating WAS SofR status failed.\n\n";
        $body .= "Update params were: \n\n"
            . json_encode($params) . "\n\n";
        $body .= "Message:\n\n";
        $body .= "$message\n\n";

        $body .= 'A new deferred call was created (id: ' . $deferredCall['id'] .
            ', key: ' . $deferredCall['key'] . ') to retry this request later.';

        $sendResponse = $this->callResource('notification.v1.email.message.create',

            array(
                'data' => array(
                    'dataType' => 'model',
                    'data' => array(
                        'toAddr' => $devTeam,
                        'fromAddr' => $fromAddrForWASFailure,
                        'priority' => $priority,
                        'subject' => $subject,
                        'body' => $body,
                    ),
                )
            ));

        $status = $sendResponse->getStatus();

        if ($status != 201) {
            throw new \Exception("Send WAS notification to dev team failed with the status code as " . $status);
        }

        $payloadResponse = $sendResponse->getPayload();

    }

    public function getWASStatus($uid)
    {

        try {
            $results = $this->callResource(
                'notification.v1.was.message.id',
                [
                    'params' => ['appKey' => 'sofr', 'uniqueId' => $uid],
                    'options' => ['daysPast' => 180, 'attributes' => 'complete,infoRequired']
                ]
            );

            if ($results->getStatus() !== App::API_OK) {
                throw new WasUpdateException('Failed to get WAS status ' . $results->getStatus());
            }

            $payload = $results->getPayload();

            return !empty($payload['infoRequired']) && $payload['infoRequired'] === '1';

        } catch (WasUpdateException $e) {
            $this->log->error("Call to 'notification.v1.was.message.read' failed: " . $e->getMessage());
            throw new WasUpdateException($e->getMessage());
        }
    }

}