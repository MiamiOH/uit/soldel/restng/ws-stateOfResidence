<?php
/**
 * Created by PhpStorm.
 * User: ravendpp
 * Date: 11/22/16
 * Time: 2:17 PM
 */

namespace MiamiOH\StateOfResidenceWebService\Services;


use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Exception\BadRequest;
use MiamiOH\RESTng\Service;
use MiamiOH\RESTng\Util\Response;

class RuleService extends Service
{
    private $rule;

    /**
     * @param $rule
     */
    public function setRule($rule)
    {
        $this->rule = $rule;

    }

    public function getRuleList()
    {
        $this->log->debug('Start the rule GET service.');
        $request = $this->getRequest();
        $response = $this->getResponse();
        $payload = $this->rule->getRuleList();
        $response->setStatus(App::API_OK);
        $response->setPayload($payload);
        return $response;
    }

    /**
     * Delete Rule service to delete the rule from SZRSORR table
     */
    public function deleteRuleList()
    {
        $this->log->debug('Start the rule DELETE service.');
        $request = $this->getRequest();
        $response = $this->getResponse();
        $ruleInformation = $request->getData();

        foreach($ruleInformation as $ruleInfo) {
            $deleteRuleResult = [
                'request' => $ruleInfo,
                'status' => App::API_OK,
                'message' => '',
            ];

            if ((!isset($ruleInfo['ruleID']))) {
                $deleteRuleResult = [
                    'status' => App::API_BADREQUEST,
                    'message' => 'Please provide rule ID to delete',
                ];
            } else {
                try {
                    $this->rule->deleteRule($ruleInfo['ruleID']);

                } catch (\Exception $e) {
                    $this->log->error($e->getMessage());
                    $deleteRuleResult['status'] = App::API_FAILED;
                    $deleteRuleResult['message'] = $e->getMessage();
                }

            }
            $payload[] = $deleteRuleResult;
        }

        $response->setPayload($payload);
        $response->setStatus(App::API_OK);

        return $response;

    }

    public function deleteRule()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $ruleId = $request->getResourceParam('id');

        $rule = $this->rule->getRule($ruleId);

        if (empty($rule)) {
            $response->setStatus(App::API_NOTFOUND);
            return $response;
        }

        $this->rule->deleteRule($ruleId);

        $response->setStatus(App::API_OK);

        return $response;

    }

    /**
     * Update Rule Service  for PUT API
     * @return Response
     */
    public function updateRuleList()
    {
        $this->log->debug('Start the rule UPDATE service.');
        $request = $this->getRequest();
        $response = $this->getResponse();
        $ruleInformation = $request->getData();

        foreach($ruleInformation as $ruleInfo) {
            $updateRuleResult = [
                'request' => $ruleInfo,
                'status' => App::API_OK,
                'message' => '',
            ];


            if ((!isset($ruleInfo['ruleID']))) {
                $updateRuleResult = [
                    'status' => App::API_BADREQUEST,
                    'message' => 'Please provide the rule ID',
                ];
            } else {
                try {
                    $this->rule->updateRule($ruleInfo);

                } catch (\Exception $e) {
                    $this->log->error($e->getMessage());
                    $updateRuleResult['status'] = App::API_FAILED;
                    $updateRuleResult['message'] = $e->getMessage();
                }

            }
            $payload[] = $updateRuleResult;
        }

        $response->setPayload($payload);
        $response->setStatus(App::API_OK);

        return $response;
    }

    public function updateRule()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $ruleId = $request->getResourceParam('id');

        $ruleData = $request->getData();

        if ($ruleId !== $ruleData['ruleID']) {
            throw new BadRequest('Request and model id mismatch');
        }

        $rule = $this->rule->getRule($ruleId);

        if (empty($rule)) {
            $response->setStatus(App::API_NOTFOUND);
            return $response;
        }

        $this->rule->updateRule($ruleData);

        $response->setStatus(App::API_OK);

        return $response;
    }

    /**
     * Create Rule Service  for POST API
     * @return Response
     */
    public function createRuleList()
    {
        $this->log->debug('Start the rule CREATE service.');
        $request = $this->getRequest();
        $response = $this->getResponse();
        $ruleInformation = $request->getData();

        foreach($ruleInformation as $ruleInfo) {
            $updateRuleResult = [
                'rule' => $ruleInfo,
                'status' => App::API_OK,
                'message' => '',
            ];


            if (!isset($ruleInfo['allowedStates']) || !isset($ruleInfo['ruleValue']) || !isset($ruleInfo['attribute'])|| !isset($ruleInfo['ruleType'])) {
                $updateRuleResult = [
                    'status' => App::API_BADREQUEST,
                    'message' => 'Please provide all the required information',
                ];
            } else {
                try {
                    $updateRuleResult['rule'] = $this->rule->createRule($ruleInfo);

                } catch (\Exception $e) {
                    $this->log->error($e->getMessage());
                    $updateRuleResult['status'] = App::API_FAILED;
                    $updateRuleResult['message'] = $e->getMessage();
                }

            }
            $payload[] = $updateRuleResult;
        }

        $response->setPayload($payload);
        $response->setStatus(App::API_OK);

        return $response;
    }


}