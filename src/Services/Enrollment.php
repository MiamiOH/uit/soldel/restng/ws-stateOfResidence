<?php

namespace MiamiOH\StateOfResidenceWebService\Services;

use MiamiOH\RESTng\Service;

class Enrollment extends Service
{

    private $datasource_name = 'MUWS_GEN_PROD';
    private $dbh;
    private $sth;
    private $config;
    private $currentDate;

    private $daysUntilTermStartCache = [];
    private $waitDaysAfterEmail1 = 0;
    private $waitDaysAfterEmail2 = 0;

    /**
     * @param $database
     */
    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->datasource_name);
    }

    /**
     * @param $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    public function getEnrollment()
    {
        $configData = $this->config->getConfiguration("CurrentTerm");
        $currentTerm = $configData[0];

        $configData = $this->config->getConfiguration('CodeMap');
        $validCodes = array_keys( json_decode($configData['courseTypeDesc'], true) );
        for($i=0; $i<count($validCodes); $i++) {
            $validCodes[$i] = $this->dbh->escape($validCodes[$i]);
        }

        if(isset($configData['RHStartDate']) && $configData['RHStartDate'] != null) {
            $RHStartDate = $configData['RHStartDate'];
        } else {
            $RHStartDate = '18-JAN-21';
        }

        $queryString =
            "(  
                select distinct sfrstcr_pidm, sfrstcr_term_code
                from sfrstcr, ssrattr, stvrsts
                where sfrstcr_crn = ssrattr_crn
                and sfrstcr_term_code = ssrattr_term_code
                and sfrstcr_rsts_code = stvrsts_code
                and ssrattr_attr_code in('" . implode("','", $validCodes) . "')
                and stvrsts_incl_sect_enrl = 'Y'
                and stvrsts_withdraw_ind = 'N'
                and sfrstcr_term_code = 202115
                and sfrstcr_term_code >= ?
                and sfrstcr_rsts_code != 'WL'
                and not exists (
                    select spraddr_pidm from spraddr
                    where (spraddr_to_date is null or spraddr_to_date >= sysdate)
                    and (spraddr_from_date <= sysdate or spraddr_from_date is null )
                    and spraddr_pidm = sfrstcr_pidm
                    and (
                            (
                                spraddr_atyp_code in('LO', 'MA') 
                                and spraddr_stat_code = 'OH'
                            ) 
                           
                    )
                )
            ) 
            union (
                select distinct sfrstcr_pidm, sfrstcr_term_code
                from sfrstcr, ssrattr, stvrsts
                where sfrstcr_crn = ssrattr_crn
                and sfrstcr_term_code = ssrattr_term_code
                and sfrstcr_rsts_code = stvrsts_code
                and ssrattr_attr_code in('" . implode("','", $validCodes) . "')
                and stvrsts_incl_sect_enrl = 'Y'
                and stvrsts_withdraw_ind = 'N'
                and sfrstcr_term_code = 202120
                and sfrstcr_term_code >= ?
                and sfrstcr_rsts_code != 'WL'
                and not exists (
                    select spraddr_pidm from spraddr
                    where (spraddr_to_date is null or spraddr_to_date >= sysdate)
                    and (
                        spraddr_from_date  <= to_date(? , 'DD-MM-YY') 
                        or spraddr_from_date is null
                    )      
                    and spraddr_pidm = sfrstcr_pidm
                    and (
                            (
                                spraddr_atyp_code in('LO', 'MA') 
                                and spraddr_stat_code = 'OH'
                            ) 
                            or spraddr_atyp_code = 'RH'
                    )
                )
            ) union (
                select distinct sfrstcr_pidm, sfrstcr_term_code
                from sfrstcr, ssrattr, stvrsts
                where sfrstcr_crn = ssrattr_crn
                and sfrstcr_term_code = ssrattr_term_code
                and sfrstcr_rsts_code = stvrsts_code
                and ssrattr_attr_code in('" . implode("','", $validCodes) . "')
                and stvrsts_incl_sect_enrl = 'Y'
                and stvrsts_withdraw_ind = 'N'
                and sfrstcr_term_code >= 202130
                and sfrstcr_term_code >= ?
                and sfrstcr_rsts_code != 'WL'
            )";

        $records = $this->dbh->queryall_array($queryString, $currentTerm, $currentTerm, $RHStartDate, $currentTerm);

        $modelCollection = [];
        foreach($records as $record)
        {
            $model = [];
            $model['pidm'] = $record['sfrstcr_pidm'];
            $model['termCode'] = $record['sfrstcr_term_code'];
            $modelCollection[] = $model;
        }

        return $modelCollection;

    }

    public function getTrackedInfo()
    {
        $this->getActionTriggerValues();
        $this->currentDate = date('Ymd');
        $configData = $this->config->getConfiguration("CurrentTerm");
        $currentTerm = $configData[0];

        $this->sth = $this->dbh->prepare('
            with taction as 
             ( select szrsora_pidm, szrsora_term_code, szrsora_action_trigger as last_action, 
                    round(sysdate - szrsora_action_date, 0) as days_since_action 
                from szrsora a
                where szrsora_term_code >= ?
                  and szrsora_action_date = (
                                                 select max(szrsora_action_date)
                                                      from szrsora b
                                                      where a.szrsora_term_code = b.szrsora_term_code
                                                        and a.szrsora_pidm = b.szrsora_pidm
                                              )
            )
            select SZRSORM_PIDM as pidm, SZRSORM_TERM_CODE as term_code, 
                  to_char(SZRSORM_LASTSEEN_DATE, \'YYYYMMDD\') as last_seen_date, 
                  trunc(sysdate) - trunc(SZRSORM_LASTSEEN_DATE) as days_since_seen,
                  case when szrsorm_lastconfirmed_date >= trunc(sysdate) then 1 else 0 end as confirmed_today,
                  SZRSORM_STATUS as status, last_action, days_since_action
              from SZRSORM left outer join taction on szrsorm.szrsorm_pidm = taction.szrsora_pidm 
                    and szrsorm.szrsorm_term_code = taction.szrsora_term_code
              WHERE SZRSORM_TERM_CODE >= ?
            ');

        $this->sth->execute($currentTerm, $currentTerm);

        $records = array();
        while ($row = $this->sth->fetchrow_assoc()) {
            $record = $this->camelCaseKeys($row);
            $record['nextAction'] = $this->nextActionCalculation($record);
            $records[] = $record;
        }

        return $records;
    }

    public function updateConfirmedDate()
    {
        $configData = $this->config->getConfiguration("CurrentTerm");
        $currentTerm = $configData[0];

        $this->dbh->perform('
            update szrsorm set szrsorm_lastconfirmed_date = sysdate
            where szrsorm_term_code = ?
              and (szrsorm_lastconfirmed_date is null or trunc(szrsorm_lastconfirmed_date) < trunc(sysdate))
        ', $currentTerm);
    }

    public function getPastPendingInfo()
    {
        $configData = $this->config->getConfiguration("CurrentTerm");
        $currentTerm = $configData[0];
        
        $records = $this->dbh->queryall_array('select SZRSORM_PIDM, SZRSORM_TERM_CODE
                from SZRSORM WHERE SZRSORM_TERM_CODE < ? AND SZRSORM_STATUS = \'pending\'',$currentTerm);

        $modelCollection = [];
        foreach($records as $record)
        {
            $model = [];
            $model['pidm'] = $record['szrsorm_pidm'];
            $model['termCode'] = $record['szrsorm_term_code'];
            $modelCollection[] = $model;
        }

        return $modelCollection;

    }

    public function getDaysUntilTermStart($termCode)
    {
        if (!isset($this->daysUntilTermStartCache[$termCode])) {
            $this->daysUntilTermStartCache[$termCode] = $this->dbh->queryfirstcolumn('
                        select STVTERM_START_DATE - sysdate  
                            from STVTERM 
                            where STVTERM_CODE = ?
                    ', $termCode);
        }

        return $this->daysUntilTermStartCache[$termCode];
    }

    public function nextActionCalculation($studentTerm)
    {
        // Set it to default
        $nextAction = '';
        if($studentTerm['confirmedToday'] && $studentTerm['daysSinceSeen'] > 0
                && in_array($studentTerm['status'], ['pending', 'complete'])) {
            $nextAction = 'cancel';
        } elseif (!in_array($studentTerm['status'], ['cancel', 'complete'])) {
            if (empty($studentTerm['lastAction'])) {
                // Determine which notification flow to initiate

                $daysUntilTermStart = $this->getDaysUntilTermStart($studentTerm['termCode']);

                if ($daysUntilTermStart > $this->requireLoginMsgDaysBeforeTerm) {
                    $nextAction = 'email1';
                } else {
                    $nextAction = 'emailAndLoginMessage';
                }
            } else {
                // Determine the next step in the notification flow
                if ($studentTerm['lastAction'] === 'email1' &&
                        $studentTerm['daysSinceAction'] >= $this->waitDaysAfterEmail1) {
                    $nextAction = 'email2';
                } elseif ($studentTerm['lastAction'] === 'email2' &&
                    $studentTerm['daysSinceAction'] >= $this->waitDaysAfterEmail2) {
                    $nextAction = 'loginMessage';
                }
            }
        }

        return $nextAction;
    }

    public function getActionTriggerValues() {
        // Fetch the action trigger values from config manager
        $configData = $this->config->getConfiguration("ActionTrigger");
        $this->requireLoginMsgDaysBeforeTerm = $configData['requireLoginMsgDaysBeforeTerm'];
        $this->waitDaysAfterEmail1 = $configData['waitDaysAfterEmail1'];
        $this->waitDaysAfterEmail2 = $configData['waitDaysAfterEmail2'];
    }

}
