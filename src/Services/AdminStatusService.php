<?php
/**
 * Created by PhpStorm.
 * User: ravendpp
 * Date: 1/6/17
 * Time: 4:23 PM
 */

namespace MiamiOH\StateOfResidenceWebService\Services;

use MiamiOH\RESTng\Service;
use MiamiOH\RESTng\Service\Extension\BannerIdNotFound;
use MiamiOH\RESTng\App;

class AdminStatusService extends Service
{
    private $admin;
    private $adminStatus;

    /** @var Service\Extension\BannerUtil $bannerUtil */
    private $bannerUtil;

    /**
     * @param $adminStatus
     */
    public function setAdmin($admin)
    {

        $this->admin = $admin;

    }

    /**
     * @param $bannerUtil
     */
    public function setBannerUtil($bannerUtil)
    {
        /** @var Service\Extension\BannerUtil $bannerUtil */
        $this->bannerUtil = $bannerUtil;
    }

    public function updateAdminStatus()
    {

        $this->log->debug('Start the update admin status service.');
        $request = $this->getRequest();
        $response = $this->getResponse();
        $adminStatusInfo = $request->getData();

        foreach($adminStatusInfo as $adminInfo) {
            $updateStatusResult = [
                'request' => $adminInfo,
                'status' => App::API_OK,
                'message' => '',
            ];


                try {
                    $bannerId = $this->bannerUtil->getId($request->getResourceParamKey('muid'),
                        $request->getResourceParam('muid'));

                    $pidm = $bannerId->getPidm();

                    $this->admin->updateAdminStatus($pidm, $adminInfo);

                } catch (\Exception $e) {
                    $this->log->error($e->getMessage());
                    $updateStatusResult['status'] = App::API_FAILED;
                    $updateStatusResult['message'] = $e->getMessage();
                }  catch (BannerIdNotFound $e) {
                $this->log->info($e->getMessage());
                $response->setStatus(App::API_NOTFOUND);
            } catch (\Exception $e) {
                $this->log->error($e->getMessage());
                $response->setStatus(App::API_FAILED);
            }

            }
            $payload[] = $updateStatusResult;

        $response->setPayload($payload);
        $response->setStatus(App::API_OK);

        return $response;

    }

}