<?php

namespace MiamiOH\StateOfResidenceWebService\Services;

use MiamiOH\RESTng\Service;
use MiamiOH\RESTng\Util\Configuration;

class Config extends Service
{

    /** @var  Configuration $configuration */
    private $configuration;

    private $applicationName = 'StateOfResidence';
    private $categoryList = [
        'StudentResidence' => 'StudentResidence',
        'uiString' => 'StudentResidence',
        'test' => 'Test',
        'SendNotification' => 'SendNotification',
        'ActionTrigger' => 'ActionTrigger',
        'CodeMap' => 'CodeMap',
        'CurrentTerm' => '',
        'programDesc' => '',
        'majorDesc' => ''
    ];

    private $datasource_name = 'MUWS_GEN_PROD';

    private $dbh;

    private $config;

    /**
     * @param $database
     */
    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->datasource_name);
    }

    /**
     * @return array
     */
    public function getCategoryList()
    {
        return $this->categoryList;
    }

    /**
     * @param $configuration
     */
    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @param $category
     * @return mixed
     */
    public function getConfiguration($category)
    {

        if ($category === 'CurrentTerm') {
            $queryString = 'select fz_get_term from DUAL';

            $payload = $this->dbh->queryfirstrow_array($queryString);

            // This is to prevent processing "current" term during the initial
            // system start up. Remove after Summer 2017.
            if ($payload[0] === '201720') {
               $payload[0] = '201730';
            }

        } elseif ($category === 'programDesc') {
            $queryString = 'select smrprle_program, smrprle_program_desc from smrprle';

            $records = $this->dbh->queryall_array($queryString);
            $payload = [];
            for ($i = 0; $i < count($records); $i++) {
                $payload[$i]['program_code'] = $records[$i]['smrprle_program'];
                $payload[$i]['program_desc'] = $records[$i]['smrprle_program_desc'];
            }

        } elseif ($category === 'majorDesc') {
            $queryString = 'select stvmajr_code, stvmajr_desc from stvmajr';

            $records = $this->dbh->queryall_array($queryString);
            $payload = [];
            for ($i = 0; $i < count($records); $i++) {
                $payload[$i]['major_code'] = $records[$i]['stvmajr_code'];
                $payload[$i]['major_desc'] = $records[$i]['stvmajr_desc'];
            }
        } else {
            $payload = $this->configuration->getConfiguration($this->applicationName,
                $this->categoryList[$category]);
        }

        return $payload;
    }

}
