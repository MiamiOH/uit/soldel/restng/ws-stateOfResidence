<?php
/**
 * Created by PhpStorm.
 * User: ravendpp
 * Date: 11/2/16
 * Time: 2:27 PM
 */

namespace MiamiOH\StateOfResidenceWebService\Services;


use MiamiOH\RESTng\Service;

class TermInfo extends Service
{
    private $datasource_name = 'MUWS_GEN_PROD';

    private $dbh;

    private $config;

    /**
     * @param $database
     */
    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->datasource_name);
    }

    /**
     * @param $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }


    public function getTermInfo()
    {

        //To fetch begin term code for Mentis
        $configDataTermCode = $this->config->getConfiguration("ActionTrigger");
        $beginTermCode = $configDataTermCode['beginTerm'];

        // Updated start and end date to follow Mentis suggested format
        $queryString = 'select STVTERM_CODE,
          STVTERM_DESC,
          to_char(STVTERM_START_DATE,\'MM/DD/YYYY\') as STVTERM_START_DATE,
          to_char(STVTERM_END_DATE,\'MM/DD/YYYY\') as STVTERM_END_DATE 
          from STVTERM
          where STVTERM_CODE >= ? and STVTERM_CODE like \'20%\'';

        $termRecords = $this->dbh->queryall_array($queryString, $beginTermCode);

        for ($i = 0; $i < count($termRecords); $i++) {
            $records[$i] = $this->makeModelFromRecord($termRecords[$i]);
        }
        return $records;
    }

    private function makeModelFromRecord($record)
    {


        $model['termDesc'] = $record['stvterm_desc'];
//        $split = explode(" ",$description);
//
//        $model['semesterName'] = $split[0]. " ".$split [1];
//        $model['abbreviation'] = $split[0];
//        $model['sessionName'] = $split[2];
        $model['termCode'] = $record['stvterm_code'];
        $model['termStartDate'] = $record['stvterm_start_date'];
        $model['termEndDate'] = $record['stvterm_end_date'];
        $model['censusDate'] = '';

        return $model;
    }
}