<?php

namespace MiamiOH\StateOfResidenceWebService\Services;

use MiamiOH\RESTng\Service;
use RESTng\Service\StateOfResidence\WAS;

class Action extends Service
{


    private $datasource_name = 'MUWS_GEN_PROD';

    private $dbh;
    private $config;

    /** @var WAS $was */
    private $was;

    /**
     * @param $database
     */
    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->datasource_name);
    }

    /**
     * @param $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @param $was
     */
    public function setWas($was)
    {
        /** @var WAS $was */
        $this->was = $was;
    }

    public function setDBH($dbh)
    {
        $this->dbh = $dbh;
    }

    /**
     * Tracks the student by updating the lastSeenDate to current date. If entry for student for the term doesnt
     * exist,creates a new entry for the term and updates the lastSeenDate to current date.
     * @param $actInfo
     * @return bool
     */
    public function updateTrackAction($actInfo)
    {
        // Verify if there is an entry in the SZRSORM table,
        $recordExists = $this->dbh->queryfirstcolumn('
          SELECT count(*) 
            FROM SATURN.SZRSORM 
            WHERE szrsorm_pidm = ? AND szrsorm_term_code = ?
        ', $actInfo['pidm'], $actInfo['termCode']);

        // If the record exists, check the status. If no record, then insert one
        if ($recordExists) {
            $existingRecordDetails = $this->dbh->queryfirstrow_assoc('
                SELECT szrsorm_status as status,szrsorm_natn_code as nation 
                  FROM SATURN.SZRSORM 
                  WHERE szrsorm_pidm = ? AND szrsorm_term_code = ?
              ', $actInfo['pidm'], $actInfo['termCode']);

            // This method ensures tracking of the pidm/termcode
            // If already complete or still pending, just update last seen and confirmed dates
            // ElseIf cancelled and nation is not empty, assume complete
            // ElseIf cancelled reset to pending
            if (in_array($existingRecordDetails['status'], ['complete', 'pending'])) {
                $this->dbh->perform('
                  UPDATE SATURN.SZRSORM SET szrsorm_lastseen_date = sysdate, 
                      szrsorm_lastconfirmed_date = sysdate 
                    WHERE szrsorm_pidm = ?
                      AND szrsorm_term_code = ?
                ', $actInfo['pidm'], $actInfo['termCode']);
            } elseif ($existingRecordDetails['status'] == 'cancel' && $existingRecordDetails['nation'] != null ){
                $this->dbh->perform('
                  UPDATE SATURN.SZRSORM SET szrsorm_lastseen_date = sysdate, 
                      szrsorm_lastconfirmed_date = sysdate, szrsorm_status = ? 
                    WHERE szrsorm_pidm = ?
                      AND szrsorm_term_code = ?
                ', 'complete', $actInfo['pidm'], $actInfo['termCode']);
            } elseif ($existingRecordDetails['status'] == 'cancel') {
                $this->dbh->perform('
                  UPDATE SATURN.SZRSORM SET szrsorm_lastseen_date = sysdate, 
                      szrsorm_lastconfirmed_date = sysdate, szrsorm_status = ? 
                    WHERE szrsorm_pidm = ?
                      AND szrsorm_term_code = ?
                ', 'pending', $actInfo['pidm'], $actInfo['termCode']);

                // If the tracked enrollment is being reset to pending, then clear any
                // prior loginMessage action. It is possible that the login message was
                // previously set, then disabled as part of cancelling the enrollment.
                // If the student has re-enrolled, we must ensure the login message
                // is displayed.
                $this->dbh->perform('
                    delete from saturn.szrsora
                    where szrsora_pidm = ?
                      and szrsora_term_code = ?
                      and (szrsora_action_trigger = ? or szrsora_action_trigger = ?)
                ', $actInfo['pidm'], $actInfo['termCode'], 'loginMessage', 'emailAndLoginMessage');
            }

        } else {

            $this->dbh->perform('
              INSERT INTO SATURN.SZRSORM(szrsorm_pidm, szrsorm_term_code, szrsorm_status, szrsorm_lastseen_date,
                  szrsorm_lastconfirmed_date, szrsorm_priority) 
                VALUES (?, ?, ?, sysdate, sysdate, ?)
              ', $actInfo['pidm'], $actInfo['termCode'], 'pending', 5);
        }

        return true;
    }

    /**
     * This method updates the status of dropped out student to cancel.
     * @param $actInfo
     * @return bool
     */
    public function updateCancelAction($uniqueId, $actInfo)
    {

        $this->dbh->perform('
            UPDATE SATURN.SZRSORM SET szrsorm_status = ? 
            WHERE szrsorm_pidm = ? AND szrsorm_term_code = ?
            ', 'cancel', $actInfo['pidm'], $actInfo['termCode']);

        // If there are any tracked terms still pending, do not disable
        // the login message.
        $hasPending = $this->dbh->queryfirstcolumn('
            select count(*)
                from saturn.szrsorm
                where szrsorm_status = ?
                  and szrsorm_pidm = ?
        ', 'pending', $actInfo['pidm']);

        $this->log->debug("$uniqueId has $hasPending pending terms");
        if (!$hasPending) {
            // calls the WAS class to update the status in WAS for the student //
            $this->was->updateWAS($uniqueId, ['complete' => false, 'infoRequired' => false]);
            $this->log->debug("Updated WAS to infoRequired=false for $uniqueId");
        }
        
        return true;
    }

    /**
     * This method updates the status of student who have provided state/country information to "complete"
     * @param $actInfo
     * @return bool
     */
    public function updateCompleteAction($pidm, $uniqueId, $termCode)
    {

        $this->dbh->perform('
            UPDATE SATURN.SZRSORM SET szrsorm_status = ? 
            WHERE szrsorm_pidm = ? AND szrsorm_term_code = ?
          ', 'complete', $pidm, $termCode);

        // calls the WAS class to update the status in WAS for the student //
        $this->was->updateWAS($uniqueId, ['complete' => true, 'infoRequired' => false]);
        
        return true;
    }

    public function updateLoginMessageAction($uniqueId, $actInfo)
    {
        $this->updateLoginMessageWas($uniqueId, $actInfo, ['complete' => false, 'infoRequired' => true]);

        $this->updateAction($actInfo['pidm'], $actInfo['termCode'], 'loginMessage', 'was');

        return true;
    }


    /**
     * This method is used to send email to the student. Email content varies based on  email type.
     * Also it updates the SZRSORA table with action status.
     * @param $emailType
     * @param $uniqueId
     * @param $actInfo
     * @throws \Exception
     */
    public function updateEmailAction($emailType, $uniqueId, $actInfo)
    {
        /*
         * Only send one email per day, even if multiple terms qualify. If
         * an email was sent today, log this action without sending another
         * email.
         */
        $hasEmailedToday = $this->dbh->queryfirstcolumn('
            select count(*) 
              from szrsora 
              where szrsora_pidm = ? 
                and szrsora_action like ?
                and szrsora_action_date >= TRUNC(sysdate)
        ', $actInfo['pidm'], 'email%');

        if ($hasEmailedToday) {
            $this->updateAction($actInfo['pidm'], $actInfo['termCode'],
                $emailType == 'email' ? 'emailAndLoginMessage' : $emailType,
                $emailType, '0000');
            return true;
        }

        $toAddress = $uniqueId . '@miamioh.edu';
        $priority = 2;

        $configData = $this->config->getConfiguration("SendNotification");

        $fromAddress = isset($configData['email.fromAddress']) ? $configData['email.fromAddress'] : 'StateofResidence@miamioh.edu';
        // Get subject and body from ConfigMr based on email type(email1 or email2)
        $subject = $configData[$emailType . '.subject'];
        $body = $configData[$emailType . '.body'];

        $records = $this->dbh->queryfirstrow_assoc('
            select spriden_id, spriden_last_name,
                coalesce(spbpers_pref_first_name, spriden_first_name) as spriden_first_name 
              from spriden 
                inner join szbuniq on (szbuniq_banner_id = spriden_id)
                inner join spbpers on (spriden_pidm = spbpers_pidm)
              where szbuniq_unique_id = ?
                and spriden_change_ind is null
        ', strtoupper($uniqueId));

        $termDesc = $this->dbh->queryfirstcolumn('
          select stvterm_desc 
            from stvterm 
            where stvterm_code = ?
        ', $actInfo['termCode']);

        $replacements = [
            'uniqueId' => $uniqueId,
            'termCode' => $actInfo['termCode'],
            'firstName' => $records['spriden_first_name'],
            'lastName' => $records['spriden_last_name'],
            'bannerId' => $records['spriden_id'],
            'termName' => $termDesc,
        ];

        if ($replacements) {
            if (!is_array($replacements)) {
                throw new \Exception('Replacement argument for sendEmail must be an associative array');
            }

            foreach ($replacements as $key => $value) {

                $subject = str_replace("{" . $key . "}", $value, $subject);
                $body = str_replace("{" . $key . "}", $value, $body);
            }
        }

        $sendResponse = $this->callResource('notification.v1.email.message.create',
            array(
                'data' => array(
                    'dataType' => 'model',
                    'data' => array(
                        'toAddr' => $toAddress,
                        'fromAddr' => $fromAddress,
                        'priority' => $priority,
                        'subject' => $subject,
                        'body' => $body,
                    ),
                )
            ));

        $status = $sendResponse->getStatus();

        if ($status != 201) {
            throw new \Exception("Send email notification failed with the status code as " . $status);
        }

        $payloadResponse = $sendResponse->getPayload();

        //Action trigger and action values are same for email notification in SZRSORA table except for
        // email where trigger is emailAndLoginMessage and action is email
        $this->updateAction($actInfo['pidm'], $actInfo['termCode'],
            $emailType === 'email' ? 'emailAndLoginMessage' : $emailType,
            $emailType, $payloadResponse['id']);

        return true;

    }

    public function updateAction($pidm, $termCode, $trigger, $action, $id = null)
    {
        $this->dbh->perform('
            INSERT INTO saturn.szrsora(szrsora_pidm, szrsora_term_code, SZRSORA_ACTION_TRIGGER, SZRSORA_ACTION_DATE,
                          SZRSORA_ACTION, szrsora_message_id) 
            VALUES (?, ?, ?, sysdate, ?, ?)
          ', $pidm, $termCode, $trigger, $action, $id);

        return true;
    }

    public function updateEmailAndLoginMessage($uniqueId, $actInfo)
    {
        $this->updateEmailAction('email', $uniqueId, $actInfo);

        $this->updateLoginMessageWas($uniqueId, $actInfo, ['complete' => false, 'infoRequired' => true]);

        $this->updateAction($actInfo['pidm'], $actInfo['termCode'], 'emailAndLoginMessage', 'was');

        return true;
    }

    private function updateLoginMessageWas($uniqueId, $actInfo, $options)
    {
        // Only enable the login message if the target term is gte the min term
        $configData = $this->config->getConfiguration('ActionTrigger');

        if (empty($configData['minLoginMessageTerm']) || $actInfo['termCode'] < $configData['minLoginMessageTerm']) {
            return true;
        }

        // calls the WAS class to update the status in WAS for the student //
        $this->was->updateWAS($uniqueId, $options);
    }
    
}

