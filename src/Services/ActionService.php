<?php

namespace MiamiOH\StateOfResidenceWebService\Services;


use MiamiOH\RESTng\Exception\BadRequest;
use MiamiOH\RESTng\Service;
use MiamiOH\RESTng\Service\Extension\BannerUtil;
use MiamiOH\RESTng\Service\Extension\BannerIdNotFound;
use MiamiOH\RESTng\App;

class ActionService extends Service
{
    /** @var Action $action */
    private $action;

    /** @var BannerUtil $bannerUtil */
    private $bannerUtil;

    /**
     * @param $action
     */
    public function setAction($action)
    {

        $this->action = $action;

    }

    public function setBannerUtil($bannerUtil)
    {
        /** @var BannerUtil $bannerUtil */
        $this->bannerUtil = $bannerUtil;
    }

    /**
     * This method performs the action based on action type.
     *"track" - It updates the lastSeenDate  of a student to for the term to current date if exist.
     * If the student is newly registered, then adds the student entry into szrsorm table and updates
     * the lastSeenDate to current date.
     * "cancel" - It updates the status of collection of students to cancel in szrsorm table
     * @return mixed
     */
    public function updateActionCollection()
    {
        $this->log->debug('Start the student collection action update service.');
        $request = $this->getRequest();
        $response = $this->getResponse();

        $action = $request->getResourceParam('action');
        $actionInformation = $request->getData();
        

        foreach ($actionInformation as $actInfo) {
            $updateActionResult = [
                'request' => $actInfo,
                'status' => App::API_OK,
                'message' => '',
            ];

            if (!isset($actInfo['pidm']) || !isset($actInfo['termCode'])) {
                $updateActionResult['status'] = App::API_BADREQUEST;
                $updateActionResult['message'] = 'Please provide pidm and term code';

            } else {
                try {
                    $bannerId = $this->bannerUtil->getId('pidm', $actInfo['pidm']);

                    $uniqueId = $bannerId->getUniqueId();

                    switch ($action) {
                        case "track":
                            $this->action->updateTrackAction($actInfo);
                            break;
                        case "cancel":
                            $this->action->updateCancelAction($uniqueId, $actInfo);
                            break;
                        case "complete":
                            $this->action->updateCompleteAction($actInfo['pidm'], $uniqueId, $actInfo['termCode']);
                            break;
                        case "email1":
                            $this->action->updateEmailAction("email1", $uniqueId, $actInfo);
                            break;
                        case "email2":
                            $this->action->updateEmailAction("email2", $uniqueId, $actInfo);
                            break;
                        case "loginMessage":
                            $this->action->updateLoginMessageAction($uniqueId, $actInfo);
                            break;
                        case "emailAndLoginMessage":
                            $this->action->updateEmailAndLoginMessage($uniqueId, $actInfo);
                            break;
                    }

                } catch (BannerIdNotFound $e) {
                    $this->log->info($e->getMessage());
                    $updateActionResult['status'] = App::API_NOTFOUND;
                    $updateActionResult['message'] = $e->getMessage();
                } catch (BadRequest $e) {
                    $this->log->info($e->getMessage());
                    $updateActionResult['status'] = App::API_BADREQUEST;
                    $updateActionResult['message'] = $e->getMessage();
                } catch (\Exception $e) {
                    $this->log->error($e->getMessage());
                    $updateActionResult['status'] = App::API_FAILED;
                    $updateActionResult['message'] = $e->getMessage();
                }
            }
            $payload[] = $updateActionResult;
        }


        $response->setPayload($payload);
        $response->setStatus(App::API_OK);


        return $response;

    }

    public function updateAction()
    {
        $this->log->debug('Start the student action update service.');
        $request = $this->getRequest();
        $response = $this->getResponse();

        $actionInformation = $request->getData();


        foreach ($actionInformation as $actInfo) {
            $updateActionResult = [
                'request' => $actInfo,
                'status' => App::API_OK,
                'message' => '',
            ];

            if (!isset($actInfo['pidm']) || !isset($actInfo['termCode']) || !isset($actInfo['nextAction'])) {
                $updateActionResult = [
                    'status' => App::API_BADREQUEST,
                    'message' => 'Please provide pidm, term code and nextAction',
                ];
            } else {

                try {
                    $bannerId = $this->bannerUtil->getId('pidm', $actInfo['pidm']);

                    $uniqueId = $bannerId->getUniqueId();
                    
                    switch ($actInfo['nextAction']) {
                        case "track":
                            $this->action->updateTrackAction($actInfo);
                            break;
                        case "cancel":
                            $this->action->updateCancelAction($uniqueId, $actInfo);
                            break;
                        case "complete":
                            $this->action->updateCompleteAction($actInfo['pidm'], $uniqueId, $actInfo['termCode']);
                            break;
                        case "email1":
                            $this->action->updateEmailAction("email1", $uniqueId, $actInfo);
                            break;
                        case "email2":
                            $this->action->updateEmailAction("email2", $uniqueId, $actInfo);
                            break;
                        case "loginMessage":
                            $this->action->updateLoginMessageAction($uniqueId, $actInfo);
                            break;
                        case "emailAndLoginMessage":
                            $this->action->updateEmailAndLoginMessage($uniqueId, $actInfo);
                            break;
                    }

                } catch (BannerIdNotFound $e) {
                    $this->log->info($e->getMessage());
                    $updateActionResult['status'] = App::API_NOTFOUND;
                    $updateActionResult['message'] = $e->getMessage();
                } catch (BadRequest $e) {
                    $this->log->info($e->getMessage());
                    $updateActionResult['status'] = App::API_BADREQUEST;
                    $updateActionResult['message'] = $e->getMessage();
                } catch (\Exception $e) {
                    $this->log->error($e->getMessage());
                    $updateActionResult['status'] = App::API_FAILED;
                    $updateActionResult['message'] = $e->getMessage();
                }
            }
            $payload[] = $updateActionResult;
        }


        $response->setPayload($payload);
        $response->setStatus(App::API_OK);


        return $response;

    }
}