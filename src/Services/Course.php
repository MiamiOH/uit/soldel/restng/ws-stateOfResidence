<?php

namespace MiamiOH\StateOfResidenceWebService\Services;


use MiamiOH\RESTng\Service;

class Course extends Service
{

    private $datasource_name = 'MUWS_GEN_PROD';

    private $dbh;

    private $config;

    /**
     * @param $database
     */
    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->datasource_name);
    }


    /**
     * @param $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return mixed
     */
    public function getCourseList()
    {

        //To fetch begin term code for Mentis
        $configDataTermCode = $this->config->getConfiguration("ActionTrigger");
        $beginTermCode = $configDataTermCode['beginTerm'];

        $configData = $this->config->getConfiguration('CodeMap');
        $validCodes = array_keys( json_decode($configData['courseTypeDesc'], true) );

        $queryString = 'select scbcrse_title,scbcrse_subj_code,scbcrse_crse_numb,
                        ssbsect_camp_code,stvcamp_desc,scrlevl_levl_code,ssbsect_term_code,ssbsect_crn,
                        ssrattr_attr_code as instructional_method,
                        trim(ssbsect_seq_numb) as course_section
                        from stvcamp,ssbsect,ssrattr,scbcrse,scrlevl 
                        where scbcrse_subj_code = ssbsect_subj_code
                            and scbcrse_crse_numb = ssbsect_crse_numb
                            and ssrattr_crn = ssbsect_crn
                            and ssrattr_term_code = ssbsect_term_code
                            and ssrattr_attr_code in(\''. implode("','", $validCodes) . '\')
                            and stvcamp_code = ssbsect_camp_code
                            and scbcrse_crse_numb = scrlevl_crse_numb
                            and scbcrse_subj_code = scrlevl_subj_code
                            and ssbsect_term_code >= ?
                            and scbcrse_eff_term = 
                            (select max(scbcrse_eff_term) 
                            from scbcrse where scbcrse_subj_code = ssbsect_subj_code 
                            and scbcrse_crse_numb = ssbsect_crse_numb 
                            and scbcrse_eff_term <= ssbsect_term_code)';

        $records = $this->dbh->queryall_array($queryString, $beginTermCode);

        for ($i = 0; $i < count($records); $i++) {

            $records[$i] = $this->makeModelForCourseList($records[$i]);
        }


        return $records;

    }

    public function makeModelForCourseList($record)
    {

        $model['courseTitle'] = $record['scbcrse_title'];
        $model['subjectCode'] = $record['scbcrse_subj_code'];
        $model['courseNumber'] = $record['scbcrse_crse_numb'];
        $model['campusDesc'] = $record['stvcamp_desc'];
        $model['campusCode'] = $record['ssbsect_camp_code'];
        $model['termCode'] = $record['ssbsect_term_code'];
        $model['crn'] = $record['ssbsect_crn'];
        $model['instructionalMethod'] = $record['instructional_method'];
        $model['courseSection'] = $record['course_section'];

        if ($record['scrlevl_levl_code'] !== 'CR') {
            $model['scheduleType'] = "credit";
        } else {
            $model['scheduleType'] = "non-credit";
        }

        return $model;

    }

}