<?php

namespace MiamiOH\StateOfResidenceWebService\Services;


use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Service;
use MiamiOH\RESTng\Util\Response;

class ProfileService extends Service
{
    /** @var Profile $profile */
    private $profile;

    /** @var Service\Extension\BannerUtil $bannerUtil */
    private $bannerUtil;

    /**
     * @param $profile
     */
    public function setProfile($profile)
    {

        $this->profile = $profile;

    }

    /**
     * @param $bannerUtil
     */
    public function setBannerUtil($bannerUtil)
    {
        /** @var Service\Extension\BannerUtil $bannerUtil */
        $this->bannerUtil = $bannerUtil;
    }


    /**
     * This method updates the student state/country information for the term.
     * @return mixed
     */
    public function updateProfile()
    {

        $this->log->debug('Start the profile update service.');
        $request = $this->getRequest();
        $response = $this->getResponse();
        $updateProfileInfo = $request->getData();

        try {
            $bannerId = $this->bannerUtil->getId($request->getResourceParamKey('muid'),
                $request->getResourceParam('muid'));

            $pidm = $bannerId->getPidm();

            $this->profile->updateProfile($pidm, $updateProfileInfo);

            $response->setStatus(App::API_OK);

        } catch (Service\Extension\BannerIdNotFound $e) {
            $this->log->info($e->getMessage());
            $response->setStatus(App::API_NOTFOUND);
        } catch (\Exception $e) {
            $this->log->error($e->getMessage());
            $response->setStatus(App::API_FAILED);
        }
        return $response;

    }

    /**
     * @return Response
     */
    public function getProfile()
    {

        $this->log->debug('Start the profile read service.');
        $request = $this->getRequest();
        $response = $this->getResponse();


        try {
            $bannerId = $this->bannerUtil->getId($request->getResourceParamKey('muid'),
                $request->getResourceParam('muid'));

            $pidm = $bannerId->getPidm();

            $payload = $this->profile->getProfile($pidm);

            $response->setStatus(App::API_OK);
            $response->setPayload($payload);


        } catch (Service\Extension\BannerIdNotFound $e) {
            $this->log->info($e->getMessage());
            $response->setStatus(App::API_NOTFOUND);
        } catch (\Exception $e) {
            $this->log->error($e->getMessage());
            $response->setStatus(App::API_FAILED);
        }
        return $response;


    }

    public function Audit()
    {
        $this->log->debug('Start the profile service.');
        $request = $this->getRequest();
        $response = $this->getResponse();

        $response->setStatus(App::API_NOTIMPLEMENTED);
        return $response;
    }


}