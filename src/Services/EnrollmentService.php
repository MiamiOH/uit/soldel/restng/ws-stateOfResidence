<?php

namespace MiamiOH\StateOfResidenceWebService\Services;

use MiamiOH\RESTng\Service;
use MiamiOH\RESTng\App;

class EnrollmentService extends Service
{


    private $enrollment;

    public function setEnrollmentService($enrollment)
    {
        
        $this->enrollment = $enrollment;

    }


    public function getEnrollmentInfo()
    {
        
        $this->log->debug('Start the Enrollment collection service.');
        $request = $this->getRequest();
        $response = $this->getResponse();

        $payload = $this->enrollment->getEnrollment();

        $response->setStatus(App::API_OK);
        $response->setPayload($payload);

        return $response;
    }

    public function getTrackedInfo()
    {
        
        $this->log->debug('Start the Track information service.');
        $request = $this->getRequest();
        $response = $this->getResponse();


        $payload = $this->enrollment->getTrackedInfo();

        $response->setStatus(App::API_OK);
        $response->setPayload($payload);
        
        return $response;
    }

    public function updateConfirmedDate() {
        $response = $this->getResponse();

        $this->enrollment->updateConfirmedDate();

        $response->setStatus(App::API_OK);

        return $response;
    }

    public function getPastPendingInfo()
    {
        
        $this->log->debug('Start the Track Pending service.');
        $request = $this->getRequest();
        $response = $this->getResponse();

        $payload = $this->enrollment->getPastPendingInfo();

        $response->setStatus(App::API_OK);
        $response->setPayload($payload);
        
        return $response;
    }
}