<?php
/**
 * Created by PhpStorm.
 * User: ravendpp
 * Date: 11/22/16
 * Time: 2:17 PM
 */

namespace MiamiOH\StateOfResidenceWebService\Services;


use MiamiOH\RESTng\Service;

class Rule extends Service
{
    private $datasource_name = 'MUWS_GEN_PROD';

    private $dbh;

    /**
     * @param $database
     */
    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->datasource_name);
    }

    /**
     *
     */
    public function getRuleList()
    {
        $queryString = 'select szrsorr_rule_id,szrsorr_rule_type,szrsorr_attribute,szrsorr_allowed_states,szrsorr_rule_value from szrsorr';

        $ruleRecords = $this->dbh->queryall_array($queryString);

        $records = [];
        for ($i = 0; $i < count($ruleRecords); $i++) {
            $records[$i] = $this->makeModelFromRecords($ruleRecords[$i]);
        }

        return $records;
    }

    public function getRule($id)
    {
        $queryString = '
            select szrsorr_rule_id, szrsorr_rule_type, szrsorr_attribute, szrsorr_allowed_states, szrsorr_rule_value 
              from szrsorr
              where szrsorr_rule_id = ?
            ';

        $ruleRecord = $this->dbh->queryfirstrow_assoc($queryString, $id);

        $rule = null;
        if ($ruleRecord !== DB_EMPTY_SET) {
            $rule = $this->makeModelFromRecords($ruleRecord);
        }

        return $rule;
    }

    /**
     * @param $record
     * @return mixed
     */
    private function makeModelFromRecords($record)
    {
        $model['ruleID'] = $record['szrsorr_rule_id'];
        $model['ruleType'] = $record['szrsorr_rule_type'];
        $model['attribute'] = $record['szrsorr_attribute'];
        $model['allowedStates'] = $record['szrsorr_allowed_states'];
        $model['ruleValue'] = $record['szrsorr_rule_value'];

        return $model;
    }

    /**
     * Delete the rule from SZRSORR table
     * @param $ruleType
     * @param $ruleAttribute
     * @return boolean
     */
    public function deleteRule($ruleID)
    {
        $this->dbh->perform('delete from szrsorr where szrsorr_rule_id = ?',
            $ruleID);

        return true;
    }

    /**
     * Update rule for SZRSORR table
     * @param $ruleInfo
     * @return bool
     */
    public function updateRule($ruleInfo)
    {
        $userName = $this->getApiUser()->getUsername();

        $queryString ='UPDATE szrsorr set szrsorr_rule_type = ?, szrsorr_attribute = ?, szrsorr_allowed_states = ?, szrsorr_rule_value = ?, szrsorr_activity_date = sysdate, 
                       szrsorr_user_id = ? where szrsorr_rule_id = ?';

        $params = array($ruleInfo['ruleType'], $ruleInfo['attribute'], strtoupper( str_replace(',', '|', $ruleInfo['allowedStates'])), $ruleInfo['ruleValue'], $userName, $ruleInfo['ruleID']);

        $this->dbh->perform($queryString,$params);

        return true;
       
    }


    /**
     * Create rule for SZRSORR table
     * @param $ruleInfo
     * @return bool
     */
    public function createRule($ruleInfo)
    {

        $userName = $this->getApiUser()->getUsername();

        $ruleID = $this->dbh->queryfirstcolumn('select max(szrsorr_rule_id) from szrsorr');

        $queryString ='insert into szrsorr (szrsorr_rule_id, szrsorr_rule_type, szrsorr_attribute, szrsorr_allowed_states, szrsorr_rule_value, szrsorr_activity_date, szrsorr_user_id) 
          values (?,?,?,?,?,sysdate,?)';

        $params = array($ruleID+1, $ruleInfo['ruleType'],$ruleInfo['attribute'],strtoupper($ruleInfo['allowedStates']),$ruleInfo['ruleValue'], $userName);

        $this->dbh->perform($queryString,$params);

        $ruleInfo['ruleID'] = $ruleID+1;

        return $ruleInfo;

    }


}