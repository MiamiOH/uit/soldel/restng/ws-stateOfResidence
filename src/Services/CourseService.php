<?php

namespace MiamiOH\StateOfResidenceWebService\Services;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Service;

class CourseService extends Service
{

    private $course;

    public function setCourse($course)
    {

        $this->course = $course;

    }

    public function getCourseList()
    {

        $this->log->debug('Start the Course List service.');
        $request = $this->getRequest();
        $response = $this->getResponse();

        $payload = $this->course->getCourseList();

        $response->setStatus(App::API_OK);
        $response->setPayload($payload);


        return $response;
    }
}