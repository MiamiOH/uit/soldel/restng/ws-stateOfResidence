<?php

namespace MiamiOH\StateOfResidenceWebService\Services;

use MiamiOH\RESTng\Exception\BadRequest;
use MiamiOH\RESTng\Service;
use MiamiOH\RESTng\App;

class ConfigService extends Service
{

    /** @var  Config $config */
    private $config;

    /**
     * @param $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return mixed
     * @throws BadRequest
     */
    public function getConfiguration()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $options = $request->getOptions();

        if (!isset($options['category'])) {
            throw new BadRequest('Missing required configuration category');
        }

        $categoryList = $this->config->getCategoryList();
        if (!isset($categoryList[$options['category']])) {
            throw new BadRequest('Invalid configuration category');
        }

        $payload = $this->config->getConfiguration($options['category']);

        $response->setStatus(App::API_OK);
        $response->setPayload($payload);

        return $response;
    }

}
