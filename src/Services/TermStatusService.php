<?php

namespace MiamiOH\StateOfResidenceWebService\Services;

use MiamiOH\RESTng\Exception\BadRequest;
use MiamiOH\RESTng\Service;
use MiamiOH\RESTng\App;

class TermStatusService extends Service
{

    private $termStatus;

    public function setTermStatus($termStatus)
    {

        $this->termStatus = $termStatus;

    }

    /**
     * @param $bannerUtil
     */
    public function setBannerUtil($bannerUtil)
    {
        /** @var Service\Extension\BannerUtil $bannerUtil */
        $this->bannerUtil = $bannerUtil;
    }

    public function getTermStatus()
    {
        $this->log->debug('Start the Term status service.');
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();
        try {
            $bannerId = $this->bannerUtil->getId($request->getResourceParamKey('muid'),
                $request->getResourceParam('muid'));

            $pidm = $bannerId->getPidm();

            if (!isset($options['termCode'])) {
                throw new BadRequest('Missing required term code field ');
            }

            if (!isset($options['country'])) {
                throw new BadRequest('Missing required country field ');
            }

            $payload = $this->termStatus->getTermStatus($pidm, $options);
            $response->setStatus(App::API_OK);
            $response->setPayload($payload);

        } catch (\Exception $e) {
            $this->log->error($e->getMessage());
            $response->setStatus(App::API_FAILED);
        }
        return $response;
    }
}