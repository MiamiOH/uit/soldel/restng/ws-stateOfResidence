<?php

namespace MiamiOH\StateOfResidenceWebService\Services;


use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Exception\BadRequest;
use MiamiOH\RESTng\Service;

class ReportService extends Service
{
    /** @var  Report $report */
    private $report;

    /**
     * @param $report
     */
    public function setReport($report)
    {
        $this->report = $report;
    }

    public function createEnrollmentReport()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        try {
            $response->setPayload($this->report->createEnrollmentReport());
            $response->setStatus(App::API_CREATED);
        } catch (\Exception $e) {
            $this->log->error($e->getMessage());
            $response->setStatus(App::API_FAILED);
        }

        return $response;

    }

    public function updateEnrollmentReport()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        try {
            $this->report->updateEnrollmentReport();
            $response->setStatus(App::API_OK);
        } catch (\Exception $e) {
            $this->log->error($e->getMessage());
            $response->setStatus(App::API_FAILED);
        }

        return $response;

    }

    public function getEnrollmentReport()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $options = $request->getOptions();

        if (!isset($options['key'])) {
            throw new BadRequest('Missing required key field ');
        }

        try {
            $isPaged = $request->isPaged();
            $offset = $request->getOffset();
            $limit = $request->getLimit();
            $payload = $this->report->getEnrollmentReport($options['key'], $isPaged, $offset, $limit);
            $totalRecords = $this->report->getTotalRecords();
            $response->setTotalObjects($totalRecords);
            $response->setStatus(App::API_OK);
            $response->setPayload($payload);
        } catch (ReportPendingException $e) {
            $response->setStatus(428);
        } catch (\Exception $e) {
            $this->log->error($e->getMessage());
            $response->setStatus(App::API_FAILED);
        }

        return $response;

    }

}