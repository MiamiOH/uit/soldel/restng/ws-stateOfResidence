<?php
/**
 * Created by PhpStorm.
 * User: ravendpp
 * Date: 11/2/16
 * Time: 2:26 PM
 */

namespace MiamiOH\StateOfResidenceWebService\Services;


use MiamiOH\RESTng\Service;
use MiamiOH\RESTng\App;

class TermInfoService extends Service
{

    private $term;

    public function setTerm($term)
    {

        $this->term = $term;

    }

    public function getTermInfo()
    {
        $this->log->debug('Start the Term Info service.');
        $request = $this->getRequest();
        $response = $this->getResponse();
        try {

            $payload = $this->term->getTermInfo();
            $response->setStatus(App::API_OK);
            $response->setPayload($payload);

        } catch (\Exception $e) {
            $this->log->error($e->getMessage());
            $response->setStatus(App::API_FAILED);
        }
        return $response;
    }
}