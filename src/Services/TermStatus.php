<?php

namespace MiamiOH\StateOfResidenceWebService\Services;

use MiamiOH\RESTng\Service;

class TermStatus extends Service
{
    private $datasource_name = 'MUWS_GEN_PROD';

    private $dbh;

    private $studentProgram = [];

    /**
     * @param $database
     */
    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->datasource_name);
    }

    /**
     * @param $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @param $pidm
     * @param $options
     * @return bool
     */
    public function getTermStatus($pidm, $options)
    {
        $records['termCode'] = $options['termCode'];

        if ($options['country'] === 'US' && !isset($options['state'])) {
            $records['status'] = true;
            $records['priority'] = 5;
        } elseif ($options['country'] !== 'US' && !isset($options['state'])) {
            $records['status'] = true;
            $records['priority'] = 5;
        } else {

            $queryStringTerm = 'select SGBSTDN_PROGRAM_1,SGBSTDN_PROGRAM_2,SGBSTDN_MAJR_CODE_1, 
                            SGBSTDN_MAJR_CODE_2,SGBSTDN_MAJR_CODE_1_2,SGBSTDN_MAJR_CODE_2_2
                            from SGBSTDN where SGBSTDN_PIDM = ? and SGBSTDN_TERM_CODE_EFF = 
                            (select max(SGBSTDN_TERM_CODE_EFF) from SGBSTDN where SGBSTDN_PIDM = ?)';

            $recordProgramMajor = $this->dbh->queryfirstrow_assoc($queryStringTerm, $pidm, $pidm);

            $program = array();
            $major = array();
            $program['programCode_1'] = '';
            $program['programCode_2'] = '';
            $major['majorCode_1'] = '';
            $major['majorCode_1_2'] = '';
            $major['majorCode_2'] = '';
            $major['majorCode_2_2'] = '';

            $program = [
                'programCode_1' => $recordProgramMajor['sgbstdn_program_1'],
                'programCode_2' => $recordProgramMajor['sgbstdn_program_2'],
            ];

            $major = [
                'majorCode_1' => $recordProgramMajor['sgbstdn_majr_code_1'],
                'majorCode_2' => $recordProgramMajor['sgbstdn_majr_code_2'],
                'majorCode_1_2' => $recordProgramMajor['sgbstdn_majr_code_1_2'],
                'majorCode_2_2' => $recordProgramMajor['sgbstdn_majr_code_2_2'],
            ];

            $records = $this->applyingRules($program, $major, $options);
        }

        return $records;
    }

    public function applyingRules($program, $major, $options)
    {
        $model = [
            'status' => true,
            'priority' => 5,
            'termCode' => $options['termCode'],
        ];

        $state = $options['state'];

        // No state always results in a passing rule
        if (empty($state)) {
            return $model;
        }

        // Fetch all student rules from database
        $rules = $this->dbh->queryall_array('
            select szrsorr_rule_type,szrsorr_attribute,szrsorr_allowed_states,
                szrsorr_rule_value from szrsorr
                where szrsorr_rule_type = ?
                ', 'Student');

        // Foreach rule, does it match program 1 or program 2 (if set) (wildcards)
        $ruleResult = true;
        foreach ($rules as $rule) {
            $ruleResult = $this->runProgramMajorRule($rule, $program['programCode_1'],
                [$major['majorCode_1'], $major['majorCode_1_2']], $state);
            if ($ruleResult) {
                $ruleResult = $this->runProgramMajorRule($rule, $program['programCode_2'],
                    [$major['majorCode_2'], $major['majorCode_2_2']], $state);
            }
            if (!$ruleResult) {
                break;
            }
        }

        if (!$ruleResult) {
            $model['status'] = false;
            $model['priority'] = 3;
        }

        return $model;
    }

    private function runProgramMajorRule($rule, $programCode, $majors, $state)
    {
        $ruleResult = true;

        // explode to list() causes errors if all parts do not exist consistently.
        $parts = explode('/', $rule['szrsorr_rule_value']);
        $ruleProgramCode = isset($parts[0]) ? $parts[0] : '';
        $ruleMajorCode = isset($parts[1]) ? $parts[1] : '';

        $programCodeMatched = preg_match('/^' . $ruleProgramCode . '$/', $programCode);
        if ($rule['szrsorr_attribute'] === 'Program/Major' && $programCodeMatched && $ruleMajorCode && in_array($ruleMajorCode, $majors)) {
            $ruleResult = in_array($state, explode('|', $rule['szrsorr_allowed_states']));
        } elseif ($rule['szrsorr_attribute'] === 'Program' && $programCodeMatched) {
            $ruleResult = in_array($state, explode('|',$rule['szrsorr_allowed_states']));
        }

        return $ruleResult;
    }
}