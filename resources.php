<?php

return [
    'resources' => [
        'stateOfResidence' => [
            \MiamiOH\StateOfResidenceWebService\Resources\ActionResourceProvider::class,
            \MiamiOH\StateOfResidenceWebService\Resources\AdminStatusResourceProvider::class,
            \MiamiOH\StateOfResidenceWebService\Resources\ConfigResourceProvider::class,
            \MiamiOH\StateOfResidenceWebService\Resources\CourseResourceProvider::class,
            \MiamiOH\StateOfResidenceWebService\Resources\EnrollmentResourceProvider::class,
            \MiamiOH\StateOfResidenceWebService\Resources\ProfileResourceProvider::class,
            \MiamiOH\StateOfResidenceWebService\Resources\ReportResourceProvider::class,
            \MiamiOH\StateOfResidenceWebService\Resources\RuleResourceProvider::class,
            \MiamiOH\StateOfResidenceWebService\Resources\TermResourceProvider::class
        ]
    ]
];