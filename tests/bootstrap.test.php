<?php

$RESTNG_HOME = getenv('RESTNG_HOME');

if ($RESTNG_HOME === false) {
    print "Please set \$RESTNG_HOME to the full path to the RESTng installation\n";
    exit(1);
}

if (!file_exists($RESTNG_HOME)) {
    print "Can't find $RESTNG_HOME\n";
    exit(1);
}

$testDir = __DIR__;
$sourceDir = dirname($testDir) . '/src';
$installDir = $RESTNG_HOME;
$vendorDir = dirname($installDir) . '/vendor';
$apiDir = $installDir . '/RESTng';

require $apiDir . '/Constants.php';

// Install the api and vendor autoloaders.
require $testDir . '/AutoLoader.php';
require $apiDir . '/AutoLoader.php';
require $vendorDir . '/autoload.php';

// Require the project vendor autoloader
if (file_exists(dirname(dirname($testDir)) . '/vendor/autoload.php')) {
    require dirname(dirname($testDir)) . '/vendor/autoload.php';
}

spl_autoload_register(array('TestAutoLoader', 'load'));
spl_autoload_register(array('AutoLoader', 'load'));

// Our autoloader lets us add paths on the fly.
TestAutoLoader::AddPath($sourceDir);
TestAutoLoader::AddPath($installDir);
AutoLoader::AddPath($installDir);

// Load the log4php config if we have one.
if (file_exists($testDir . '/log4php.conf.php')) {
    require_once $testDir . '/log4php.conf.php';
}

// Load the api config if we have one.
if (file_exists($testDir . '/api.conf.php')) {
    require_once $testDir . '/api.conf.php';
}
