<?php

namespace RESTng;

const MARMOTLIB = '/vagrant/ws/www/marmotlib';
const MU_API_URL = 'https://wsdev.miamioh.edu/api';

// Errors will not be reported back in the response if display errors is off
ini_set("display_errors", "on");
error_reporting(E_ALL | E_STRICT);
