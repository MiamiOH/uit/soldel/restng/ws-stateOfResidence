<?php
namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Service\Extension\BannerId;
use MiamiOH\RESTng\Service\Extension\BannerUtil;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\StateOfResidenceWebService\Services\Profile;
use MiamiOH\StateOfResidenceWebService\Services\ProfileService;

class ProfileUpdateServiceTest extends TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $profileService;
    private $bannerId;

    private $profile;

    private $mockModel = [];
    private $updateModel = [];
    private $requestResourceParam = '';
    private $requestResourceParamMocks = [];

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp(): void
    {

        $this->mockModel = [];
        $this->updateModel = [];
        $this->requestResourceParam = '';
        $this->requestResourceParamMocks = [];

        //set up the mock api:
        $this->app = $this->getMockBuilder(App::class)
            ->setMethods(array('newResponse'))
            ->getMock();

        $this->app->method('newResponse')->willReturn(new Response());

        $this->bannerId = $this->getMockBuilder(BannerId::class)
            ->setMethods(array('getPidm'))
            ->getMock();

        $bannerUtil = $this->getMockBuilder(BannerUtil::class)
            ->setMethods(array('getId'))
            ->getMock();

        $bannerUtil->method('getId')->willReturn($this->bannerId);


        $this->profile = $this->getMockBuilder(Profile::class)
            ->setMethods(array('UpdateProfile'))
            ->getMock();

        $this->profile->method('updateProfile')
            ->with($this->callback(array($this, 'updateProfileWith')))
            ->willReturn(true);

        $this->request = $this->getMockBuilder('\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData','getResourceParamKey'))
            ->getMock();

        $logger = $this->getMockBuilder('Logger')
            ->setConstructorArgs(array('logger'))
            ->setMethods(array('debug', 'info','error'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->profileService = new ProfileService();
        $reflection = new \ReflectionClass($this->profileService);
        $reflection_property = $reflection->getProperty('log');
        $reflection_property->setAccessible(true);
        $reflection_property->setValue($this->profileService, $logger);

        $this->profileService->setApp($this->app);
        $this->profileService->setProfile($this->profile);
        $this->profileService->setBannerUtil($bannerUtil);

    }

    /*************************/
    /**********Tests**********/
    /*************************/

    public function testUpdateProfile()
    {
        $this->mockModel = [
            'pidm' => 123456,
            'uniqueId' => 'doej',
            'firstName' => 'John',
            'lastName' => 'doe',
            'term' => [
                [
                    'pidm' => 0,
                    'termCode' => '201710',
                    'termDesc' => 'Fall term',
                    'status' => 'complete',
                    'country' => 'US',
                    'state' => 'TX',
                    'lastSeenDate' => 0,
                    'courseList' => [
                        [
                            'subject' => 'CS',
                            'number' => 0
                        ]
                    ]
                ]
            ]
        ];

        $this->bannerId->method('getPidm')->willReturn('123456');

        $this->request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('muid'))->willReturn('doej');

        $this->request->expects($this->once())->method('getResourceParamKey')
            ->with($this->equalTo('muid'))->willReturn('uniqueId');

        $this->request->expects($this->once())->method('getData')
            ->will($this->returnCallback(array($this, 'getDataMock')));

        $this->profileService->setRequest($this->request);

        $response = $this->profileService->updateProfile();

        $this->assertEquals(App::API_OK, $response->getStatus());

    }

    public function updateProfileWith($subject)
    {
        $this->updateModel = $subject;
        return true;
    }

    public function getDataMock()
    {
        return $this->mockModel;
    }

}
