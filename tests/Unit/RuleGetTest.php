<?php
namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;

use MiamiOH\RESTng\Connector\DatabaseFactory;
use MiamiOH\RESTng\Core\DB\DBH;
use MiamiOH\StateOfResidenceWebService\Services\Rule;

/**
 * Created by PhpStorm.
 * User: ravendpp
 * Date: 11/22/16
 * Time: 2:19 PM
 */
class RuleGetTest extends TestCase
{
    private $rule;

    private $queryString = '';

    /**
     * Setup for the tests which is called by PhpUnit before executing tests.
     */
    protected function setUp(): void
    {

        $this->prepareQueryString = '';
        $this->DBRecord = [];
        $this->queryString = '';
        $this->index = 0;

        $this->dbh = $this->getMockBuilder(DBH::class)
            ->setMethods(array('queryall_array'))
            ->getMock();

        $db = $this->getMockBuilder(DatabaseFactory::class)
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->rule = new Rule();

        $this->rule->setDatabase($db);
    }

    /**
     *
     */
    public function testGetRuleList()
    {
        $this->DBRecord = [
            [
                'szrsorr_rule_id' => 2,
                'szrsorr_rule_type' => 'Student',
                'szrsorr_attribute' => 'Program',
                'szrsorr_allowed_states' => 'OH',
                'szrsorr_rule_value' => 'BS NURSING',
            ]
        ];


        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')))
            ->willReturn($this->DBRecord);

        $model = $this->rule->getRuleList();

    }

    public function queryall_arrayWithQuery($subject)
    {
        $this->queryString = $subject;
        return true;
    }

}
