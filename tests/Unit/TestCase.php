<?php
/**
 * Author: liaom
 * Date: 7/31/18
 * Time: 11:09 AM
 */

namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;

use MiamiOH\RESTng\Connector\DatabaseFactory;
use MiamiOH\RESTng\Legacy\DB\DBH;
use MiamiOH\RESTng\Service\AuthorizationValidator\AuthorizationValidatorInterface;
use MiamiOH\RESTng\Service\CredentialValidator\CredentialValidatorInterface;
use PHPUnit\Framework\MockObject\MockObject;

class TestCase extends \MiamiOH\RESTng\Testing\TestCase
{
    /**
     * @var MockObject
     */
    protected $dbh;
    /**
     * @var MockObject
     */
    protected $request;
    /**
     * @var MockObject
     */
    protected $credentialValidator;
    /**
     * @var MockObject
     */
    protected $authorizationValidator;

    protected function setUp(): void
    {
        parent::setUp();
        $databaseFactory = $this->createMock(DatabaseFactory::class);
        $this->dbh = $this->createMock(DBH::class);
        $databaseFactory->method('getHandle')->willReturn($this->dbh);

        $this->app->useService([
            'name' => 'APIDatabaseFactory',
            'object' => $databaseFactory,
            'description' => 'Mocked Database Factory'
        ]);

        $this->credentialValidator =
            $this->createMock(CredentialValidatorInterface::class);

        $this->app->useService([
            'name' => 'CredentialValidator',
            'object' => $this->credentialValidator,
            'description' => 'Mock credential validator',
        ]);

        $this->app->useService([
            'name' => 'MU\\BannerUtil',
            'object' => $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerUtil')->getMock(),
            'description' => 'Mock BannerUtil',
        ]);

        $this->authorizationValidator =
            $this->createMock(AuthorizationValidatorInterface::class);

        $this->app->useService([
            'name' => 'AuthorizationValidator',
            'object' => $this->authorizationValidator,
            'description' => 'Mock authorization validator',
        ]);
    }
}