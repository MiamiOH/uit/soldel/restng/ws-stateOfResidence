<?php
namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\Configuration;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\StateOfResidenceWebService\Services\Config;
use MiamiOH\StateOfResidenceWebService\Services\WAS;

class WASTest extends TestCase
{
    private $was;
    private $configObj;
    private $config;

    private $configData = [];

    private $resourceBeingCalledName = [];
    private $resourceBeingCalledArgs = [];

    protected function setUp(): void
    {

        $this->resourceBeingCalledName = [];
        $this->resourceBeingCalledArgs = [];

        // Test will use this data unless you call setConfig again
        $this->configData = [
            'wasClientUpdateUrl' => 'http://ws/StateOfResidence/updateWAS.php?extapp=StateOfResidence&ss=bob',
        ];

        $this->app = $this->getMockBuilder(App::class)
            ->setMethods(array('newResponse', 'callResource'))
            ->getMock();

        $this->response = $this->getMockBuilder(Response::class)
            ->setMethods(array('getStatus', 'getPayload'))
            ->getMock();

        $this->app->method('newResponse')->willReturn($this->response);

        $this->config = $this->getMockBuilder(Config::class)
            ->setMethods(array('getConfiguration'))
            ->getMock();

        $this->configObj = $this->getMockBuilder(Configuration::class)
            ->setMethods(array('getConfiguration'))
            ->getMock();
        
        $this->configObj->method('getConfiguration')
            ->will($this->returnCallback(array($this, 'getConfigMock')));

        $this->was = new WAS();
        $logger = $this->getMockBuilder('Logger')
            ->setConstructorArgs(array('logger'))
            ->setMethods(array('debug', 'info','error'))
            ->getMock();
        //set up the service with the mocked out resources:
        $this->was = new WAS();
        $reflection = new \ReflectionClass($this->was);
        $reflection_property = $reflection->getProperty('log');
        $reflection_property->setAccessible(true);
        $reflection_property->setValue($this->was, $logger);
        $this->was->setConfiguration($this->configObj);
        $this->was->setApp($this->app);
    }

    public function testWASUpdateComplete()
    {
        $response = $this->getMockBuilder('\RESTng\Util\Response')
            ->setMethods(array('getStatus', 'getPayload'))
            ->getMock();

        $response->method('getStatus')->willReturn(App::API_CREATED);

        $response->method('getPayload')->willReturn(
            [
              "data" => [
                "complete" => "1",
                "infoRequired" => "0"
              ],
              "status" => 201,
              "error" => false
            ]
        );

        $this->app->method('callResource')
            ->with($this->callback(array($this, 'callResourceWithName')),
                $this->callback(array($this, 'callResourceWithArgs')))
            ->will($this->onConsecutiveCalls($response));

        $this->was->updateWAS('doej', ['complete' => true]);

        $this->assertEquals(1, $this->resourceBeingCalledArgs[0]['data']['complete']);
        $this->assertEquals('doej', $this->resourceBeingCalledArgs[0]['params']['uniqueId']);
    }
    
    public function testWASFailure()
    {
        $configuration = [
            'email.devTeam' => 'duit-soldel42@miamioh.edu',
            'email.fromAddress' => 'StateofResidence@miamioh.edu',
        ];

        $this->config->method('getConfiguration')
            ->with('SendNotification')
            ->willReturn($configuration);

        $response1 = $this->getMockBuilder(Response::class)
            ->setMethods(array('getStatus'))
            ->getMock();

        $response1->method('getStatus')->willReturn(App::API_FAILED);

        $response2 = $this->getMockBuilder(Response::class)
            ->setMethods(array('getStatus', 'getPayload'))
            ->getMock();

        $response2->method('getStatus')->willReturn(App::API_ACCEPTED);

        $response2->method('getPayload')->willReturn(
            [
                "id" => "1",
                "key" => "abc"
            ]
        );

        $response3 = $this->getMockBuilder(Response::class)
            ->setMethods(array('getStatus', 'getPayload'))
            ->getMock();

        $response3->method('getStatus')->willReturn(App::API_CREATED);

        $this->app->method('callResource')
            ->with($this->callback(array($this, 'callResourceWithName')),
                $this->callback(array($this, 'callResourceWithArgs')))
            ->will($this->onConsecutiveCalls($response1, $response2, $response3));

        $this->was->updateWAS('doej', ['complete' => true]);

        $this->assertCount(3, $this->resourceBeingCalledName);
        $this->assertStringContainsString('key: abc', $this->resourceBeingCalledArgs[2]['data']['data']['body']);
    }

    public function getConfigMock()
    {
        return $this->configData;
    }

    public function callResourceWithName($subject)
    {
        $this->resourceBeingCalledName[] = $subject;
        return true;
    }

    public function callResourceWithArgs($subject)
    {
        $this->resourceBeingCalledArgs[] = $subject;
        return true;
    }
}
