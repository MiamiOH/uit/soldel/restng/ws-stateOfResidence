<?php

namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\StateOfResidenceWebService\Services\Course;
use MiamiOH\StateOfResidenceWebService\Services\CourseService;

class CourseGetServiceTest extends TestCase
{

    private $courseService;
    private $course;

    private $mockModel = [];
    private $requestResourceParam = '';
    private $requestResourceParamMocks = [];

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp(): void
    {

        $this->mockModel = [];
        $this->requestResourceParam = '';
        $this->requestResourceParamMocks = [];

        //set up the mock api:
        $this->app = $this->getMockBuilder(App::class)
            ->setMethods(array('newResponse'))
            ->getMock();

        $this->app->method('newResponse')->willReturn(new Response());

        $this->course = $this->getMockBuilder(Course::class)
            ->setMethods(array('getCourseList'))
            ->getMock();

        $this->course->method('getCourseList')
            ->will($this->returnCallback(array($this, 'getCourseListMock')));

        $this->request = $this->getMockBuilder(Request::class)
            ->setMethods(array('getResourceParam'))
            ->getMock();
        $logger = $this->getMockBuilder('Logger')
            ->setConstructorArgs(array('logger'))
            ->setMethods(array('debug', 'info','error'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->courseService = new CourseService();
        $reflection = new \ReflectionClass($this->courseService);
        $reflection_property = $reflection->getProperty('log');
        $reflection_property->setAccessible(true);
        $reflection_property->setValue($this->courseService, $logger);

        $this->courseService->setApp($this->app);
        $this->courseService->setCourse($this->course);

    }

    /*************************/
    /**********Tests**********/
    /*************************/

    public function testGetCourseList()
    {
        $this->mockModel = [
            [
                "courseTitle" => "Leadership Laboratory",
                "subjectCode" => "AES",
                "courseNumber" => "110",
                "campusDesc" => "Hamilton",
                "campusCode" => "H",
                "termCode" => "201710",
                "crn" => "50289",
                "instructionalMethod" => "HYB",
            ],
            [
                "courseTitle" => "Intro to Management&Leadership",
                "subjectCode" => "MGT",
                "courseNumber" => "291",
                "campusDesc" => "Hamilton",
                "campusCode" => "H",
                "termCode" => "201715",
                "crn" => "40148",
                "instructionalMethod" => "ONL",
            ],
        ];


        $this->courseService->setRequest($this->request);

        $response = $this->courseService->getCourseList();

        $payload = $response->getPayload();

        $this->assertEquals(App::API_OK, $response->getStatus());
        $this->assertEquals("201710", $payload[0]['termCode']);
        $this->assertEquals("201715", $payload[1]['termCode']);

        $this->assertEquals("Leadership Laboratory", $payload[0]['courseTitle']);
        $this->assertEquals("AES", $payload[0]['subjectCode']);
        $this->assertEquals("110", $payload[0]['courseNumber']);
        $this->assertEquals("Hamilton", $payload[0]['campusDesc']);
        $this->assertEquals("H", $payload[0]['campusCode']);
        $this->assertEquals("50289", $payload[0]['crn']);
        $this->assertEquals("HYB", $payload[0]['instructionalMethod']);


    }

    public function getCourseListMock()
    {
        return $this->mockModel;

    }

}
