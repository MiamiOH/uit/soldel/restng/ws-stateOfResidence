<?php

namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;

use MiamiOH\RESTng\Connector\DatabaseFactory;
use MiamiOH\RESTng\Core\DB\DBH;
use MiamiOH\StateOfResidenceWebService\Services\TermStatus;
use Propel\Generator\Model\Database;

class TermStatusGetTest extends TestCase
{

    private $termStatus;

    private $config;

    protected function setUp(): void
    {

        $db = $this->getMockBuilder(DatabaseFactory::class)
            ->setMethods(array('getHandle'))
            ->getMock();

        $this->dbh = $this->getMockBuilder(DBH::class)
            ->setMethods(array('queryfirstrow_assoc', 'queryall_array',))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->termStatus = new TermStatus();

        $this->termStatus->setDatabase($db);

    }

    public function testGetTermStatus()
    {
        $options = [
            'termCode' => '201710',
            'country' => 'US',
            'state' => 'TX',
        ];

        $this->recordForPrograms = [
            'sgbstdn_pidm' => '123456',
            'sgbstdn_term_code_eff' => '201710',
            'sgbstdn_program_1' => 'PRE BS NURS',
            'sgbstdn_program_2' => 'AB THE',
            'sgbstdn_majr_code_1' => 'RCN4',
            'sgbstdn_majr_code_2' => null,
            'sgbstdn_majr_code_1_2' => null,
            'sgbstdn_majr_code_2_2' => null,
            'status' => true,
        ];

        $this->ruleListRecords = [
            [
                'szrsorr_rule_type' => 'Student',
                'szrsorr_attribute' => 'Program',
                'szrsorr_allowed_states' => 'OH|KY|NY',
                'szrsorr_rule_value' => 'Bachelor of Science in Nursing',
            ],
            [
                'szrsorr_rule_type' => 'Student',
                'szrsorr_attribute' => 'Program',
                'szrsorr_allowed_states' => 'OH|IN',
                'szrsorr_rule_value' => 'Bachelor of Arts in Theatre',
            ]

        ];
        $this->mockConfigurationItems = [
            [
                'program_code' => 'PRE BS NURS',
                'program_desc' => 'Bachelor of Science in Nursing',
            ],
            [
                'program_code' => 'AB THE',
                'program_desc' => 'Bachelor of Arts in Theatre',
            ],
        ];

        $this->dbh->expects($this->at(0))->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocWithQuery')),
                $this->callback(array($this, 'queryfirstrow_assocWithParams')))
            ->will($this->returnCallback(array($this, 'queryfirstrow_assocMock')));


        $this->dbh->expects(($this->at(1)))
            ->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')))
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));


        $model = $this->termStatus->getTermStatus(123456, $options);

        $this->assertTrue(stripos($this->queryString, 'select szrsorr_rule_type,szrsorr_attribute,szrsorr_allowed_states') !== false,
            'Query to select allowed states of student program is not mentioned');

    }

    public function queryfirstrow_assocWithQuery($subject)
    {
        $this->queryString = $subject;
        return true;
    }

    public function queryfirstrow_assocWithParams($subject)
    {
        $this->queryParams = $subject;
        return true;
    }

    public function queryfirstrow_assocMock()
    {
        return $this->recordForPrograms;
    }

    public function queryall_arrayWithQuery($subject)
    {
        $this->queryString = $subject;
        return true;
    }

    public function queryall_arrayWithParams($subject)
    {
        $this->queryParams = $subject;
        return true;
    }

    public function queryall_arrayMock()
    {
        return $this->ruleListRecords;
    }

    public function getConfigurationWithParams($subject)
    {
        $this->calledApplicationName = $subject;
        return true;
    }

    public function getConfigurationMock()
    {
        return $this->mockConfigurationItems;
    }
}
