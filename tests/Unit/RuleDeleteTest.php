<?php
namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;

use MiamiOH\RESTng\Connector\DatabaseFactory;
use MiamiOH\RESTng\Core\DB\DBH;
use MiamiOH\StateOfResidenceWebService\Services\Rule;

class RuleDeleteTest extends TestCase
{
    private $rule;

    private $prepareQueryString = '';

    /**
     * Setup for the tests which is called by PhpUnit before executing tests.
     */
    protected function setUp(): void
    {
        $this->prepareQueryString = '';
        $this->dbh = $this->getMockBuilder(DBH::class)
            ->setMethods(array('perform'))
            ->getMock();

        $db = $this->getMockBuilder(DatabaseFactory::class)
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->rule = new Rule();

        $this->rule->setDatabase($db);
    }

    /**
     *
     */
    public function testDeleteRule()
    {
        $this->dbh->expects($this->once())->method('perform')
            ->with($this->callback(array($this, 'performWithQuery')),
            2)
            ->willReturn(true);

        $model = $this->rule->deleteRule(2);
    }

    public function performWithQuery($subject)
    {
        $this->prepareQueryString = $subject;
        return true;
    }

}
