<?php

namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;

use MiamiOH\RESTng\Connector\DatabaseFactory;
use MiamiOH\RESTng\Core\DB\DBH;
use MiamiOH\StateOfResidenceWebService\Services\Config;
use MiamiOH\StateOfResidenceWebService\Services\Enrollment;

class EnrollmentGetCurrentTest extends TestCase
{


    private $enrollment;


    private $records = [];
    private $queryString = '';
    private $queryParams = [];


    protected function setUp(): void
    {

        $this->records = [];
        $this->queryString = '';
        $this->queryParams = [];

        $this->dbh = $this->getMockBuilder(DBH::class)
            ->setMethods(array('queryall_array', 'escape'))
            ->getMock();

        $db = $this->getMockBuilder(DatabaseFactory::class)
            ->setMethods(array('getHandle'))
            ->getMock();

        $this->config = $this->getMockBuilder(Config::class)
            ->getMock();

        $this->config->method("getConfiguration")->will(
            $this->returnValueMap(
                [
                    [ 'CodeMap', ['courseTypeDesc' => '{ "ONLS": "Online Synchronous", "ONLA": "Online Asynchronous", "HYBS": "Hybrid Synchronous", "HYBA": "Hybrid Asynchronous" }'] ]
                ]
            )
        );

        $db->method('getHandle')->willReturn($this->dbh);

        $this->enrollment = new Enrollment();

        $this->enrollment->setDatabase($db);
        $this->enrollment->setConfig($this->config);

    }

    public function testGetEnrollment()
    {

        $this->records = [
            [
                'sfrstcr_pidm' => '123456',
                'sfrstcr_term_code' => '201710',
            ],
        ];
        $this->dbh->expects($this->once())->method('queryall_array')
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $configuration = [
            '0' => '201710',
        ];

        $model = $this->enrollment->getEnrollment();

    }


    public function queryall_arrayWithQuery($subject)
    {
        $this->queryString = $subject;
        return true;
    }

    public function queryall_arrayWithParams($subject)
    {
        $this->queryParams = $subject;
        return true;
    }

    public function queryall_arrayMock()
    {
        return $this->records;
    }

}