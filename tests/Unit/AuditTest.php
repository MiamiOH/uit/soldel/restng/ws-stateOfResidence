<?php
namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;

use MiamiOH\RESTng\Connector\DatabaseFactory;
use MiamiOH\RESTng\Core\DB\DBH;
use MiamiOH\StateOfResidenceWebService\Services\Audit;

class AuditTest extends TestCase

{
    private $insertAudit;

    private $records = [];
    private $queryString = '';
    private $queryParams = [];

    protected function setUp(): void
    {


        $this->recordIndex = 0;
        $this->queryStringPerform = '';
        $this->queryParams = [];


        $this->dbh = $this->getMockBuilder(DBH::class)
            ->setMethods(array('perform'))
            ->getMock();


        $db = $this->getMockBuilder(DatabaseFactory::class)
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->insertAudit = new Audit();

        $this->insertAudit->setDatabase($db);

    }

    public function testinsertAuditTable()
    {
        $this->dbh->method('perform')
            ->with($this->callback(array($this, 'performWithQuery')),
                $this->callback(array($this, 'performWithParams')))
            ->willReturn(true);
        $this->assertTrue(
            $this->insertAudit->insertAuditTable('US', 'OH', '123456', '201710', 'doej', '08-NOV-16')
        );
    }

    public function performWithQuery($subject)
    {
        $this->queryString = $subject;
        return true;
    }

    public function performWithParams($subject)
    {
        $this->queryParams = $subject;
        return true;
    }

    public function performMock()
    {
        return $this->records;
    }

}

