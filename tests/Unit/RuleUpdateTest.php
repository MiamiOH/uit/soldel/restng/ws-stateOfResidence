<?php
namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Connector\DatabaseFactory;
use MiamiOH\StateOfResidenceWebService\Services\Rule;

/**
 * Created by PhpStorm.
 * User: ravendpp
 * Date: 12/2/16
 * Time: 4:07 PM
 */



class RuleUpdateTest extends TestCase
{
    private $rule;
    private $apiUser;


    private $data = [];
    private $queryStringPerform = '';
    private $queryParams = [];
    private $mockApiUsername = '';

    protected function setUp(): void
    {

        $this->data = [];
        $this->recordIndex = 0;
        $this->queryStringPerform = '';
        $this->queryParams = [];


        $db = $this->getMockBuilder(DatabaseFactory::class)
            ->setMethods(array('getHandle'))
            ->getMock();

        $this->dbh = $this->getMockBuilder(DBH::class)
            ->setMethods(array('perform'))
            ->getMock();

        $this->apiUser = $this->getMockBuilder(App::class)
            ->setMethods(array('getUsername'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->rule = new Rule();

        $this->rule->setDatabase($db);
        $this->rule->setApiUser($this->apiUser);

    }

    public function testUpdateRule()
    {

        $this->data = [
            'ruleID' => 2,
            'ruleType' => 'student',
            'attribute' => 'program',
            'allowedStates'=> 'OH',
            'ruleValue' => 'BS NURSING'
        ];

        $this->dbh->method('perform')
            ->with($this->callback(array($this, 'performWithQuery')),
                $this->callback(array($this, 'performWithParams')))
            ->willReturn(true);

        $this->rule->updateRule($this->data);

        $this->assertTrue(stripos($this->queryStringPerform, 'UPDATE szrsorr set szrsorr_rule_type = ?') !== false,
            'Query from szrsorr');

    }
    public function performWithQuery($subject)
    {
        $this->queryStringPerform = $subject;
        return true;
    }

    public function performWithParams($subject)
    {
        $this->queryParams = $subject;
        return true;
    }

    public function getUsernameMock()
    {
        return $this->mockApiUsername;
    }

    }
