<?php
/**
 * Created by PhpStorm.
 * User: ravendpp
 * Date: 12/5/16
 * Time: 3:49 PM
 */
namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;


use MiamiOH\RESTng\Connector\DatabaseFactory;
use MiamiOH\RESTng\Core\DB\DBH;
use MiamiOH\StateOfResidenceWebService\Services\Rule;

class RuleCreateTest extends TestCase
{

    private $rule;

    private $apiUser;
    private $data = [];
    private $queryStringPerform = '';
    private $queryParams = [];
    private $queryStringFirstColumn = '';
    private $mockApiUsername = '';

    protected function setUp(): void
    {

        $this->data = [];
        $this->recordIndex = 0;
        $this->queryStringPerform = '';
        $this->queryParams = [];
        $this->queryStringFirstColumn = '';


        $db = $this->getMockBuilder(DatabaseFactory::class)
            ->setMethods(array('getHandle'))
            ->getMock();

        $this->dbh = $this->getMockBuilder(DBH::class)
            ->setMethods(array('perform','queryfirstcolumn'))
            ->getMock();

        $this->apiUser = $this->getMockBuilder(App::class)
            ->setMethods(array('getUsername'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->rule = new Rule();

        $this->rule->setDatabase($db);
        $this->rule->setApiUser($this->apiUser);


    }

    public function testCreateRule()
    {

        $this->data = [
            'ruleType' => 'student',
            'attribute' => 'program',
            'allowedStates'=> 'OH',
            'ruleValue' => 'BS NURSING'
        ];

        $this->dbh->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryFirstColumnWithQuery')))
            ->willReturn(2);

        $this->dbh->method('perform')
            ->with($this->callback(array($this, 'performWithQuery')),
                $this->callback(array($this, 'performWithParams')))
            ->willReturn(true);

        $this->rule->createRule($this->data);

        $this->assertTrue(stripos($this->queryStringPerform, 'insert into szrsorr (szrsorr_rule_id, szrsorr_rule_type, szrsorr_attribute, szrsorr_allowed_states, szrsorr_rule_value, szrsorr_activity_date, szrsorr_user_id)') !== false,
            'Query from szrsorr');

    }
    public function performWithQuery($subject)
    {
        $this->queryStringPerform = $subject;
        return true;
    }

    public function performWithParams($subject)
    {
        $this->queryParams = $subject;
        return true;
    }

    public function queryFirstColumnWithQuery($subject)
    {
        $this->queryStringFirstColumn = $subject;
        return true;
    }

    public function getUsernameMock()
    {
        return $this->mockApiUsername;
    }
}
