<?php

namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\StateOfResidenceWebService\Services\EnrollmentService;

class EnrollmentGetCurrentServiceTest extends TestCase
{

    private $enrollmentService;

    private $enrollment;
    private $mockReadResponse = [];


    protected function setUp(): void
    {


        $this->app = $this->getMockBuilder(App::class)
            ->setMethods(array('newResponse'))
            ->getMock();

        $this->app->method('newResponse')->willReturn(new Response());


        $this->request = $this->getMockBuilder(Request::class)
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $this->enrollment = $this->getMockBuilder(Enrollment::class)
            ->setMethods(array('getEnrollment'))
            ->getMock();

        $this->enrollment->method('getEnrollment')
            ->will($this->returnCallback(array($this, 'getEnrollmentMock')));
        $logger = $this->getMockBuilder('Logger')
            ->setConstructorArgs(array('logger'))
            ->setMethods(array('debug', 'info','error'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->enrollmentService = new EnrollmentService();
        $reflection = new \ReflectionClass($this->enrollmentService);
        $reflection_property = $reflection->getProperty('log');
        $reflection_property->setAccessible(true);
        $reflection_property->setValue($this->enrollmentService, $logger);

        $this->enrollmentService->setApp($this->app);
        $this->enrollmentService->setEnrollmentService($this->enrollment);


    }


    public function testGetEnrollmentInfo()
    {


        $this->mockReadResponse = [
            [
                'pidm' => 123456,
                'termCode' => '201710',
            ]
        ];

        $this->enrollmentService->setRequest($this->request);

        $response = $this->enrollmentService->getEnrollmentInfo();

        $payload = $response->getPayload();


        $this->assertEquals(App::API_OK, $response->getStatus());

        $this->assertEquals(123456, $payload[0]['pidm']);
        $this->assertEquals('201710', $payload[0]['termCode']);

    }

    public function getEnrollmentMock()
    {
        return $this->mockReadResponse;

    }


}