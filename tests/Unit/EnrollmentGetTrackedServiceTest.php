<?php
namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\StateOfResidenceWebService\Services\Enrollment;
use MiamiOH\StateOfResidenceWebService\Services\EnrollmentService;

class EnrollmentGetTrackedServiceTest extends TestCase
{

    private $enrollmentTrackService;

    private $enrollmentTrack;
    private $mockReadResponse = [];


    protected function setUp(): void
    {


        $this->app = $this->getMockBuilder(App::class)
            ->setMethods(array('newResponse'))
            ->getMock();

        $this->app->method('newResponse')->willReturn(new Response());


        $this->request = $this->getMockBuilder(Request::class)
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $this->enrollmentTrack = $this->getMockBuilder(Enrollment::class)
            ->setMethods(array('getTrackedInfo'))
            ->getMock();

        $this->enrollmentTrack->method('getTrackedInfo')
            ->will($this->returnCallback(array($this, 'getTrackedInfoMock')));

        $logger = $this->getMockBuilder('Logger')
            ->setConstructorArgs(array('logger'))
            ->setMethods(array('debug', 'info','error'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->enrollmentTrackService = new EnrollmentService();
        $reflection = new \ReflectionClass($this->enrollmentTrackService);
        $reflection_property = $reflection->getProperty('log');
        $reflection_property->setAccessible(true);
        $reflection_property->setValue($this->enrollmentTrackService, $logger);

        $this->enrollmentTrackService->setApp($this->app);
        $this->enrollmentTrackService->setEnrollmentService($this->enrollmentTrack);


    }


    public function testGetTrackedInfoRESTCancel()
    {


        $this->mockReadResponse = [
            [
                'pidm' => 123456,
                'termCode' => '201715',
                'nextAction' => 'cancel',
                'status' => 'pending',
            ]
        ];

        $this->enrollmentTrackService->setRequest($this->request);

        $response = $this->enrollmentTrackService->getTrackedInfo();

        $payload = $response->getPayload();


        $this->assertEquals(App::API_OK, $response->getStatus());

        $this->assertEquals(123456, $payload[0]['pidm']);
        $this->assertEquals('201715', $payload[0]['termCode']);
        $this->assertEquals('cancel', $payload[0]['nextAction']);
        $this->assertEquals('pending', $payload[0]['status']);


    }

    public function testGetTrackedInfoRESTPending()
    {


        $this->mockReadResponse = [
            [
                'pidm' => 123456,
                'termCode' => '201710',
                'nextAction' => 'track',
                'status' => 'pending',
            ]
        ];

        $this->enrollmentTrackService->setRequest($this->request);

        $response = $this->enrollmentTrackService->getTrackedInfo();

        $payload = $response->getPayload();


        $this->assertEquals(App::API_OK, $response->getStatus());

        $this->assertEquals(123456, $payload[0]['pidm']);
        $this->assertEquals('201710', $payload[0]['termCode']);
        $this->assertEquals('track', $payload[0]['nextAction']);
        $this->assertEquals('pending', $payload[0]['status']);


    }

    public function getTrackedInfoMock()
    {
        return $this->mockReadResponse;

    }


}