<?php
/**
 * Created by PhpStorm.
 * User: ravendpp
 * Date: 12/5/16
 * Time: 3:49 PM
 */
namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;


use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\StateOfResidenceWebService\Services\Rule;
use MiamiOH\StateOfResidenceWebService\Services\RuleService;

class RuleCreateServiceTest extends TestCase
{

    private $ruleService;

    private $rule;

    private $mockModel = [];

    private $requestResourceParam = '';

    private $requestResourceParamMocks = [];

    /**
     * set up method which is automatically called by PHPUnit before every test method:
     */
    protected function setUp(): void
    {

        $this->mockModel = [];
        $this->requestResourceParam = '';
        $this->requestResourceParamMocks = [];

        //set up the mock api:
        $this->app = $this->getMockBuilder(App::class)
            ->setMethods(array('newResponse'))
            ->getMock();

        $this->app->method('newResponse')->willReturn(new Response());

        $this->rule = $this->getMockBuilder(Rule::class)
            ->setMethods(array('createRule'))
            ->getMock();

        $this->rule->method('createRule')
            ->with($this->callback(array($this, 'createRuleWith')))
            ->willReturn(true);

        $this->request = $this->getMockBuilder(Request::class)
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();

        $logger = $this->getMockBuilder('Logger')
            ->setConstructorArgs(array('logger'))
            ->setMethods(array('debug', 'info','error'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->ruleService = new RuleService();
        $reflection = new \ReflectionClass($this->ruleService);
        $reflection_property = $reflection->getProperty('log');
        $reflection_property->setAccessible(true);
        $reflection_property->setValue($this->ruleService, $logger);

        $this->ruleService->setApp($this->app);
        $this->ruleService->setRule($this->rule);

    }

    public function testUpdateRuleListREST()
    {
        $this->mockModel = [
            [
                'ruleType' => 'student',
                'attribute' => 'program',
                'allowedStates'=> 'OH',
                'ruleValue' => 'BS NURSING'
            ],
        ];
        $this->request->expects($this->once())->method('getData')
            ->will($this->returnCallback(array($this, 'getDataMock')));


        $this->ruleService->setRequest($this->request);

        $response = $this->ruleService->createRuleList();

        $payload = $response->getPayload();

        $this->assertEquals(App::API_OK, $response->getStatus());

    }

    public function getDataMock()
    {
        return $this->mockModel;

    }

    public function createRuleWith($subject)
    {
        $this->createModel = $subject;
        return true;
    }
}
