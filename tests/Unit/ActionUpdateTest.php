<?php

namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Connector\DatabaseFactory;
use MiamiOH\RESTng\Core\DB\DBH;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\User;
use MiamiOH\StateOfResidenceWebService\Services\Action;
use MiamiOH\StateOfResidenceWebService\Services\Config;
use MiamiOH\StateOfResidenceWebService\Services\WAS;

class ActionUpdateTest extends TestCase
{

    private $action;
    private $config;
    private $was;
    private $response;

    private $firstColumnQueryString = [];
    private $firstRowQueryString = [];
    private $firstColumnParam = [];
    private $firstRowParam = '';
    private $queryStringPerform = '';
    private $queryParams = [];
    private $resourceBeingCalledName = '';

    private $mockConfigurationItems = [];
    private $resourceBeingCalledArgs = array();
    private $updateWASUid = '';
    private $updateWASOptions = [];
    private $updateWASResponse = '';

    protected function setUp(): void
    {

        $this->recordIndex = 0;
        $this->firstColumnQueryString = [];
        $this->firstRowQueryString = [];
        $this->firstColumnParam = [];
        $this->firstRowParam = '';
        $this->queryStringPerform = '';
        $this->queryParams = [];
        $this->resourceBeingCalledName = '';
        $this->updateWASUid = '';
        $this->updateWASOptions = [];
        $this->updateWASResponse = '';
        $this->actionRecord = [];


        $this->mockConfigurationItems = array();

        $this->app = $this->getMockBuilder(App::class)
            ->setMethods(array('callResource', 'newResponse'))
            ->getMock();

        $this->response = $this->getMockBuilder(Response::class)
            ->setMethods(array('getStatus', 'getPayload'))
            ->getMock();

        $this->apiUser = $this->createMock(User::class);

        $this->app->method('newResponse')->willReturn($this->response);

        $this->app->method('callResource')
            ->with($this->callback(array($this, 'callResourceWithName')),
                $this->callback(array($this, 'callResourceWithArgs')))
            ->willReturn($this->response);

        $this->config = $this->getMockBuilder(Config::class)
            ->setMethods(array('getConfiguration'))
            ->getMock();

        $this->dbh = $this->getMockBuilder(DBH::class)
            ->setMethods(array('queryfirstcolumn', 'perform', 'queryfirstrow_assoc'))
            ->getMock();

        $db = $this->getMockBuilder(DatabaseFactory::class)
            ->setMethods(array('getHandle'))
            ->getMock();
        $this->was = $this->getMockBuilder(WAS::class)
            ->setMethods(array('updateWAS'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->action = new Action();

        $this->action->setApp($this->app);
        $this->action->setApiUser($this->apiUser);
        $this->action->setDatabase($db);
        $this->action->setConfig($this->config);
        $this->action->setWas($this->was);
        $this->action->setDBH($this->dbh);

    }

    public function testUpdateTrackActionForNewStudent()
    {
        $this->dbh->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithParam')),
                $this->callback(array($this, 'queryfirstcolumnWithParam')))
            ->willReturn(0);

        $this->dbh->method('perform')
            ->with($this->callback(array($this, 'performWithQueryUpdate')),
                $this->callback(array($this, 'performWithParamsUpdate')),
                $this->callback(array($this, 'performWithParamsUpdate')))
            ->willReturn(true);

        $this->action->updateTrackAction([
            'pidm' => '12345',
            'termCode' => '201710',
            'nextAction' => '',
            'status' => ''
        ]);

        $this->assertTrue(stripos($this->firstColumnQueryString[0], 'SELECT count(*)') !== false,
            'Query to get count of records');

        $this->assertTrue(stripos($this->queryStringPerform, 'INSERT INTO SATURN.SZRSORM') !== false,
            'Query for inserting new student record');


    }

    public function testUpdateTrackActionForExistingStudentWithStatusCancel()
    {
        $this->studentIdentity = [
            'status' => 'cancel',
            'nation' => 'US'
        ];
        
        $this->dbh->expects($this->at(0))
            ->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithParam')),
                $this->callback(array($this, 'queryfirstcolumnWithParam')))
            ->willReturn(1);

        $this->dbh->expects($this->at(1))
            ->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocWithQuery')),
                $this->callback(array($this, 'queryfirstrow_assocWithParam')))
            ->will($this->returnCallBack(array($this, 'queryfirstrow_assocMock')));


        $this->dbh->method('perform')
            ->with($this->callback(array($this, 'performWithQueryUpdate')),
                $this->callback(array($this, 'performWithParamsUpdate')),
                $this->callback(array($this, 'performWithParamsUpdate')))
            ->willReturn(true);

        $this->action->updateTrackAction([
            'pidm' => '12345',
            'termCode' => '201710',
            'nextAction' => '',
            'status' => ''
        ]);

        $this->assertTrue(stripos($this->firstColumnQueryString[0], 'SELECT count(*)') !== false,
            'Query to get the count');

        $this->assertTrue(stripos($this->firstRowQueryString[0],
                'SELECT szrsorm_status as status') !== false,
            'Query to get the status value');

//        $this->assertTrue(stripos($this->queryStringPerform,
//                'UPDATE SATURN.SZRSORM SET szrsorm_lastseen_date = sysdate, szrsorm_status = \'pending\'') !== false,
//            'Query to update last seen date and status to pending for re-registered student');


    }

    public function testUpdateTrackActionForExistingStudentWithStatusPending()
    {

        $this->studentIdentity = [
            'status' => 'pending',
            'nation' => 'US'
        ];
        
        $this->dbh->expects($this->at(0))
            ->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithParam')),
                $this->callback(array($this, 'queryfirstcolumnWithParam')))
            ->willReturn(1);

        $this->dbh->expects($this->at(1))
            ->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocWithQuery')),
                $this->callback(array($this, 'queryfirstrow_assocWithParam')))
            ->will($this->returnCallBack(array($this, 'queryfirstrow_assocMock')));

        $this->dbh->method('perform')
            ->with($this->callback(array($this, 'performWithQueryUpdate')),
                $this->callback(array($this, 'performWithParamsUpdate')),
                $this->callback(array($this, 'performWithParamsUpdate')))
            ->willReturn(true);

        $this->action->updateTrackAction([
            'pidm' => '12345',
            'termCode' => '201710',
            'nextAction' => '',
            'status' => ''
        ]);

        $this->assertTrue(stripos($this->firstColumnQueryString[0], 'SELECT count(*)') !== false,
            'Query to get the count');

        $this->assertTrue(stripos($this->firstRowQueryString[0],
                'SELECT szrsorm_status as status') !== false,
            'Query to get the status value');


        $this->assertTrue(stripos($this->queryStringPerform,
                'UPDATE SATURN.SZRSORM') !== false,
            'Query to update last seen date registered student');


    }

    public function testUpdateCancelActionForExistingStudent()
    {

        $this->dbh->expects(($this->at(0)))
            ->method('perform')
            ->with($this->callback(array($this, 'performWithQueryUpdate')),
                $this->callback(array($this, 'performWithParamsUpdate')),
                $this->callback(array($this, 'performWithParamsUpdate')))
            ->willReturn(true);

        $this->dbh->expects(($this->at(1)))
            ->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithParam')))
            ->willReturn(0);

        $this->was->expects($this->once())
            ->method('updateWAS')
            ->with($this->callback(array($this, 'updateWASWithUid')),
                $this->callback(array($this, 'updateWASWithOptions')))
            ->will($this->returnCallback(array($this, 'updateWASMock')));

        $this->action->updateCancelAction('doej',
            ['pidm' => '12345', 'termCode' => '201710', 'nextAction' => '', 'status' => '']);


        $this->assertEquals('doej', $this->updateWASUid, 'Update WAS with correct uniqueid');
        $this->assertEquals($this->updateWASOptions['complete'], false, 'Update WAS with complete false');
        $this->assertEquals($this->updateWASOptions['infoRequired'], false, 'Update WAS with infoRequired false');

    }

    public function testUpdateCancelActionWithPendingTerm()
    {

        $this->dbh->expects(($this->at(0)))
            ->method('perform')
            ->with($this->callback(array($this, 'performWithQueryUpdate')),
                $this->callback(array($this, 'performWithParamsUpdate')),
                $this->callback(array($this, 'performWithParamsUpdate')))
            ->willReturn(true);

        $this->dbh->expects(($this->at(1)))
            ->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithParam')))
            ->willReturn(1);

        $this->was->expects($this->never())
            ->method('updateWAS')
            ->with($this->callback(array($this, 'updateWASWithUid')),
                $this->callback(array($this, 'updateWASWithOptions')))
            ->will($this->returnCallback(array($this, 'updateWASMock')));

        $this->action->updateCancelAction('doej',
            ['pidm' => '12345', 'termCode' => '201710', 'nextAction' => '', 'status' => '']);

    }

    public function testUpdateCompleteActionForExistingStudent()
    {

        $this->dbh->method('perform')
            ->with($this->callback(array($this, 'performWithQueryUpdate')),
                $this->callback(array($this, 'performWithParamsUpdate')),
                $this->callback(array($this, 'performWithParamsUpdate')))
            ->willReturn(true);
        $this->was->expects($this->once())
            ->method('updateWAS')
            ->with($this->callback(array($this, 'updateWASWithUid')),
                $this->callback(array($this, 'updateWASWithOptions')))
            ->will($this->returnCallback(array($this, 'updateWASMock')));

        $this->updateWASResponse = '<xml><status>1</status><message>logged</message></xml>';

        $this->action->updateCompleteAction('123456', 'doej', '201710');


        $this->assertTrue(stripos($this->queryStringPerform,
                'UPDATE SATURN.SZRSORM SET szrsorm_status =') !== false,
            'Query to update status to cancel');

        $this->assertEquals('doej', $this->updateWASUid, 'Update WAS with correct uniqueid');
        $this->assertTrue($this->updateWASOptions['complete'], 'Update WAS with complete true');


    }
    public function testUpdateCompleteActionForWASFailure()
    {

        $payload = [
            'id' => 172,
            'status' => 'P',
            'priority' => 2,
            'uid' => '',
            'startDate' => '2016-10-21 14:13:17',
            'scheduledDate' => '2016-10-21 14:13:17',
            'toAddr' => 'duit-soldel42@miamioh.edu',
            'fromAddr' => 'noreply@miamioh.edu',
        ];
        $configuration = [
            'email.devTeam' => 'duit-soldel42@miamioh.edu'
        ];
        
        $this->config->method('getConfiguration')
            ->with('SendNotification')
            ->willReturn($configuration);
        
        $this->response->method('getStatus')->willReturn(App::API_CREATED);

        $this->response->method('getPayload')->willReturn($payload);

        $this->dbh->method('perform')
            ->with($this->callback(array($this, 'performWithQueryUpdate')),
                $this->callback(array($this, 'performWithParamsUpdate')),
                $this->callback(array($this, 'performWithParamsUpdate')))
            ->willReturn(true);
        
        $this->was->expects($this->once())
            ->method('updateWAS')
            ->with($this->callback(array($this, 'updateWASWithUid')),
                $this->callback(array($this, 'updateWASWithOptions')))
            ->will($this->returnCallback(array($this, 'updateWASMock')));

        $this->updateWASResponse = '<xml><status>1</status><message></message></xml>';

        $this->action->updateCompleteAction('123456', 'doej', '201710');


        $this->assertTrue(stripos($this->queryStringPerform,
                'UPDATE SATURN.SZRSORM SET szrsorm_status =') !== false,
            'Query to update status to cancel');

        $this->assertEquals('doej', $this->updateWASUid, 'Update WAS with correct uniqueid');
        $this->assertTrue($this->updateWASOptions['complete'], 'Update WAS with complete true');


    }

    public function testUpdateEmail1Action()
    {

        $payload = [
            'id' => 172,
            'status' => 'P',
            'priority' => 2,
            'uid' => '',
            'startDate' => '2016-10-21 14:13:17',
            'scheduledDate' => '2016-10-21 14:13:17',
            'toAddr' => 'doej@miamioh.edu',
            'fromAddr' => 'noreply@miamioh.edu',
            'subject' => 'Please provide state of residence information',
            'body' => 'Click on below link to provide state of residence info',
        ];
        $configuration = [
            'email1.subject' => 'Please provide state of residence information',
            'email1.body' => 'Click on below link to provide state of residence info',
            'email2.subject' => 'Please provide state of residence information',
            'email2.body' => 'Click on below link to provide state of residence info'
        ];

        $this->studentIdentity = [
            'spriden_id' => "+00123456",
            'spriden_last_name' => "Doe",
            'spriden_first_name' => "Jane",
        ];


        $this->response->method('getStatus')->willReturn(App::API_CREATED);

        $this->response->method('getPayload')->willReturn($payload);

        $this->config->method('getConfiguration')
            ->with('SendNotification')
            ->willReturn($configuration);

        $this->dbh->method('perform')
            ->with($this->callback(array($this, 'performWithQueryUpdate')),
                $this->callback(array($this, 'performWithParamsUpdate')))
            ->willReturn(true);

        $this->dbh->expects($this->at(0))
            ->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithParam')))
            ->willReturn(0);

        $this->dbh->expects($this->at(1))
            ->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocWithQuery')),
                $this->callback(array($this, 'queryfirstrow_assocWithParam')))
            ->will($this->returnCallBack(array($this, 'queryfirstrow_assocMock')));

        $this->dbh->expects($this->at(2))
            ->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithParam')))
            ->willReturn("Fall Semester 2016-17");

        $response = $this->action->updateEmailAction('email1', 'doej',
            ['pidm' => '123456', 'termCode' => '201710', 'nextAction' => '', 'status' => '']);
        

        $this->assertTrue(stripos($this->queryStringPerform, 'INSERT INTO saturn.szrsora') !== false,
            'Query to insert into SZRSORA');

        $this->assertEquals($response, 1);
        $this->assertTrue(array_key_exists("email1.subject", $configuration) !== false,
            'Key of Config Mgr does not email1.subject key');


    }

    public function testUpdateEmail2Action()
    {

        $payload = [
            'id' => 172,
            'status' => 'P',
            'priority' => 2,
            'uid' => '',
            'startDate' => '2016-10-21 14:13:17',
            'scheduledDate' => '2016-10-21 14:13:17',
            'toAddr' => 'doej@miamioh.edu',
            'fromAddr' => 'noreply@miamioh.edu',
            'subject' => 'Please provide state of residence information',
            'body' => 'Click on below link to provide state of residence info',
        ];
        $configuration = [
            'email1.subject' => 'Please provide state of residence information',
            'email1.body' => 'Click on below link to provide state of residence info',
            'email2.subject' => 'Please provide state of residence information',
            'email2.body' => 'Click on below link to provide state of residence info'
        ];


        $this->studentIdentity = [
            'spriden_id' => "+00123456",
            'spriden_last_name' => "Doe",
            'spriden_first_name' => "Jane",
        ];


        $this->response->method('getStatus')->willReturn(App::API_CREATED);

        $this->response->method('getPayload')->willReturn($payload);

        $this->config->method('getConfiguration')
            ->with('SendNotification')
            ->willReturn($configuration);

        $this->dbh->method('perform')
            ->with($this->callback(array($this, 'performWithQueryUpdate')),
                $this->callback(array($this, 'performWithParamsUpdate')))
            ->willReturn(true);

        $this->dbh->expects($this->at(0))
            ->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithParam')))
            ->willReturn(0);

        $this->dbh->expects($this->at(1))
            ->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocWithQuery')),
                $this->callback(array($this, 'queryfirstrow_assocWithParam')))
            ->will($this->returnCallBack(array($this, 'queryfirstrow_assocMock')));

        $this->dbh->expects($this->at(2))
            ->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithParam')))
            ->willReturn("Fall Semester 2016-17");


        $response = $this->action->updateEmailAction('email2', 'doej',
            ['pidm' => '123456', 'termCode' => '201710', 'nextAction' => '', 'status' => '']);

        $this->assertTrue(stripos($this->queryStringPerform, 'INSERT INTO saturn.szrsora') !== false,
            'Query to insert into SZRSORA');

        $this->assertEquals($response, 1);

        $this->assertTrue(array_key_exists("email2.subject", $configuration) !== false,
            'Key of Config Mgr does not email2.subject key');


    }

    public function testUpdateEmailActionFailure()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Send email notification failed');

        $configuration = [
            'email1.subject' => 'Please provide state of residence information',
            'email1.body' => 'Click on below link to provide state of residence info',
            'email2.subject' => 'Please provide state of residence information',
            'email2.body' => 'Click on below link to provide state of residence info'
        ];

        $this->studentIdentity = [
            'spriden_id' => "+00123456",
            'spriden_last_name' => "Doe",
            'spriden_first_name' => "Jane",
        ];
        

        $this->response->method('getStatus')->willReturn(App::API_FAILED);

        $this->config->method('getConfiguration')
            ->with('SendNotification')
            ->willReturn($configuration);

        $this->dbh->method('perform')
            ->with($this->callback(array($this, 'performWithQueryUpdate')),
                $this->callback(array($this, 'performWithParamsUpdate')))
            ->willReturn(true);

        $this->dbh->expects($this->at(0))
            ->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithParam')))
            ->willReturn(0);

        $this->dbh->expects($this->at(1))
            ->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocWithQuery')),
                $this->callback(array($this, 'queryfirstrow_assocWithParam')))
            ->will($this->returnCallBack(array($this, 'queryfirstrow_assocMock')));

        $this->dbh->expects($this->at(2))
            ->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithParam')))
            ->willReturn("Fall Semester 2016-17");

        $response = $this->action->updateEmailAction('email1', 'doej',
            ['pidm' => '123456', 'termCode' => '201710', 'nextAction' => '', 'status' => '']);


    }

    public function testupdateLoginMessageAction()
    {
        $this->config->method('getConfiguration')
            ->with('ActionTrigger')
            ->willReturn(['minLoginMessageTerm' => '201710']);

        $this->was->expects($this->once())
            ->method('updateWAS')
            ->with($this->callback(array($this, 'updateWASWithUid')),
                $this->callback(array($this, 'updateWASWithOptions')))
            ->will($this->returnCallback(array($this, 'updateWASMock')));

        $this->updateWASResponse = '<xml><status>1</status><message>logged</message></xml>';
        $this->action->updateLoginMessageAction('doej',
            ['pidm' => '123456', 'termCode' => '201710', 'nextAction' => '', 'status' => '']);

        $this->assertEquals('doej', $this->updateWASUid, 'Update WAS with correct uniqueid');

        $this->assertEquals($this->updateWASOptions['complete'], false, 'Update WAS with complete false');
        $this->assertEquals($this->updateWASOptions['infoRequired'], true, 'Update WAS with infoRequired true');

    }

    public function testupdateLoginMessageActionMinTerm()
    {
        $this->config->method('getConfiguration')
            ->with('ActionTrigger')
            ->willReturn(['minLoginMessageTerm' => '201720']);

        $this->was->expects($this->never())
            ->method('updateWAS')
            ->with($this->callback(array($this, 'updateWASWithUid')),
                $this->callback(array($this, 'updateWASWithOptions')))
            ->will($this->returnCallback(array($this, 'updateWASMock')));

        $this->action->updateLoginMessageAction('doej',
            ['pidm' => '123456', 'termCode' => '201710', 'nextAction' => '', 'status' => '']);

    }

    public function testUpdateEmail1ActionEmailContent()
    {

        $payload = [
            'id' => 172,
            'status' => 'P',
            'priority' => 2,
            'uid' => '',
            'startDate' => '2016-10-21 14:13:17',
            'scheduledDate' => '2016-10-21 14:13:17',
            'toAddr' => 'doej@miamioh.edu',
            'fromAddr' => 'noreply@miamioh.edu',
            'subject' => 'Please provide state of residence information',
            'body' => 'Click on below link to provide state of residence info',
        ];
        $configuration = [
            'email1.subject' => 'Dear {firstName}, Please provide state of residence information',
            'email1.body' => 'Dear {firstName}, Click on below link to provide state of residence info {firstName}.',
            'email2.subject' => 'Dear {firstName}, Reminder. Please provide state of residence information',
            'email2.body' => 'Dear {firstName}, Reminder. Click on below link to provide state of residence info',
            'email.fromAddress' => 'sofr@miamioh.edu',
        ];

        $this->studentIdentity = [
            'spriden_id' => "+00123456",
            'spriden_last_name' => "Doe",
            'spriden_first_name' => "Jane",
        ];
        
        $this->response->method('getStatus')->willReturn(App::API_CREATED);

        $this->response->method('getPayload')->willReturn($payload);

        $this->config->method('getConfiguration')
            ->with('SendNotification')
            ->willReturn($configuration);

        $this->dbh->method('perform')
            ->with($this->callback(array($this, 'performWithQueryUpdate')),
                $this->callback(array($this, 'performWithParamsUpdate')))
            ->willReturn(true);

        $this->dbh->expects($this->at(0))
            ->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithParam')))
            ->willReturn(0);

        $this->dbh->expects($this->at(1))
            ->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocWithQuery')),
                $this->callback(array($this, 'queryfirstrow_assocWithParam')))
            ->will($this->returnCallBack(array($this, 'queryfirstrow_assocMock')));

        $this->dbh->expects($this->at(2))
            ->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithParam')))
            ->willReturn("Fall Semester 2016-17");

        $response = $this->action->updateEmailAction('email1', 'doej',
            ['pidm' => '123456', 'termCode' => '201710', 'nextAction' => '', 'status' => '']);

        $this->assertTrue(stripos($this->queryStringPerform, 'INSERT INTO saturn.szrsora') !== false,
            'Query to insert into SZRSORA');

        $this->assertEquals($response, 1);
        $this->assertTrue(array_key_exists("email1.subject", $configuration) !== false,
            'Key of Config Mgr does not email1.subject key');

        $this->assertStringContainsString('Dear Jane,', $this->resourceBeingCalledArgs['data']['data']['body']);
        $this->assertStringContainsString('Dear Jane,', $this->resourceBeingCalledArgs['data']['data']['subject']);
        $this->assertStringNotContainsString('{firstName}', $this->resourceBeingCalledArgs['data']['data']['body']);
        $this->assertEquals($configuration['email.fromAddress'], $this->resourceBeingCalledArgs['data']['data']['fromAddr']);
    }

    public function testUpdateEmail1ActionDefaultFromAddress()
    {

        $payload = [
            'id' => 172,
            'status' => 'P',
            'priority' => 2,
            'uid' => '',
            'startDate' => '2016-10-21 14:13:17',
            'scheduledDate' => '2016-10-21 14:13:17',
            'toAddr' => 'doej@miamioh.edu',
            'fromAddr' => 'noreply@miamioh.edu',
            'subject' => 'Please provide state of residence information',
            'body' => 'Click on below link to provide state of residence info',
        ];
        $configuration = [
            'email1.subject' => 'Dear {firstName}, Please provide state of residence information',
            'email1.body' => 'Dear {firstName}, Click on below link to provide state of residence info {firstName}.',
            'email2.subject' => 'Dear {firstName}, Reminder. Please provide state of residence information',
            'email2.body' => 'Dear {firstName}, Reminder. Click on below link to provide state of residence info',
            //'email.fromAddress' => 'stateofresidence@miamioh.edu',
        ];

        $this->studentIdentity = [
            'spriden_id' => "+00123456",
            'spriden_last_name' => "Doe",
            'spriden_first_name' => "Jane",
        ];


        $this->response->method('getStatus')->willReturn(App::API_CREATED);

        $this->response->method('getPayload')->willReturn($payload);

        $this->config->method('getConfiguration')
            ->with('SendNotification')
            ->willReturn($configuration);

        $this->dbh->method('perform')
            ->with($this->callback(array($this, 'performWithQueryUpdate')),
                $this->callback(array($this, 'performWithParamsUpdate')))
            ->willReturn(true);

        $this->dbh->expects($this->at(0))
            ->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithParam')))
            ->willReturn(0);

        $this->dbh->expects($this->at(1))
            ->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocWithQuery')),
                $this->callback(array($this, 'queryfirstrow_assocWithParam')))
            ->will($this->returnCallBack(array($this, 'queryfirstrow_assocMock')));

        $this->dbh->expects($this->at(2))
            ->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithParam')))
            ->willReturn("Fall Semester 2016-17");

        $response = $this->action->updateEmailAction('email1', 'doej',
            ['pidm' => '123456', 'termCode' => '201710', 'nextAction' => '', 'status' => '']);

        $this->assertTrue(stripos($this->queryStringPerform, 'INSERT INTO saturn.szrsora') !== false,
            'Query to insert into SZRSORA');

        $this->assertEquals($response, 1);
        $this->assertTrue(array_key_exists("email1.subject", $configuration) !== false,
            'Key of Config Mgr does not email1.subject key');

        $this->assertStringContainsString('Dear Jane,', $this->resourceBeingCalledArgs['data']['data']['body']);
        $this->assertStringContainsString('Dear Jane,', $this->resourceBeingCalledArgs['data']['data']['subject']);
        $this->assertStringNotContainsString('{firstName}', $this->resourceBeingCalledArgs['data']['data']['body']);
        $this->assertEquals('StateofResidence@miamioh.edu', $this->resourceBeingCalledArgs['data']['data']['fromAddr']);
    }

    public function testUpdateEmail1ActionOnSameDate()
    {

        $currentDate = date('Ymd');
        

        $this->studentIdentity = [
            'spriden_id' => "+00123456",
            'spriden_last_name' => "Doe",
            'spriden_first_name' => "Jane",
        ];
        
        $this->actionRecord = [
            'szrsora_pidm' => '123456',
            'szrsora_term_code' => '201720',
            'szrsora_action_trigger' => 'email1',
            'szrsora_action_date' => $currentDate,
        ];


        $this->response->method('getStatus')->willReturn(App::API_CREATED);


        $this->dbh->method('perform')
            ->with($this->callback(array($this, 'performWithQueryUpdate')),
                $this->callback(array($this, 'performWithParamsUpdate')))
            ->willReturn(true);

        $this->dbh->expects($this->at(0))
            ->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithParam')))
            ->willReturn(1);
        

        $response = $this->action->updateEmailAction('email1', 'doej',
            ['pidm' => '123456', 'termCode' => '201720', 'nextAction' => 'email1', 'status' => 'pending']);

        $this->assertTrue(stripos($this->queryStringPerform, 'INSERT INTO saturn.szrsora') !== false,
            'Query to insert into SZRSORA');
    }

    public function testupdateEmailAndLoginMessageAction()
    {
        $payload = [
            'id' => 172,
            'status' => 'P',
            'priority' => 2,
            'uid' => '',
            'startDate' => '2016-10-21 14:13:17',
            'scheduledDate' => '2016-10-21 14:13:17',
            'toAddr' => 'doej@miamioh.edu',
            'fromAddr' => 'noreply@miamioh.edu',
            'subject' => 'Please provide state of residence information',
            'body' => 'Click on below link to provide state of residence info',
        ];
        $configuration = [
            'SendNotification' => [
                'email.subject' => 'Dear {firstName}, Please provide state of residence information',
                'email.body' => 'Dear {firstName}, Click on below link to provide state of residence info {firstName}.',
                'email1.subject' => 'Dear {firstName}, Please provide state of residence information',
                'email1.body' => 'Dear {firstName}, Click on below link to provide state of residence info {firstName}.',
                'email2.subject' => 'Dear {firstName}, Reminder. Please provide state of residence information',
                'email2.body' => 'Dear {firstName}, Reminder. Click on below link to provide state of residence info',
                'email.fromAddress' => 'sofr@miamioh.edu',
            ],
            'ActionTrigger' => [
                'minLoginMessageTerm' => '201710',
            ]
        ];

        $this->studentIdentity = [
            'spriden_id' => "+00123456",
            'spriden_last_name' => "Doe",
            'spriden_first_name' => "Jane",
        ];

        $this->response->method('getStatus')->willReturn(App::API_CREATED);

        $this->response->method('getPayload')->willReturn($payload);

        $this->config->method('getConfiguration')
            ->will($this->onConsecutiveCalls($configuration['SendNotification'], $configuration['ActionTrigger']));

        $this->dbh->method('perform')
            ->with($this->callback(array($this, 'performWithQueryUpdate')),
                $this->callback(array($this, 'performWithParamsUpdate')))
            ->willReturn(true);

        $this->dbh->expects($this->at(0))
            ->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithParam')))
            ->willReturn(0);

        $this->dbh->expects($this->at(1))
            ->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocWithQuery')),
                $this->callback(array($this, 'queryfirstrow_assocWithParam')))
            ->will($this->returnCallBack(array($this, 'queryfirstrow_assocMock')));

        $this->dbh->expects($this->at(2))
            ->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithParam')))
            ->willReturn("Fall Semester 2016-17");

        $this->was->expects($this->once())
            ->method('updateWAS')
            ->with($this->callback(array($this, 'updateWASWithUid')),
                $this->callback(array($this, 'updateWASWithOptions')))
            ->will($this->returnCallback(array($this, 'updateWASMock')));

        $this->updateWASResponse = '<xml><status>1</status><message>logged</message></xml>';
        $this->action->updateEmailAndLoginMessage('doej',
            ['pidm' => '123456', 'termCode' => '201710', 'nextAction' => '', 'status' => '']);

        $this->assertEquals('doej', $this->updateWASUid, 'Update WAS with correct uniqueid');

        $this->assertEquals($this->updateWASOptions['complete'], false, 'Update WAS with complete false');
        $this->assertEquals($this->updateWASOptions['infoRequired'], true, 'Update WAS with infoRequired true');

    }

    public function queryfirstcolumnWithQuery($subject)
    {
        $this->firstColumnQueryString[] = $subject;
        return true;
    }

    public function queryfirstcolumnWithParam($subject)
    {
        $this->firstColumnParam[] = $subject;
        return true;
    }

    public function queryfirstrow_assocWithQuery($subject)
    {
        $this->firstRowQueryString[] = $subject;
        return true;
    }

    public function queryfirstrow_assocWithParam($subject)
    {
        $this->firstRowParam = $subject;
        return true;
    }
    public function queryfirstrow_assocWithQueryLatestEmail($subject)
    {
        $this->firstRowQueryStringLatestEmail[] = $subject;
        return true;
    }

    public function queryfirstrow_assocWithParamLatestEmail($subject)
    {
        $this->firstRowParamLatestEmail = $subject;
        return true;
    }


    public function performWithQueryUpdate($subject)
    {
        $this->queryStringPerform = $subject;
        return true;
    }

    public function performWithParamsUpdate($subject)
    {
        $this->queryParams[] = $subject;
        return true;
    }

    public function getConfigurationMockUpdate($subject)
    {
        $this->mockConfigurationItems = $subject;
    }

    public function callResourceWithName($subject)
    {
        $this->resourceBeingCalledName = $subject;
        return true;
    }

    public function callResourceWithArgs($subject)
    {
        $this->resourceBeingCalledArgs = $subject;
        return true;
    }

    public function updateWASWithUid($subject)
    {
        $this->updateWASUid = $subject;
        return true;
    }

    public function updateWASWithOptions($subject)
    {
        $this->updateWASOptions = $subject;
        return true;
    }

    public function updateWASMock()
    {
        return $this->updateWASResponse;
    }

    public function queryfirstrow_assocMock()
    {
        return $this->studentIdentity;
    }
    
    public function queryfirstrow_assocMockLatestDate()
    {
        return  DBH::DB_EMPTY_SET;
    }
    
    public function queryfirstrow_assocMockSameDate()
    {
        return  $this->actionRecord;
    }
}