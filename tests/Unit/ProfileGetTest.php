<?php
namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;

use MiamiOH\RESTng\Connector\DatabaseFactory;
use MiamiOH\RESTng\Core\DB\DBH;
use MiamiOH\StateOfResidenceWebService\Services\Config;
use MiamiOH\StateOfResidenceWebService\Services\Profile;
use MiamiOH\StateOfResidenceWebService\Services\WAS;

class ProfileGetTest extends TestCase
{

    private $profile;
    private $config;

    private $was;

    private $records = [];
    private $queryStringPerform = '';
    private $queryParams = [];
    private $queryProgramDesc = '';
    private $campusCodes = [];
    private $queryStringCampus = '';

    protected function setUp(): void
    {

        $this->identityRecords = [];
        $this->termRecords = [];
        $this->courseRecords = [];
        $this->queryStringPerform = '';
        $this->queryParams = [];
        $this->queryProgramDesc = '';
        $this->campusCodes = [];
        $this->queryStringCampus = '';


        $this->dbh = $this->getMockBuilder(DBH::class)
            ->setMethods(array('queryfirstrow_assoc', 'queryall_array', 'queryfirstcolumn', 'queryall_list'))
            ->getMock();


        $db = $this->getMockBuilder(DatabaseFactory::class)
            ->setMethods(array('getHandle'))
            ->getMock();

        $this->config = $this->getMockBuilder(Config::class)
            ->getMock();

        $this->config->method("getConfiguration")->will(
            $this->returnValueMap(
                [
                    [ 'CodeMap', ['courseTypeDesc' => '{ "ONLS": "Online Synchronous", "ONLA": "Online Asynchronous", "HYBS": "Hybrid Synchronous", "HYBA": "Hybrid Asynchronous" }'] ]
                ]
            )
        );

        $db->method('getHandle')->willReturn($this->dbh);

        $this->was = $this->getMockBuilder(WAS::class)
            ->setMethods(array('getWASStatus'))
            ->getMock();

        $this->profile = new Profile();

        $this->profile->setDatabase($db);
        $this->profile->setWas($this->was);
        $this->profile->setConfig($this->config);


    }

    public function testGetProfile()
    {

        $pidm = 123456;

        $configurationTrigger = [
            "triggerValueBeforeTermStart" => "-10 days",
            "triggerValueBetweenEmails" => "+10 days",
            "triggerValueAfterTermStart" => "+14 days",
            "beginTerm" => "201710"
        ];

        $configurationTerm = [
            '0' => '201710',
        ];

        $configurationCodeMap = [
            'courseTypeDesc' => '{ "ONLS": "Online Synchronous", "ONLA": "Online Asynchronous", "HYBS": "Hybrid Synchronous", "HYBA": "Hybrid Asynchronous" }'
        ];

        $map = [
            ['CurrentTerm', $configurationTerm],
            ['ActionTrigger', $configurationTrigger],
            ['CodeMap', $configurationCodeMap],
        ];

        $config = $this->getMockBuilder(Config::class)->getMock();

        $config->method('getConfiguration')
            ->willReturnMap($map);

        $this->profile->setConfig($config);

        $this->was->method('getWASStatus')
            ->with('doej')
            ->willReturn(1);

        $this->identityRecords = [

            'szbuniq_pidm' => 123456,
            'szbuniq_banner_id' => '+00123456',
            'szbuniq_unique_id' => 'doej',
            'spriden_first_name' => 'John',
            'spriden_last_name' => 'doe',
            'term' => [
                'szrsorm_pidm' => 123456,
                'szrsorm_term_code' => 201710,
                'stvterm_desc' => 'Fall Semester 2016-17',
                'stvterm_start_date' => '04-OCT-16',
                'stvterm_end_date' => '08-JAN-17',
                'szrsorm_status' => 'complete',
                'szrsorm_stat_code' => 'TX',
                'szrsorm_natn_code' => 'US',
                'szrsorm_lastseen_date' => '10-OCT-16',
                'szrsorm_lastconfirmed_date' => '10-OCT-16',
                'readOnly' => true,
                'szrsorm_admin_status' => 'pending',
                'szrsorm_comment' => 'to be resolved',
                'courseList' => [
                    'sfrstcr_pidm' => 123456,
                    'sfrstcr_term_code' => 201710,
                    'sfrstcr_crn' => '50289',
                    'course_subject' => 'AES',
                    'course_number' => '110',
                    'course_section' => 'A',
                    'course_title' => 'Leadership Laboratory',
                    'campusDesc' => 'Hamilton',
                ]
            ]

        ];

        $this->termRecords = [
            [
                'szrsorm_pidm' => 123456,
                'szrsorm_term_code' => 201710,
                'stvterm_desc' => 'Fall Semester 2016-17',
                'szbuniq_unique_id' =>'doej',
                'stvterm_start_date' => '04-OCT-16',
                'stvterm_end_date' => '08-JAN-17',
                'szrsorm_status' => 'complete',
                'szrsorm_stat_code' => 'TX',
                'szrsorm_natn_code' => 'US',
                'szrsorm_lastseen_date' => '10-OCT-16',
                'szrsorm_lastconfirmed_date' => '10-OCT-16',
                'szrsorm_admin_status' => 'pending',
                'szrsorm_comment' => 'to be resolved',
                'szrsorm_priority'=>'5',
            ]
        ];

        $this->courseRecords = [
            [
                'sfrstcr_pidm' => 123456,
                'sfrstcr_term_code' => 201710,
                'sfrstcr_crn' => '50289',
                'stvcamp_desc' => 'Hamilton',
                'course_subject' => 'AES',
                'course_number' => '110',
                'course_section' => 'A',
                'course_title' => 'Leadership Laboratory',
                'course_type' => 'IVDL',
                'course_type_desc' => 'Interactive Video Dist Learn',
            ]
        ];

        $programDesc = [
            'Bachelor of Science in Nursing',
            'Master of Education',
        ];

        $this->recordForStartDate = "14-OCT-16";

        $this->campusCodes = [
          'sgbstdn_coll_code_1' => 'RC',
          'sgbstdn_coll_code_2' => 'IS',
        ];

        $this->dbh->expects($this->at(0))->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocWithQuery')),
                $this->callback(array($this, 'queryfirstrow_assocWithParams')))
            ->will($this->returnCallback(array($this, 'queryfirstrow_assocMock')));

        $this->dbh->expects(($this->at(1)))
            ->method('queryall_list')
            ->willReturn($programDesc);

        $this->dbh->expects(($this->at(2)))
            ->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQueryTerm')),
                $this->callback(array($this, 'queryall_arrayWithParamsTerm')))
            ->will($this->returnCallback(array($this, 'queryall_arrayMockTerm')));

        $this->dbh->expects($this->at(3))
            ->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithParams')))
            ->will($this->returnCallback(array($this, 'queryfirstcolumnMock')));

        $this->dbh->expects(($this->at(4)))
            ->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQueryCourse')),
                $this->callback(array($this, 'queryall_arrayWithParamsCourse')))
            ->will($this->returnCallback(array($this, 'queryall_arrayMockCourse')));

        $this->dbh->expects($this->at(5))->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocWithQueryCampus')),
                '123456')
            ->will($this->returnCallback(array($this, 'queryfirstrow_assocMockCampus')));


        $model = $this->profile->getProfile($pidm);

        print var_dump($model);

    }

    public function queryfirstrow_assocWithQuery($subject)
    {
        $this->queryString = $subject;
        return true;
    }

    public function queryfirstrow_assocWithParams($subject)
    {
        $this->queryParams = $subject;
        return true;
    }

    public function queryfirstrow_assocMock()
    {
        return $this->identityRecords;
    }

    public function queryall_arrayWithQueryTerm($subject)
    {
        $this->queryString = $subject;
        return true;
    }

    public function queryall_arrayWithParamsTerm($subject)
    {
        $this->queryParams = $subject;
        return true;
    }

    public function queryall_arrayMockTerm()
    {
        return $this->termRecords;
    }

    public function queryall_arrayWithQueryCourse($subject)
    {
        $this->queryString = $subject;
        return true;
    }

    public function queryall_arrayWithParamsCourse($subject)
    {
        $this->queryParams = $subject;
        return true;
    }

    public function queryall_arrayMockCourse()
    {
        return $this->courseRecords;
    }

    public function queryfirstcolumnWithQuery($subject)
    {
        $this->queryString = $subject;
        return true;
    }

    public function queryfirstcolumnWithParams($subject)
    {
        $this->queryParams = $subject;
        return true;
    }

    public function queryfirstcolumnMock()
    {
        return $this->recordForStartDate;
    }

    public function queryall_arrayWithQueryProgramDesc($subject) {
        $this->queryProgramDesc = $subject;
        return true;
    }

    public function queryfirstrow_assocWithQueryCampus($subject)
    {
        $this->queryStringCampus = $subject;
        return true;
    }

    public function queryfirstrow_assocMockCampus()
    {
        return $this->campusCodes;
    }
}
