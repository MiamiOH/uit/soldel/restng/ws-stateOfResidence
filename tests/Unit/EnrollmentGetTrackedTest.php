<?php
namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;

use MiamiOH\RESTng\Connector\DatabaseFactory;
use MiamiOH\RESTng\Core\DB\DBH;
use MiamiOH\StateOfResidenceWebService\Services\Config;
use MiamiOH\StateOfResidenceWebService\Services\Enrollment;

class EnrollmentGetTrackedTest extends TestCase
{


    private $enrollmentTrack;
    private $config;
    private $sth;

    private $records =[];
    private $prepareQueryString = '';
    private $DBRecord = [];
    private $currentTrackedEnrollment = 0;
    private $trackedEnrollmentRecords = [];
    private $daysUntilTermStart = 0;
    private $actionRecords = [];
    private $actionRecordForPidm = '';
    private $index = 0;

    protected function setUp(): void
    {

        $this->records = [];
        $this->prepareQueryString = '';
        $this->DBRecord = [];
        $this->currentTrackedEnrollment = 0;
        $this->trackedEnrollmentRecords = [];
        $this->daysUntilTermStart = 0;
        $this->actionRecords = [];
        $this->actionRecordForPidm = '';
        $this->index = 0;

        $this->dbh = $this->getMockBuilder(DBH::class)
            ->setMethods(array('prepare','queryfirstcolumn', 'queryall_array', 'queryfirstrow_assoc'))
            ->getMock();

        $this->sth = $this->getMockBuilder('\MiamiOH\RESTng\Legacy\DB\MU_STH')
            ->setMethods(array('fetchrow_assoc', 'execute'))
            ->getMock();

        $db = $this->getMockBuilder(DatabaseFactory::class)
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->config = $this->getMockBuilder(Config::class)
            ->setMethods(array('getConfiguration'))
            ->getMock();

        $this->enrollmentTrack = new Enrollment();

        $this->enrollmentTrack->setDatabase($db);
        $this->enrollmentTrack->setConfig($this->config);
    }

    // -no previous action and term start - today > 10: email1
    // -no previous action and term start - today <= 10: email and loginMsg
    // last action email1 and today - last action date >= interval: email2
    // last action email2 and today - last action date >= interval: loginMsg
    // last action loginMsg: no action
    // last action email and loginmsg: no action
    public function testGetNextActionFlow1WithoutPrevious()
    {

        $this->trackedEnrollmentRecords = [
            [
                'pidm' => '123456',
                'term_code' => '201710',
                'days_since_seen' => 0,
                'confirmed_today' => 1,
                'status' => 'pending',
            ],
            [
                'pidm' => '123457',
                'term_code' => '201710',
                'days_since_seen' => 1,
                'confirmed_today' => 1,
                'status' => 'pending',
            ],
            [
                'pidm' => '123458',
                'term_code' => '201710',
                'days_since_seen' => 1,
                'confirmed_today' => 1,
                'status' => 'complete',
            ],
            [
                'pidm' => '123459',
                'term_code' => '201710',
                'days_since_seen' => 0,
                'confirmed_today' => 1,
                'status' => 'complete',
            ],
        ];
        $this->currentTrackedEnrollment = 0;

        $this->daysUntilTermStart = 20;
      
       
        $configuration = [
            'requireLoginMsgDaysBeforeTerm' => '10',
            'waitDaysAfterEmail1' => '3',
            'waitDaysAfterEmail2' => '3',
           
        ];
        $this->config = $this->getMockBuilder('\RESTng\Service\StateOfResidence\Config')
            ->setMethods(array('getConfiguration'))
            ->getMock();
        $this->dbh->expects($this->once())->method('prepare')
            ->with($this->callback(array($this, 'prepareWithQuery')))
            ->willReturn($this->sth);

        $this->sth->method('execute')
            ->willReturn(true);

        $this->sth->method('fetchrow_assoc')
            ->will($this->returnCallback(array($this, 'fetchrow_assocEnrollmentMock')));

        $this->dbh->expects($this->once())->method('queryfirstcolumn')
            ->willReturn($this->daysUntilTermStart);

        $this->config->method('getConfiguration')
            ->with('ActionTrigger')
            ->willReturn($configuration);
        $this->config->method('getConfiguration')
            ->with('CurrentTerm')
            ->willReturn($configuration);

        $model = $this->enrollmentTrack->getTrackedInfo();
        
        $this->assertTrue(is_array($model));
        $this->assertEquals(count($this->trackedEnrollmentRecords), count($model));
        $this->assertEquals('email1', $model[0]['nextAction']);
        $this->assertEquals('cancel', $model[1]['nextAction']);
        $this->assertEquals('cancel', $model[2]['nextAction']);
        $this->assertEquals('', $model[3]['nextAction']);

    }

    public function testGetNextActionFlow2WithoutPrevious()
    {
        $this->trackedEnrollmentRecords = [
            [
                'pidm' => '123456',
                'term_code' => '201710',
                'days_since_seen' => 0,
                'confirmed_today' => 1,
                'status' => 'pending',
            ],
            [
                'pidm' => '123457',
                'term_code' => '201710',
                'days_since_seen' => 1,
                'confirmed_today' => 1,
                'status' => 'pending',
            ],
            [
                'pidm' => '123458',
                'term_code' => '201710',
                'days_since_seen' => 1,
                'confirmed_today' => 1,
                'status' => 'complete',
            ],
            [
                'pidm' => '123459',
                'term_code' => '201710',
                'days_since_seen' => 0,
                'confirmed_today' => 1,
                'status' => 'complete',
            ],
        ];
        $this->currentTrackedEnrollment = 0;

        $this->daysUntilTermStart = 5;

        $configuration = [
            'requireLoginMsgDaysBeforeTerm' => '10',
            'waitDaysAfterEmail1' => '3',
            'waitDaysAfterEmail2' => '3',
        ];
        $this->config = $this->getMockBuilder('\RESTng\Service\StateOfResidence\Config')
            ->setMethods(array('getConfiguration'))
            ->getMock();
        $this->config->method('getConfiguration')
            ->with('ActionTrigger')
            ->willReturn($configuration);
        $this->config->method('getConfiguration')
            ->with('CurrentTerm')
            ->willReturn($configuration);

        $this->dbh->expects($this->once())->method('prepare')
            ->with($this->callback(array($this, 'prepareWithQuery')))
            ->willReturn($this->sth);

        $this->sth->method('execute')
            ->willReturn(true);

        $this->sth->method('fetchrow_assoc')
            ->will($this->returnCallback(array($this, 'fetchrow_assocEnrollmentMock')));

        $this->dbh->expects($this->once())->method('queryfirstcolumn')
            ->willReturn($this->daysUntilTermStart);

        $model = $this->enrollmentTrack->getTrackedInfo();

        $this->assertTrue(is_array($model));
        $this->assertEquals(count($this->trackedEnrollmentRecords), count($model));
        $this->assertEquals('email1', $model[0]['nextAction']);
        $this->assertEquals('cancel', $model[1]['nextAction']);
        $this->assertEquals('cancel', $model[2]['nextAction']);
        $this->assertEquals('', $model[3]['nextAction']);

    }

    public function testGetNextActionWithPrevious()
    {
        $this->trackedEnrollmentRecords = [
            [
                'pidm' => '123456',
                'term_code' => '201710',
                'days_since_seen' => 0,
                'confirmed_today' => 1,
                'status' => 'pending',
                'last_action' => 'email1',
                'days_since_action' => 4,
            ],
            [
                'pidm' => '123457',
                'term_code' => '201710',
                'days_since_seen' => 0,
                'confirmed_today' => 1,
                'status' => 'pending',
                'last_action' => 'email2',
                'days_since_action' => 4,
            ],
            [
                'pidm' => '123458',
                'term_code' => '201710',
                'days_since_seen' => 0,
                'confirmed_today' => 1,
                'status' => 'pending',
                'last_action' => 'loginMessage',
                'days_since_action' => 4,
            ],
            [
                'pidm' => '123459',
                'term_code' => '201710',
                'days_since_seen' => 0,
                'confirmed_today' => 1,
                'status' => 'pending',
                'last_action' => 'email1',
                'days_since_action' => 1,
            ],
            [
                'pidm' => '123450',
                'term_code' => '201710',
                'days_since_seen' => 0,
                'confirmed_today' => 1,
                'status' => 'pending',
                'last_action' => 'emailAndLoginMessage',
                'days_since_action' => 1,
            ],
        ];
        $this->currentTrackedEnrollment = 0;

        $this->daysUntilTermStart = 20;

        $configuration = [
            'requireLoginMsgDaysBeforeTerm' => '10',
            'waitDaysAfterEmail1' => '3',
            'waitDaysAfterEmail2' => '3',
            
            
        ];

        $this->config = $this->getMockBuilder('\RESTng\Service\StateOfResidence\Config')
            ->setMethods(array('getConfiguration'))
            ->getMock();

        $this->config->method('getConfiguration')
            ->with('ActionTrigger')
            ->willReturn($configuration);
        
        $this->config->method('getConfiguration')
            ->with('CurrentTerm')
            ->willReturn($configuration);

        $this->dbh->expects($this->once())->method('prepare')
            ->with($this->callback(array($this, 'prepareWithQuery')))
            ->willReturn($this->sth);

        $this->sth->method('execute')
            ->willReturn(true);

        $this->sth->method('fetchrow_assoc')
            ->will($this->returnCallback(array($this, 'fetchrow_assocEnrollmentMock')));

        $this->dbh->expects($this->never())->method('queryfirstcolumn')
            ->willReturn($this->daysUntilTermStart);

        $model = $this->enrollmentTrack->getTrackedInfo();
        
        $this->assertTrue(is_array($model));
        $this->assertEquals(count($this->trackedEnrollmentRecords), count($model));
        $this->assertEquals('email2', $model[0]['nextAction']);
        $this->assertEquals('loginMessage', $model[1]['nextAction']);
        $this->assertEquals('', $model[2]['nextAction']);
        $this->assertEquals('email2', $model[3]['nextAction']);
        $this->assertEquals('', $model[4]['nextAction']);
        

    }

    public function prepareWithQuery($subject)
    {
        $this->prepareQueryString = $subject;
        return true;
    }

    public function fetchrow_assocMock()
    {
        $nextIndex = $this->index++;
        if(!isset($this->DBRecord[$nextIndex])) {
            return null;
        }
        return $this->DBRecord[$nextIndex];

    }

    public function fetchrow_assocEnrollmentMock()
    {
        if(isset($this->trackedEnrollmentRecords[$this->currentTrackedEnrollment])) {
            $index = $this->currentTrackedEnrollment;
            $this->currentTrackedEnrollment++;
            return $this->trackedEnrollmentRecords[$index];
        }

        return null;

    }

    public function queryfirstrow_assocWithQuery($subject)
    {
        return true;
    }

    public function queryfirstrow_assocWithParams($subject)
    {
        $this->actionRecordForPidm = $subject;
        return true;
    }

    public function queryfirstrow_assocMock()
    {
        if (isset($this->actionRecords[$this->actionRecordForPidm])) {
            return $this->actionRecords[$this->actionRecordForPidm];
        } else {
            return DBH::DB_EMPTY_SET;
        }
//        return isset($this->actionRecords[$this->actionRecordForPidm]) ?
//            $this->actionRecords[$this->actionRecordForPidm] : \RESTng\Core\DB\DBH::DB_EMPTY_SET;
    }
}