<?php
namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Connector\DatabaseFactory;
use MiamiOH\RESTng\Connector\DataSource;
use MiamiOH\RESTng\Connector\DataSourceFactory;
use MiamiOH\RESTng\Core\DB\DBH;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\User;
use MiamiOH\StateOfResidenceWebService\Services\Action;
use MiamiOH\StateOfResidenceWebService\Services\Audit;
use MiamiOH\StateOfResidenceWebService\Services\Profile;
use MiamiOH\StateOfResidenceWebService\Services\TermStatus;

class ProfileUpdateTest extends TestCase
{

    private $profile;
    private $action;
    private $insertAudit;
    private $termStatus;
    private $config;

    private $apiUser;
    private $dsFactoryObj;

    private $data = [];
    private $queryStringPerform = '';
    private $queryParams = [];
    /** @var  Response $callResourceResponse */
    private $callResourceResponse = null;
    private $resourceBeingCalledName = '';
    private $resourceBeingCalledArgs = array();

    protected function setUp(): void
    {

        $this->data = [];
        $this->recordIndex = 0;
        $this->queryStringPerform = '';
        $this->queryParams = [];
        $this->resourceBeingCalledName = '';
        $this->resourceBeingCalledArgs = array();

        $this->app = $this->getMockBuilder(App::class)
            ->setMethods(array('newResponse', 'callResource'))
            ->getMock();

        $this->apiUser = $this->getMockBuilder(User::class)
            ->setMethods(array('addUserByCredentials', 'removeUser'))
            ->getMock();

        /** @var  Response $callResourceResponse */
        $this->callResourceResponse = new Response();

        $this->app->method('callResource')
            ->with($this->callback(array($this, 'callResourceWithName')),
                $this->callback(array($this, 'callResourceWithArgs')))
            ->will($this->returnCallback(array($this, 'callResourceMock')));

        $this->dsFactoryObj = $this->getMockBuilder(DataSourceFactory::class)
            ->setMethods(array('getDataSource'))
            ->getMock();

        $this->action = $this->getMockBuilder(Action::class)
            ->setMethods(array('updateCompleteAction'))
            ->getMock();

        $db = $this->getMockBuilder(DatabaseFactory::class)
            ->setMethods(array('getHandle'))
            ->getMock();

        $this->dbh = $this->getMockBuilder(DBH::class)
            ->setMethods(array('auto_commit', 'perform', 'commit', 'rollback', 'queryall_array', 'queryfirstcolumn'))
            ->getMock();

        $this->config = $this->getMockBuilder(Config::class)
            ->setMethods(array('getConfiguration'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->profile = new Profile();

        $this->profile->setDatabase($db);
        $this->profile->setAction($this->action);
        $this->profile->setConfig($this->config);
        $this->profile->setApp($this->app);

        $this->insertAudit = $this->getMockBuilder(Audit::class)
            ->setMethods(array('insertAuditTable'))
            ->getMock();

        $this->profile->setAudit($this->insertAudit);

        $this->termStatus = $this->getMockBuilder(TermStatus::class)
            ->setMethods(array('getTermStatus'))
            ->getMock();

        $this->profile->setTermStatus($this->termStatus);
        $this->profile->setDataSourceFactory($this->dsFactoryObj);
        $this->profile->setApiUser($this->apiUser);

    }

    public function testUpdateProfileWithStateAndCountryInfo()
    {

        $this->data = [
            'pidm' => 123456,
            'uniqueId' => 'doej',
            'firstName' => 'John',
            'lastName' => 'doe',
            'term' => [
                [
                    'pidm' => 0,
                    'termCode' => '201715',
                    'termDesc' => 'Spring term',
                    'status' => 'complete',
                    'country' => 'US',
                    'state' => 'TX',
                    'lastSeenDate' => 0,
                    'courseList' => [
                        [
                            'subject' => 'CS',
                            'number' => 0
                        ]
                    ]
                ]
            ]
        ];

        $currentDate = date('Ymd');


        $configurationTrigger = [
            'triggerValueAfterTermStart' => date('Ymd',strtotime($currentDate. ' +14 days')),

        ];

        $configurationTerm = [
            '0' => '201710',
        ];

        $actionConfiguration = [
            'confirmation.subject' => 'SofR Confirmation',
            'confirmation.body' => "Your information has been received.\n\n{termInfo}\nThanks",
            'email.fromAddress' => 'sofr@miamioh.edu',
        ];

        $termStatusOptions = [
            'termCode' => '201715',
            'country' => 'US',
            'state' => 'TX',
        ];

        $mockDataSource = [
            'user' => 'SofR_WS_User',
            'password' => 'secret',
            'type' => 'Other',
            'name' => 'RestNG'
        ];

        $this->recordForStartDate = "14-OCT-16";

        $this->config->expects($this->at(0))
            ->method('getConfiguration')
            ->with('ActionTrigger')
            ->willReturn($configurationTrigger);

        $this->config->expects($this->at(1))
            ->method('getConfiguration')
            ->with('CurrentTerm')
            ->willReturn($configurationTerm);

        $this->config->expects($this->at(2))
            ->method('getConfiguration')
            ->with('SendNotification')
            ->willReturn($actionConfiguration);

        $this->dbh->expects($this->once())
            ->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithParams')))
            ->will($this->returnCallback(array($this, 'queryfirstcolumnMock')));

        $this->dbh->method('perform')
            ->with($this->callback(array($this, 'performWithQuery')),
                $this->callback(array($this, 'performWithParams')))
            ->willReturn(true);

        $this->action->method('updateCompleteAction')
            ->with(123456, 'doej', '201715')
            ->willReturn(true);

        $this->termStatus->method('getTermStatus')
            ->with(123456, $termStatusOptions)
            ->willReturn(['status' => true, 'priority' => 5]);

        $this->dsFactoryObj->expects($this->once())->method('getDataSource')
            ->willReturn(DataSource::fromArray($mockDataSource));

        $this->apiUser->expects($this->once())->method('addUserByCredentials')
            ->willReturn('abc123');

        $this->apiUser->expects($this->once())->method('removeUser')
            ->with($this->equalTo('abc123'));

        $this->insertAudit->expects($this->exactly(count($this->data['term'])))->method('insertAuditTable')
            ->willReturn(true);

        $this->callResourceResponse->setStatus(App::API_CREATED);

        $this->profile->updateProfile(123456, $this->data);

        $this->assertTrue(stripos($this->queryStringPerform,
                'UPDATE SATURN.SZRSORM SET szrsorm_natn_code = ?, szrsorm_stat_code=?') !== false,
            'Query from szrsorm');

        $this->assertEquals($this->queryParams[1], 'TX',
            'Verify state is set to TX');

        $this->assertEquals('notification.v1.email.message.create', $this->resourceBeingCalledName);
        $this->assertStringContainsString('information has been received', $this->resourceBeingCalledArgs['data']['data']['body']);
        $this->assertStringContainsString('SofR Confirmation', $this->resourceBeingCalledArgs['data']['data']['subject']);
        $this->assertStringNotContainsString('{termInfo}', $this->resourceBeingCalledArgs['data']['data']['body']);
        $this->assertEquals($actionConfiguration['email.fromAddress'], $this->resourceBeingCalledArgs['data']['data']['fromAddr']);

    }

    public function testUpdateProfileForInternationalStudent()
    {

        $this->data = [
            'pidm' => 987654,
            'uniqueId' => 'sharmg',
            'firstName' => 'Geeth',
            'lastName' => 'Sharma',
            'term' => [
                [
                    'pidm' => 0,
                    'termCode' => '201710',
                    'termDesc' => 'Fall term',
                    'status' => 'complete',
                    'country' => 'IN',
                    'state' => null,
                    'lastSeenDate' => 0,
                    'courseList' => [
                        [
                            'subject' => 'CS',
                            'number' => 0
                        ]
                    ]
                ]
            ]
        ];
        $currentDate = date('Ymd');


        $configurationTrigger = [
            'triggerValueAfterTermStart' => date('Ymd',strtotime($currentDate. ' +14 days')),

        ];

        $configurationTerm = [
            '0' => '201710',
        ];

        $termStatusOptions = [
            'termCode' => '201710',
            'country' => 'IN',
            'state' => null,
        ];
        
        $this->recordForStartDate = "14-OCT-16";

        $mockDataSource = [
            'user' => 'SofR_WS_User',
            'password' => 'secret',
            'type' => 'Other',
            'name' => 'RestNG'
        ];

        $this->config->expects($this->at(0))
            ->method('getConfiguration')
            ->with('ActionTrigger')
            ->willReturn($configurationTrigger);

        $this->config->expects($this->at(1))
            ->method('getConfiguration')
            ->with('CurrentTerm')
            ->willReturn($configurationTerm);


        $this->dbh->expects($this->once())
            ->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithParams')))
            ->will($this->returnCallback(array($this, 'queryfirstcolumnMock')));

        $this->dbh->method('perform')
            ->with($this->callback(array($this, 'performWithQuery')),
                $this->callback(array($this, 'performWithParams')))
            ->willReturn(true);

        $this->termStatus->method('getTermStatus')
            ->with(987654, $termStatusOptions)
            ->willReturn(['status' => true, 'priority' => 5]);

        $this->dsFactoryObj->expects($this->once())->method('getDataSource')
            ->willReturn(DataSource::fromArray($mockDataSource));

        $this->apiUser->expects($this->once())->method('addUserByCredentials')
            ->willReturn('abc123');

        $this->apiUser->expects($this->once())->method('removeUser')
            ->with($this->equalTo('abc123'));

        $this->callResourceResponse->setStatus(App::API_CREATED);

        $this->profile->updateProfile(987654, $this->data);


        $this->assertTrue(stripos($this->queryStringPerform,
                'UPDATE SATURN.SZRSORM SET szrsorm_natn_code = ?, szrsorm_stat_code=?') !== false,
            'Query from szrsorm');
        $this->assertTrue(stripos($this->queryStringPerform, 'szrsorm_priority = ?') !== false,
            'Query from szrsorm has update to priority column');
        $this->assertEquals($this->queryParams[1], null,
            'Verify state is set to null');

    }

    public function testUpdateProfileConfirmationUnauthorized()
    {
        $this->expectException(\Exception::class);

        $this->data = [
            'pidm' => 123456,
            'uniqueId' => 'doej',
            'firstName' => 'John',
            'lastName' => 'doe',
            'term' => [
                [
                    'pidm' => 0,
                    'termCode' => '201715',
                    'termDesc' => 'Spring term',
                    'status' => 'complete',
                    'country' => 'US',
                    'state' => 'TX',
                    'lastSeenDate' => 0,
                    'courseList' => [
                        [
                            'subject' => 'CS',
                            'number' => 0
                        ]
                    ]
                ]
            ]
        ];

        $currentDate = date('Ymd');


        $configurationTrigger = [
            'triggerValueAfterTermStart' => date('Ymd',strtotime($currentDate. ' +14 days')),

        ];

        $configurationTerm = [
            '0' => '201710',
        ];

        $actionConfiguration = [
            'confirmation.subject' => 'SofR Confirmation',
            'confirmation.body' => "Your information has been received.\n\n{termInfo}\nThanks",
            'email.fromAddress' => 'sofr@miamioh.edu',
        ];

        $termStatusOptions = [
            'termCode' => '201715',
            'country' => 'US',
            'state' => 'TX',
        ];

        $this->recordForStartDate = "14-OCT-16";

        $mockDataSource = [
            'user' => 'SofR_WS_User',
            'password' => 'secret',
            'type' => 'Other',
            'name' => 'RestNG'
        ];

        $this->config->expects($this->at(0))
            ->method('getConfiguration')
            ->with('ActionTrigger')
            ->willReturn($configurationTrigger);

        $this->config->expects($this->at(1))
            ->method('getConfiguration')
            ->with('CurrentTerm')
            ->willReturn($configurationTerm);

        $this->config->expects($this->at(2))
            ->method('getConfiguration')
            ->with('SendNotification')
            ->willReturn($actionConfiguration);

        $this->dbh->expects($this->once())
            ->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithParams')))
            ->will($this->returnCallback(array($this, 'queryfirstcolumnMock')));

        $this->dbh->method('perform')
            ->with($this->callback(array($this, 'performWithQuery')),
                $this->callback(array($this, 'performWithParams')))
            ->willReturn(true);

        $this->action->method('updateCompleteAction')
            ->with(123456, 'doej', '201715')
            ->willReturn(true);

        $this->termStatus->method('getTermStatus')
            ->with(123456, $termStatusOptions)
            ->willReturn(['status' => true, 'priority' => 5]);

        $this->dsFactoryObj->expects($this->once())->method('getDataSource')
            ->willReturn(DataSource::fromArray($mockDataSource));

        $this->apiUser->expects($this->once())->method('addUserByCredentials')
            ->willReturn('abc123');

        $this->apiUser->expects($this->once())->method('removeUser')
            ->with($this->equalTo('abc123'));

        $this->insertAudit->expects($this->exactly(count($this->data['term'])))->method('insertAuditTable')
            ->willReturn(true);

        $this->callResourceResponse->setStatus(App::API_UNAUTHORIZED);

        $ds = $this->profile->updateProfile(123456, $this->data);


        $this->assertTrue(stripos($this->queryStringPerform,
                'UPDATE SATURN.SZRSORM SET szrsorm_natn_code = ?, szrsorm_stat_code=?') !== false,
            'Query from szrsorm');

        $this->assertEquals($this->queryParams[1], 'TX',
            'Verify state is set to TX');

        $this->assertEquals('notification.v1.email.message.create', $this->resourceBeingCalledName);
        $this->assertContains('information has been received', $this->resourceBeingCalledArgs['data']['data']['body']);
        $this->assertContains('SofR Confirmation', $this->resourceBeingCalledArgs['data']['data']['subject']);
        $this->assertNotContains('{termInfo}', $this->resourceBeingCalledArgs['data']['data']['body']);
        $this->assertEquals($actionConfiguration['email.fromAddress'], $this->resourceBeingCalledArgs['data']['data']['fromAddr']);

    }

    public function performWithQuery($subject)
    {
        $this->queryStringPerform = $subject;
        return true;
    }

    public function performWithParams($subject)
    {
        $this->queryParams = $subject;
        return true;
    }

    public function queryfirstcolumnWithQuery($subject)
    {
        $this->queryString = $subject;
        return true;
    }

    public function queryfirstcolumnWithParams($subject)
    {
        $this->queryParams = $subject;
        return true;
    }

    public function queryfirstcolumnMock()
    {
        return $this->recordForStartDate;
    }

    public function callResourceWithName($subject)
    {
        $this->resourceBeingCalledName = $subject;
        return true;
    }

    public function callResourceWithArgs($subject)
    {
        $this->resourceBeingCalledArgs = $subject;
        return true;
    }

    public function callResourceMock()
    {
        return $this->callResourceResponse;
    }

}