<?php

namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;


use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Service\Extension\BannerId;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\StateOfResidenceWebService\Services\AdminStatus;
use MiamiOH\StateOfResidenceWebService\Services\AdminStatusService;

class AdminPutServiceTest extends TestCase
{
    private $adminService;

    private $admin;

    private $bannerId;

    private $mockModel = [];

    private $requestResourceParam = '';

    private $requestResourceParamMocks = [];

    /**
     * set up method which is automatically called by PHPUnit before every test method:
     */
    protected function setUp(): void
    {

        $this->mockModel = [];
        $this->requestResourceParam = '';
        $this->requestResourceParamMocks = [];

        //set up the mock api:
        $this->app = $this->getMockBuilder(App::class)
            ->setMethods(array('newResponse'))
            ->getMock();

        $this->app->method('newResponse')->willReturn(new Response());

        $this->admin = $this->getMockBuilder(AdminStatus::class)
            ->setMethods(array('updateAdminStatus'))
            ->getMock();

        $this->admin->method('updateAdminStatus')
            ->with($this->callback(array($this, 'updateAdminStatusWith')))
            ->willReturn(true);

        $this->request = $this->getMockBuilder(Request::class)
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();

        $this->bannerId = $this->getMockBuilder(BannerId::class)
            ->setMethods(array('getPidm'))
            ->getMock();

        $bannerUtil = $this->getMockBuilder(BannerUtil::class)
            ->setMethods(array('getId'))
            ->getMock();

        $bannerUtil->method('getId')->willReturn($this->bannerId);

        //set up the service with the mocked out resources:
        $logger = $this->getMockBuilder('Logger')
            ->setConstructorArgs(array('logger'))
            ->setMethods(array('debug', 'info','error'))
            ->getMock();
        $this->adminService = new AdminStatusService();
        $reflection = new \ReflectionClass($this->adminService);
        $reflection_property = $reflection->getProperty('log');
        $reflection_property->setAccessible(true);

        $reflection_property->setValue($this->adminService, $logger);

        $this->adminService->setApp($this->app);
        $this->adminService->setAdmin($this->admin);
        $this->adminService->setBannerUtil($bannerUtil);

    }

    public function testUpdateAdminStatus()
    {
        $this->mockModel = [
            [
                'pidm' => '123456',
                'termCode' => '201710',
                'adminStatus'=> 'resolved',
                'comment' => 'Admin'
            ],
        ];

        $this->bannerId->method('getPidm')->willReturn('123456');

        $this->request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('muid'))->willReturn('doej');

        $this->request->expects($this->once())->method('getData')
            ->will($this->returnCallback(array($this, 'getDataMock')));


        $this->adminService->setRequest($this->request);

        $response = $this->adminService->updateAdminStatus();

        $payload = $response->getPayload();

        $this->assertEquals(App::API_OK, $response->getStatus());

    }

    public function getDataMock()
    {
        return $this->mockModel;

    }

    public function updateAdminStatusWith($subject)
    {
        $this->updateModel = $subject;
        return true;
    }

}
