<?php

namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;

use MiamiOH\RESTng\Connector\DatabaseFactory;
use MiamiOH\RESTng\Core\DB\DBH;
use MiamiOH\StateOfResidenceWebService\Services\Config;
use MiamiOH\StateOfResidenceWebService\Services\Enrollment;
use Propel\Generator\Model\Database;

class EnrollmentGetPastPendingTest extends TestCase
{


    private $enrollmentPastPending;


    private $records = [];
    private $queryString = '';
    private $queryParams = [];


    protected function setUp(): void
    {

        $this->records = [];
        $this->queryString = '';
        $this->queryParams = [];

        $this->dbh = $this->getMockBuilder(DBH::class)
            ->setMethods(array('queryall_array'))
            ->getMock();

        $db = $this->getMockBuilder(DatabaseFactory::class)
            ->setMethods(array('getHandle'))
            ->getMock();

        $this->config = $this->getMockBuilder(Config::class)
            ->setMethods(array('getConfiguration'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->enrollmentPastPending = new Enrollment();
        
        $this->enrollmentPastPending->setDatabase($db);
        $this->enrollmentPastPending->setConfig($this->config);

    }

    public function testGetPastPendingInfo()
    {

        $this->records = [
            [
                'szrsorm_pidm' => '123456',
                'szrsorm_term_code' => '201730',
            ],
        ];
        $this->dbh->expects($this->once())->method('queryall_array')
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $configuration = [
            '0' => '201720',
        ];

        $this->config->method('getConfiguration')
            ->with('CurrentTerm')
            ->willReturn($configuration);

        $model = $this->enrollmentPastPending->getPastPendingInfo();

    }


    public function queryall_arrayWithQuery($subject)
    {
        $this->queryString = $subject;
        return true;
    }

    public function queryall_arrayWithParams($subject)
    {
        $this->queryParams = $subject;
        return true;
    }

    public function queryall_arrayMock()
    {
        return $this->records;
    }

}