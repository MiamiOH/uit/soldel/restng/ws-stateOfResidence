<?php

namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\StateOfResidenceWebService\Services\Enrollment;
use MiamiOH\StateOfResidenceWebService\Services\EnrollmentService;

class EnrollmentGetPastPendingServiceTest extends TestCase
{

    private $enrollmentPastPendingService;
    private $enrollmentPastPending;
    private $mockReadResponse = [];


    protected function setUp(): void
    {
        
        $this->app = $this->getMockBuilder(App::class)
            ->setMethods(array('newResponse'))
            ->getMock();

        $this->app->method('newResponse')->willReturn(new Response());


        $this->request = $this->getMockBuilder(Request::class)
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $this->enrollmentPastPending = $this->getMockBuilder(Enrollment::class)
            ->setMethods(array('getPastPendingInfo'))
            ->getMock();

        $this->enrollmentPastPending->method('getPastPendingInfo')
            ->will($this->returnCallback(array($this, 'getPastPendingInfoMock')));
        $logger = $this->getMockBuilder('Logger')
            ->setConstructorArgs(array('logger'))
            ->setMethods(array('debug', 'info','error'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->enrollmentPastPendingService = new EnrollmentService();
        $reflection = new \ReflectionClass($this->enrollmentPastPendingService);
        $reflection_property = $reflection->getProperty('log');
        $reflection_property->setAccessible(true);
        $reflection_property->setValue($this->enrollmentPastPendingService, $logger);

        $this->enrollmentPastPendingService->setApp($this->app);
        $this->enrollmentPastPendingService->setEnrollmentService($this->enrollmentPastPending);


    }


    public function testGetPastPendingInfoREST()
    {


        $this->mockReadResponse = [
            [
                'pidm' => 123456,
                'termCode' => '201730',
            ]
        ];

        $this->enrollmentPastPendingService->setRequest($this->request);

        $response = $this->enrollmentPastPendingService->getPastPendingInfo();

        $payload = $response->getPayload();


        $this->assertEquals(App::API_OK, $response->getStatus());

        $this->assertEquals(123456, $payload[0]['pidm']);
        $this->assertEquals('201730', $payload[0]['termCode']);

    }

    public function getPastPendingInfoMock()
    {
        return $this->mockReadResponse;

    }
    
}