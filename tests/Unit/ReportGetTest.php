<?php
namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;

use MiamiOH\StateOfResidenceWebService\Services\Config;
use MiamiOH\RESTng\Connector\DatabaseFactory;
use MiamiOH\RESTng\Core\DB\DBH;
use MiamiOH\StateOfResidenceWebService\Services\Report;

class ReportGetTest extends TestCase
{

    private $report;
    private $config;

    private $sth;
    private $response;

    private $prepareQueryString = '';
    private $queryString = [];
    private $DBRecord = [];
    private $index = 0;
    private $queryIndex = 0;
    private $bindPlaceHolder = '';
    private $boundValues = [];

    protected function setUp(): void
    {
        $this->bindPlaceHolder = '';
        $this->boundValues = [];
        $this->prepareQueryString = '';
        $this->DBRecord = [];
        $this->queryString = [];
        $this->index = 0;
        $this->queryIndex = 0;

        $this->dbh = $this->getMockBuilder(DBH::class)
            ->setMethods(array('queryfirstcolumn', 'prepare'))
            ->getMock();

        $this->sth = $this->getMockBuilder('\MiamiOH\RESTng\Legacy\DB\MU_STH')
            ->setMethods(array('fetchrow_assoc', 'execute', 'bind_by_name'))
            ->getMock();

        $this->sth->method('bind_by_name')
            ->with($this->callback(array($this, 'bind_by_nameWithName')),
                $this->callback(array($this, 'bind_by_nameWithValue')))
            ->willReturn(true);

        $db = $this->getMockBuilder(DatabaseFactory::class)
            ->setMethods(array('getHandle'))
            ->getMock();

        $this->config = $this->getMockBuilder(Config::class)
            ->setMethods(array('getConfiguration'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->report = new Report();

        $this->report->setDatabase($db);
        $this->report->setConfig($this->config);


    }

    public function testGetEnrollmentReport()
    {
        $this->index = 0;
        $this->queryIndex = 0;
        $this->DBRecord = [

            [
                'pidm' => 123456,
                'szrsorm_term_code' => "201710",
                'stvterm_desc' => 'Fall Semester 2016-17',
                'szrsorm_status' => 'complete',
                'szrsorm_stat_code' => 'OH',
                'szrsorm_natn_code' => 'US',
                'szrsorm_lastseen_date' => '10-OCT-16',
                'szrsorm_priority' => '5',
                'course_subject' => 'AES',
                'course_number' => '110',
                'course_section' => 'A',
                'course_title' => 'Leadership Laboratory',
                'sfrstcr_crn' => '50289',
                'szbuniq_banner_id' => '+00123456',
                'szbuniq_unique_id' => 'DOEJ',
                'spriden_first_name' => 'Doe',
                'spriden_last_name' => 'Jane',
                'instructional_method' => 'ONL',
                'ssbsect_camp_code' => 'H',
                'stvcamp_desc' => 'Hamilton',
                'scrlevl_levl_code' => 'UG',
                'sgbstdn_program_1' => 'PRE BS NURS',
                'sgbstdn_program_2' => null,
                'sgbstdn_majr_code_1' => 'RCN4',
                'sgbstdn_majr_code_2' => null,
                'sgbstdn_majr_code_1_2' => null,
                'sgbstdn_majr_code_2_2' => null,
                'szrsorm_admin_status' => 'pending',
                'szrsorm_comment' => 'to be resolved',

            ],
            [
                'pidm' => 987654,
                'szrsorm_term_code' => "201710",
                'stvterm_desc' => 'Fall Semester 2016-17',
                'szrsorm_status' => 'pending',
                'szrsorm_stat_code' => 'TX',
                'szrsorm_natn_code' => 'US',
                'szrsorm_lastseen_date' => '10-OCT-16',
                'szrsorm_priority' => '5',
                'course_subject' => 'AES',
                'course_number' => '110',
                'course_section' => 'A',
                'course_title' => 'Leadership Laboratory',
                'sfrstcr_crn' => '50289',
                'szbuniq_banner_id' => '+09876543',
                'szbuniq_unique_id' => 'SMITHD',
                'spriden_first_name' => 'Smith',
                'spriden_last_name' => 'Bubba',
                'instructional_method' => 'HYB',
                'ssbsect_camp_code' => 'H',
                'stvcamp_desc' => 'Hamilton',
                'scrlevl_levl_code' => 'UG',
                'sgbstdn_program_1' => 'AB',
                'sgbstdn_program_2' => null,
                'sgbstdn_majr_code_1' => 'AS47',
                'sgbstdn_majr_code_2' => null,
                'sgbstdn_majr_code_1_2' => null,
                'sgbstdn_majr_code_2_2' => null,
                'szrsorm_admin_status' => 'pending',
                'szrsorm_comment' => 'to be resolved',

            ]

        ];

        $this->dbh->expects($this->once())
            ->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')))
            ->willReturn(3);

        $configuration = [
            "triggerValueBeforeTermStart" => "-10 days",
            "triggerValueBetweenEmails" => "+10 days",
            "triggerValueAfterTermStart" => "+14 days",
            "beginTerm" => "201710"
        ];

        $this->config->method('getConfiguration')
            ->with('ActionTrigger')
            ->willReturn($configuration);

        $this->dbh->expects($this->once())->method('prepare')
            ->with($this->callback(array($this, 'prepareWithQuery')))
            ->willReturn($this->sth);

        $this->sth->method('execute')
            ->willReturn(true);

        $this->sth->method('fetchrow_assoc')
            ->will($this->returnCallback(array($this, 'fetchrow_assocMock')));

        $deferredCallMgr = $this->getMockBuilder('\RESTng\Util\DeferredCallManager')
            ->setMethods(array('getCallByKey'))
            ->getMock();

        $deferredCall = $this->getMockBuilder('\RESTng\Storage\DeferredCall')
            ->setMethods(array('status'))
            ->getMock();

        $deferredCallMgr->method('getCallByKey')->willReturn($deferredCall);

        $this->report->setDeferredCallManager($deferredCallMgr);

        $deferredCall->method('status')->willReturn('c');

        $model = $this->report->getEnrollmentReport('abc', 1, 1, 2);

//       $this->assertTrue(stripos($this->queryString[0], 'select fz_get_term') !== false,
//            'Get  current term query');

        $this->assertTrue(stripos($this->queryString[0], 'select count(*)') !== false,
            'Get  number of records');

        $this->assertTrue(stripos($this->prepareQueryString,
                'from szrsord') !== false,
            'List of tables in  prepare query');

        $this->assertEquals($this->DBRecord[0]['pidm'], $model[0]['pidm']);

        $this->assertEquals(2, $this->boundValues['MAX_ROW_TO_FETCH'], 'Max row to fetch');
        $this->assertEquals(1, $this->boundValues['MIN_ROW_TO_FETCH'], 'Min row to fetch');

    }

    public function prepareWithQuery($subject)
    {
        $this->prepareQueryString = $subject;
        return true;
    }

    public function queryfirstcolumnWithQuery($subject)
    {
        $this->queryString[$this->queryIndex++] = trim($subject);
        return true;
    }

    public function fetchrow_assocMock()
    {
        if ($this->index == 2) {
            return null;
        }
        return $this->DBRecord[$this->index++];

    }

    public function bind_by_nameWithName($subject)
    {
        $this->bindPlaceHolder = $subject;
        return true;
    }

    public function bind_by_nameWithValue($subject)
    {
        $this->boundValues[$this->bindPlaceHolder] = $subject;
        $this->bindPlaceHolder = '';
        return true;
    }
}