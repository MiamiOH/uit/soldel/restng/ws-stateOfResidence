<?php

namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Exception\BadRequest;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\StateOfResidenceWebService\Services\Config;
use MiamiOH\StateOfResidenceWebService\Services\ConfigService;
use PHPUnit\Framework\Exception;

class ConfigServiceTest extends TestCase
{

    private $configService;

    private $config;

    private $mockGetConfiguration = [];

    protected function setUp(): void
    {

        $this->mockGetConfiguration = [];

        $this->app = $this->getMockBuilder(App::class)
            ->setMethods(array('newResponse'))
            ->getMock();

        $this->app->method('newResponse')->willReturn(new Response());

        $this->request = $this->getMockBuilder(Request::class)
            ->setMethods(array('getOptions'))
            ->getMock();

        $this->config = $this->getMockBuilder(Config::class)
            ->setMethods(array('getConfiguration', 'getCategoryList'))
            ->getMock();

        $logger = $this->getMockBuilder('Logger')
            ->setConstructorArgs(array('logger'))
            ->setMethods(array('debug', 'info','error'))
            ->getMock();

        $this->configService = new ConfigService();
        $reflection = new \ReflectionClass($this->configService);
        $reflection_property = $reflection->getProperty('log');
        $reflection_property->setAccessible(true);
        $reflection_property->setValue($this->configService, $logger);

        $this->configService->setApp($this->app);
        $this->configService->setConfig($this->config);

    }

    public function testGetConfigurationWithoutCategory()
    {

        $this->expectException(BadRequest::class);

        $this->request->expects($this->once())->method('getOptions')
            ->willReturn('');

        $this->configService->setRequest($this->request);

        $resp = $this->configService->getConfiguration();

    }

    public function testGetConfigurationWithInvalidCategory()
    {

        $this->expectException(BadRequest::class);

        $request = $this->getMockBuilder('\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getOptions')
            ->willReturn(['category' => 'bob']);

        $this->configService->setRequest($request);

        $resp = $this->configService->getConfiguration();

    }

    public function testGetConfigurationWithCategory()
    {

        $this->mockGetConfiguration = [
            'configItem1' => 'Config Item 1 value',
        ];

        $categoryList = [
            'uiString' => 'StudentResidence',
            'test' => 'Test',
            'SendNotification' => 'SendNotification',
        ];

        $request = $this->getMockBuilder('\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getOptions')
            ->willReturn(['category' => 'uiString']);

        $this->configService->setRequest($request);

        $this->config->method('getCategoryList')
            ->willReturn($categoryList);

        $this->config->method('getConfiguration')
            ->with('uiString')
            ->will($this->returnCallback(array($this, 'getConfigurationMock')));

        $resp = $this->configService->getConfiguration();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));
        $this->assertEquals(1, count($payload));
        $this->assertEquals($payload['configItem1'], $this->mockGetConfiguration['configItem1']);


    }

    public function testGetConfigurationWithSendNotificationCategory()
    {

        $this->mockGetConfiguration = [
            'email1.subject' => 'Please provide state of residence information',
            'email1.body' => 'Click on below link to provide state of residence info',
            'email2.subject' => 'Please provide state of residence information',
            'email2.body' => 'Click on below link to provide state of residence info',
        ];

        $categoryList = [
            'uiString' => 'StudentResidence',
            'test' => 'Test',
            'SendNotification' => 'SendNotification',
        ];

        $request = $this->getMockBuilder('\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getOptions')
            ->willReturn(['category' => 'SendNotification']);

        $this->configService->setRequest($request);

        $this->config->method('getCategoryList')
            ->willReturn($categoryList);


        $this->config->method('getConfiguration')
            ->with('SendNotification')
            ->will($this->returnCallback(array($this, 'getConfigurationMock')));

        $resp = $this->configService->getConfiguration();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));
        $this->assertEquals(4, count($payload));

        $this->assertEquals($payload['email1.subject'], $this->mockGetConfiguration['email1.subject']);

    }

    public function getConfigurationMock()
    {
        return $this->mockGetConfiguration;

    }

}
