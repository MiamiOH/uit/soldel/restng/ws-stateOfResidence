<?php
namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\StateOfResidenceWebService\Services\TermInfo;
use MiamiOH\StateOfResidenceWebService\Services\TermInfoService;

/**
 * Created by PhpStorm.
 * User: ravendpp
 * Date: 11/2/16
 * Time: 4:38 PM
 */
class TermInfoGetServiceTest extends TestCase
{
    private $termInfoService;
    private $termInfo;


    /**
     *  set up method which is automatically called by PHPUnit before every test method:
     */
    protected function setUp(): void
    {
        $this->mockModel = [];

        //set up the mock api:
        $this->app = $this->getMockBuilder(App::class)
            ->setMethods(array('newResponse'))
            ->getMock();

        $this->app->method('newResponse')->willReturn(new Response());

        $this->termInfo = $this->getMockBuilder(TermInfo::class)
            ->setMethods(array('getTermInfo'))
            ->getMock();

        $this->termInfo->method('getTermInfo')
            ->will($this->returnCallback(array($this, 'getTermInfoMock')));

        $this->request = $this->getMockBuilder(Request::class)
            ->setMethods(array('getResourceParam'))
            ->getMock();

        $logger = $this->getMockBuilder('Logger')
            ->setConstructorArgs(array('logger'))
            ->setMethods(array('debug', 'info','error'))
            ->getMock();
        //set up the service with the mocked out resources:
        $this->termInfoService = new TermInfoService();
        $reflection = new \ReflectionClass($this->termInfoService);
        $reflection_property = $reflection->getProperty('log');
        $reflection_property->setAccessible(true);
        $reflection_property->setValue($this->termInfoService, $logger);


        $this->termInfoService->setApp($this->app);
        $this->termInfoService->setTerm($this->termInfo);

    }

    public function testGetTermInfo()
    {
        $this->mockModel = [
            [
                "semesterName" => "Winter Semester",
                "abbreviation" => "Winter",
                "sessionName" => "2016-17",
                "sessionAbbreviation" => "201715",
                "termStartDate" => "07-FEB-17",
                "termEndDate" => "09-MAR-17",
                "censusDate" => " ",
            ]
        ];


        $response = $this->termInfoService->getTermInfo();

        $payload = $response->getPayload();

        $this->assertEquals(App::API_OK, $response->getStatus());
        $this->assertEquals("Winter Semester", $payload[0]['semesterName']);
        $this->assertEquals("Winter", $payload[0]['abbreviation']);

        $this->assertEquals("2016-17", $payload[0]['sessionName']);
        $this->assertEquals("201715", $payload[0]['sessionAbbreviation']);
        $this->assertEquals("07-FEB-17", $payload[0]['termStartDate']);
        $this->assertEquals("09-MAR-17", $payload[0]['termEndDate']);
        $this->assertEquals(" ", $payload[0]['censusDate']);

    }

    public function getTermInfoMock()
    {
        return $this->mockModel;

    }
}
