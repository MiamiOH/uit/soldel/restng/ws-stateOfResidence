<?php

namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Service\Extension\BannerId;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\StateOfResidenceWebService\Services\ActionService;

class ActionUpdateServiceTest extends TestCase
{
    /*************************/
    /**********Set Up*********/
    /*************************/
    private $actionService;

    private $action;
    private $bannerId;

    private $mockModel = [];
    private $updateModel = [];
    private $requestResourceParam = '';
    private $requestResourceParamMocks = [];

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp(): void
    {

        $this->mockModel = [];
        $this->updateModel = [];
        $this->requestResourceParam = '';
        $this->requestResourceParamMocks = [];

        //set up the mock api:

        $this->bannerId = $this->getMockBuilder(BannerId::class)
            ->setMethods(array('getUniqueId'))
            ->getMock();

        $bannerUtil = $this->getMockBuilder('MU\\BannerUtil')
            ->setMethods(array('getId'))
            ->getMock();

        $this->request = $this->getMockBuilder(Request::class)
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();

        $bannerUtil->method('getId')->willReturn($this->bannerId);

        $this->action = $this->getMockBuilder('MiamiOH\RESTng\Services\StateOfResidence\Action')
            ->setMethods(array('updateTrackAction', 'updateCancelAction', 'updateCompleteAction'))
            ->getMock();

        $this->action->method('updateCancelAction')
            ->with($this->callback(array($this, 'updateCancelActionWith')))
            ->willReturn(true);

        $this->action->method('updateTrackAction')
            ->with($this->callback(array($this, 'updateTrackActionWith')))
            ->willReturn(true);

        $this->action->method('updateCompleteAction')
            ->with($this->callback(array($this, 'updateCompleteActionWith')))
            ->willReturn(true);

        $this->request->expects($this->once())->method('getData')
            ->will($this->returnCallback(array($this, 'getDataMock')));

        //set up the service with the mocked out resources:
        $logger = $this->getMockBuilder('Logger')
            ->setConstructorArgs(array('logger'))
            ->setMethods(array('debug', 'info','error'))
            ->getMock();
        $this->actionService = new ActionService();
        $reflection = new \ReflectionClass($this->actionService);
        $reflection_property = $reflection->getProperty('log');
        $reflection_property->setAccessible(true);

        $reflection_property->setValue($this->actionService, $logger);
        $this->actionService->setAction($this->action);
        $this->actionService->setBannerUtil($bannerUtil);


    }

    /*************************/
    /**********Tests**********/
    /*************************/

    public function testUpdateTrackActionCollection()
    {
        $this->mockModel = [
            [
                'pidm' => 123456,
                'termCode' => '201710',
                'nextAction' => '',
                'status' => ''
            ]
        ];

        $this->bannerId->method('getUniqueId')->willReturn('not required for this action');

        $this->request->expects($this->once())->method('getResourceParam')
            ->willReturn("track");

        $this->actionService->setRequest($this->request);

        $response = $this->actionService->updateActionCollection();

        $payload = $response->getPayload();


        $this->assertEquals(App::API_OK, $response->getStatus());
        $this->assertEquals(App::API_OK, $payload[0]['status']);

    }


    public function testUpdateTrackActionCollectionBadRequest()
    {
        $this->mockModel = [
            [
                'pidm' => 345677,
                'nextAction' => '',
                'status' => ''
            ]
        ];

        $this->bannerId->method('getUniqueId')->willReturn('not required for this action');

        $this->request->expects($this->once())->method('getResourceParam')
            ->willReturn("track");

        $this->actionService->setRequest($this->request);

        $response = $this->actionService->updateActionCollection();

        $payload = $response->getPayload();

        $this->assertEquals(App::API_OK, $response->getStatus());
        $this->assertEquals(App::API_BADREQUEST, $payload[0]['status']);
    }

    public function testUpdateCancelActionCollection()
    {
        $this->mockModel = [
            [
                'pidm' => 123456,
                'termCode' => '201710',
                'nextAction' => '',
                'status' => ''
            ],
            [
                'pidm' => 987654,
                'termCode' => '201710',
                'nextAction' => '',
                'status' => ''
            ]
        ];

        $this->bannerId->method('getUniqueId')->willReturn('not required for this action');

        $this->request->expects($this->once())->method('getResourceParam')
            ->willReturn("cancel");

        $this->actionService->setRequest($this->request);

        $response = $this->actionService->updateActionCollection();

        $payload = $response->getPayload();

        $this->assertEquals(App::API_OK, $response->getStatus());
        $this->assertEquals(App::API_OK, $payload[0]['status']);
        $this->assertEquals(App::API_OK, $payload[1]['status']);

    }

    public function testUpdateActionCollection()
    {
        $this->mockModel = [
            [
                'pidm' => 123456,
                'termCode' => '201710',
                'nextAction' => 'cancel',
                'status' => ''
            ],
            [
                'pidm' => 987654,
                'termCode' => '201710',
                'nextAction' => 'complete',
                'status' => ''
            ]
        ];

        $this->bannerId->method('getUniqueId')->willReturn('not required for this action');

        $this->actionService->setRequest($this->request);

//        $this->action->method('updateCompleteAction')
//            ->with(987654,"smithd","201710")
//            ->willReturn(true);

        $response = $this->actionService->updateAction();

        $payload = $response->getPayload();

        $this->assertEquals(App::API_OK, $response->getStatus());
        $this->assertEquals(App::API_OK, $payload[0]['status']);

        $this->assertEquals(App::API_OK, $payload[1]['status']);


    }

    public function testUpdateEmailAction()
    {
        $this->mockModel = [
            [
                'pidm' => 123456,
                'termCode' => '201710',
                'nextAction' => '',
                'status' => ''
            ],
            [
                'pidm' => 987654,
                'termCode' => '201710',
                'nextAction' => '',
                'status' => ''
            ]
        ];

        $uniqueIds = ['doej', 'smithd'];
        $i = 0;

        $this->bannerId->method('getUniqueId')->willReturn($uniqueIds[$i++]);

        $this->actionService->setRequest($this->request);

        $response = $this->actionService->updateAction();

        $payload = $response->getPayload();

        $this->assertEquals(App::API_OK, $response->getStatus());
        $this->assertEquals(App::API_OK, $payload[0]['status']);
        $this->assertEquals(App::API_OK, $payload[1]['status']);
    }

    public function updateTrackActionWith($subject)
    {
        $this->updateModel = $subject;
        return true;
    }

    public function updateCancelActionWith($subject)
    {
        $this->updateModel = $subject;
        return true;
    }

    public function updateCompleteActionWith($subject)
    {
        $this->updateModel = $subject;
        return true;
    }

    public function getDataMock()
    {
        return $this->mockModel;
    }

    public function getConfigurationWithApplication($subject)
    {
        $this->calledApplicationName = $subject;
        return true;
    }

    public function getConfigurationWithCategory($subject)
    {
        $this->calledCategoryName = $subject;
        return true;
    }

    public function getConfigurationMock()
    {
        return $this->mockConfigurationItems;
    }

}
