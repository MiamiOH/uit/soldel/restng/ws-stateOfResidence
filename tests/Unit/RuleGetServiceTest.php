<?php
namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\StateOfResidenceWebService\Services\Rule;
use MiamiOH\StateOfResidenceWebService\Services\RuleService;

/**
 * Created by PhpStorm.
 * User: ravendpp
 * Date: 11/22/16
 * Time: 2:18 PM
 */
class RuleGetServiceTest extends TestCase
{
    private $ruleService;

    private $rule;

    private $mockModel = [];

    private $requestResourceParam = '';

    private $requestResourceParamMocks = [];

    /**
     * set up method which is automatically called by PHPUnit before every test method:
     */
    protected function setUp(): void
    {

        $this->mockModel = [];
        $this->requestResourceParam = '';
        $this->requestResourceParamMocks = [];

        //set up the mock api:
        $this->app = $this->getMockBuilder(App::class)
            ->setMethods(array('newResponse'))
            ->getMock();

        $this->app->method('newResponse')->willReturn(new Response());

        $this->rule = $this->getMockBuilder(Rule::class)
            ->setMethods(array('getRuleList'))
            ->getMock();

        $this->rule->method('getRuleList')
            ->will($this->returnCallback(array($this, 'getRuleListMock')));

        $this->request = $this->getMockBuilder(Request::class)
            ->setMethods(array('getResourceParam'))
            ->getMock();

        $logger = $this->getMockBuilder('Logger')
            ->setConstructorArgs(array('logger'))
            ->setMethods(array('debug', 'info','error'))
            ->getMock();
        //set up the service with the mocked out resources:
        $this->ruleService = new RuleService();
        $reflection = new \ReflectionClass($this->ruleService);
        $reflection_property = $reflection->getProperty('log');
        $reflection_property->setAccessible(true);
        $reflection_property->setValue($this->ruleService, $logger);

        $this->ruleService->setApp($this->app);
        $this->ruleService->setRule($this->rule);

    }

    public function testGetRuleListREST()
    {
        $this->mockModel = [
            [
                'ruleType' => 'student',
                'attribute' => 'program',
                'allowedStates'=> 'OH',
                'ruleValue' => 'BS NURSING'
            ],
            [
                'ruleType' => 'student',
                'attribute' => 'program/major',
                'allowedStates'=> 'OH|NY|CA',
                'ruleValue' => 'M ED|EASL'

            ],
        ];

        $this->ruleService->setRequest($this->request);

        $response = $this->ruleService->getRuleList();

        $payload = $response->getPayload();

        $this->assertEquals(App::API_OK, $response->getStatus());
        $this->assertEquals("student", $payload[0]['ruleType']);
        $this->assertEquals("program", $payload[0]['attribute']);
        $this->assertEquals("OH", $payload[0]['allowedStates']);
        $this->assertEquals("BS NURSING", $payload[0]['ruleValue']);

    }

    public function getRuleListMock()
    {
        return $this->mockModel;

    }
}
