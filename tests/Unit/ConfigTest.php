<?php

namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;

use MiamiOH\RESTng\Util\Configuration;
use MiamiOH\StateOfResidenceWebService\Services\Config;

class ConfigTest extends TestCase
{

    private $config;
    private $configuration;

    private $calledApplicationName = '';
    private $calledCategoryName = '';
    private $mockConfigurationItems = [];

    protected function setUp(): void
    {

        $this->calledApplicationName = '';
        $this->calledCategoryName = '';
        $this->mockConfigurationItems = [];

        $this->configuration = $this->getMockBuilder(Configuration::class)
            ->setMethods(array('getConfiguration'))
            ->getMock();

        $this->configuration->method('getConfiguration')
            ->with($this->callback(array($this, 'getConfigurationWithApplication')),
                $this->callback(array($this, 'getConfigurationWithCategory')))
            ->will($this->returnCallback(array($this, 'getConfigurationMock')));

        $this->config = new Config();

        $this->config->setConfiguration($this->configuration);

    }

    public function testGetConfigurationWithCategory()
    {

        $this->mockConfigurationItems = [
            'configItem1' => 'Config Item 1 value',
        ];

        $payload = $this->config->getConfiguration('uiString');

        $this->assertTrue(is_array($payload));
        $this->assertEquals(1, count($payload));

        $this->assertEquals('StateOfResidence', $this->calledApplicationName);

    }

    public function testGetConfigurationWithSendNotificationCategory()
    {

        $this->mockConfigurationItems = [
            'configItem1' => 'Config Item 1 value',
        ];

        $payload = $this->config->getConfiguration('uiString');

        $this->assertTrue(is_array($payload));
        $this->assertEquals(1, count($payload));

        $this->assertEquals('StateOfResidence', $this->calledApplicationName);

    }

    public function getConfigurationWithApplication($subject)
    {
        $this->calledApplicationName = $subject;
        return true;
    }

    public function getConfigurationWithCategory($subject)
    {
        $this->calledCategoryName = $subject;
        return true;
    }

    public function getConfigurationMock()
    {
        return $this->mockConfigurationItems;
    }

}
