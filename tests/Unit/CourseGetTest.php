<?php

namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;

use MiamiOH\RESTng\Connector\DatabaseFactory;
use MiamiOH\RESTng\Core\DB\DBH;
use MiamiOH\RESTng\Legacy\DB\STH;
use MiamiOH\StateOfResidenceWebService\Services\Config;
use MiamiOH\StateOfResidenceWebService\Services\Course;
use Propel\Generator\Model\Database;

class CourseGetTest extends TestCase
{

    private $course;
    private $config;

    private $sth;

    private $prepareQueryString = '';
    private $queryString = '';
    private $DBRecord = [];
    private $index = 0;

    protected function setUp(): void
    {

        $this->prepareQueryString = '';
        $this->DBRecord = [];
        $this->queryString = '';
        $this->index = 0;

        $this->dbh = $this->getMockBuilder(DBH::class)
            ->setMethods(array('queryall_array'))
            ->getMock();

        $db = $this->getMockBuilder(DatabaseFactory::class)
            ->setMethods(array('getHandle'))
            ->getMock();

        $this->config = $this->getMockBuilder(Config::class)
            ->getMock();

        $this->config->method("getConfiguration")->will(
            $this->returnValueMap(
                [
                    [ 'CodeMap', ['courseTypeDesc' => '{ "ONLS": "Online Synchronous", "ONLA": "Online Asynchronous", "HYBS": "Hybrid Synchronous", "HYBA": "Hybrid Asynchronous" }'] ]
                ]
            )
        );

        $db->method('getHandle')->willReturn($this->dbh);

        $this->course = new Course();

        $this->course->setDatabase($db);
        $this->course->setConfig($this->config);


    }

    public function testGetCourseList()
    {
        $this->index = 0;
        $this->DBRecord = [

            [
                'scbcrse_title' => 'Leadership Laboratory',
                'scbcrse_subj_code' => 'AES',
                'scbcrse_crse_numb' => 110,
                'ssbsect_camp_code' => 'H',
                'stvcamp_desc' => 'Hamilton',
                'ssbsect_term_code' => 201715,
                'ssbsect_crn' => 50289,
                'instructional_method' => 'HYBA',
                'course_section' => 'B',
                'scrlevl_levl_code' => 'UG',
            ],
            [
                'scbcrse_title' => 'Intro to Management&Leadership',
                'scbcrse_subj_code' => 'MGT',
                'scbcrse_crse_numb' => 291,
                'ssbsect_camp_code' => 'H',
                'stvcamp_desc' => 'Hamilton',
                'ssbsect_term_code' => 201710,
                'ssbsect_crn' => 40148,
                'instructional_method' => 'ONLA',
                'course_section' => 'A',
                'scrlevl_levl_code' => 'UG',
            ]

        ];
        $configuration = [
            "triggerValueBeforeTermStart" => "-10 days",
            "triggerValueBetweenEmails" => "+10 days",
            "triggerValueAfterTermStart" => "+14 days",
            "beginTerm" => "201710"
        ];

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')))
            ->willReturn($this->DBRecord);

        $model = $this->course->getCourseList();

        $this->assertTrue(stripos($this->queryString, 'select scbcrse_title,scbcrse_subj_code,scbcrse_crse_numb,
                        ssbsect_camp_code,stvcamp_desc,scrlevl_levl_code,ssbsect_term_code,ssbsect_crn,
                        ssrattr_attr_code as instructional_method,
                        trim(ssbsect_seq_numb) as course_section
                        from stvcamp,ssbsect,ssrattr,scbcrse,scrlevl') !== false,
            'Get Course collection query');

        $this->assertEquals($this->DBRecord[0]['ssbsect_term_code'], $model[0]['termCode']);
        $this->assertEquals($this->DBRecord[1]['ssbsect_term_code'], $model[1]['termCode']);

    }

    public function queryall_arrayWithQuery($subject)
    {
        $this->queryString = $subject;
        return true;
    }

}