<?php

namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Service\Extension\BannerId;
use MiamiOH\RESTng\Service\Extension\BannerUtil;
use MiamiOH\StateOfResidenceWebService\Services\TermStatus;
use MiamiOH\StateOfResidenceWebService\Services\TermStatusService;
use Slim\Http\Response;

class TermStatusGetServiceTest extends TestCase
{
    private $termStatusService;
    private $termStatus;

    private $bannerId;


    /**
     *  set up method which is automatically called by PHPUnit before every test method:
     */
    protected function setUp(): void
    {
        $this->mockModel = [];

        //set up the mock api:
        $this->app = $this->getMockBuilder(App::class)
            ->setMethods(array('newResponse'))
            ->getMock();

        $this->app->method('newResponse')->willReturn(new Response());

        $this->termStatus = $this->getMockBuilder(TermStatus::class)
            ->setMethods(array('getTermStatus'))
            ->getMock();

        $this->termStatus->method('getTermStatus')
            ->will($this->returnCallback(array($this, 'getTermStatusMock')));

        $this->request = $this->getMockBuilder('\RESTng\Util\Request')
            ->setMethods(array('getOptions', 'getResourceParam', 'getResourceParamKey'))
            ->getMock();

        $this->bannerId = $this->getMockBuilder(BannerId::class)
            ->setMethods(array('getPidm'))
            ->getMock();

        $bannerUtil = $this->getMockBuilder(BannerUtil::class)
            ->setMethods(array('getId'))
            ->getMock();

        $bannerUtil->method('getId')->willReturn($this->bannerId);

        $logger = $this->getMockBuilder('Logger')
            ->setConstructorArgs(array('logger'))
            ->setMethods(array('debug', 'info','error'))
            ->getMock();
        //set up the service with the mocked out resources:
        $this->termStatusService = new TermStatusService();
        $reflection = new \ReflectionClass($this->termStatusService);
        $reflection_property = $reflection->getProperty('log');
        $reflection_property->setAccessible(true);
        $reflection_property->setValue($this->termStatusService, $logger);

        $this->termStatusService->setApp($this->app);
        $this->termStatusService->setTermStatus($this->termStatus);
        $this->termStatusService->setBannerUtil($bannerUtil);

    }

    public function testGetTermStatus()
    {
        $options = [
            'termCode' => '201710',
            'country' => 'US',
            'state' => 'OH',
        ];

        $this->mockModel = [
            'status' => true,
            'priority' => 5,
        ];

        $this->request->expects($this->once())->method('getOptions')
            ->willReturn($options);

        $this->request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('muid'))->willReturn('doej');

        $this->termStatusService->setRequest($this->request);

        $this->bannerId->method('getPidm')->willReturn('123456');

        $response = $this->termStatusService->getTermStatus();

        $payload = $response->getPayload();

        $this->assertEquals(App::API_OK, $response->getStatus());

        $this->assertEquals(true, $payload['status']);

        $this->assertEquals(5, $payload['status']);


    }

    public function getTermStatusMock()
    {
        return $this->mockModel;

    }
}
