<?php

namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;


use MiamiOH\RESTng\Connector\DatabaseFactory;
use MiamiOH\RESTng\Core\DB\DBH;
use MiamiOH\StateOfResidenceWebService\Services\AdminStatus;

class AdminPutTest extends TestCase
{
    private $admin;


    private $data = [];
    private $queryStringPerform = '';
    private $queryParams = [];

    protected function setUp(): void
    {

        $this->data = [];
        $this->recordIndex = 0;
        $this->queryStringPerform = '';
        $this->queryParams = [];


        $db = $this->getMockBuilder(DatabaseFactory::class)
            ->setMethods(array('getHandle'))
            ->getMock();

        $this->dbh = $this->getMockBuilder(DBH::class)
            ->setMethods(array('perform'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->admin = new AdminStatus();

        $this->admin->setDatabase($db);

    }

    public function testUpdateAdminStatusInfo()
    {

        $this->data = [
                    'pidm' => '123456',
                    'termCode' => '201710',
                    'adminStatus'=> 'resolved',
                    'comment' => 'Admin',
                    'priority'=>'5',
        ];

        $this->dbh->method('perform')
            ->with($this->callback(array($this, 'performWithQuery')),
                $this->callback(array($this, 'performWithParams')))
            ->willReturn(true);

        $this->admin->updateAdminStatus(123456,$this->data);

        $this->assertTrue(stripos($this->queryStringPerform, 'update szrsorm set szrsorm_admin_status = "completed",') !== true,
            'Query from szrsorm');

    }
    public function performWithQuery($subject)
    {
        $this->queryStringPerform = $subject;
        return true;
    }

    public function performWithParams($subject)
    {
        $this->queryParams = $subject;
        return true;
    }

}
