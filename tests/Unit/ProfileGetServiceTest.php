<?php
namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Service\Extension\BannerId;
use MiamiOH\RESTng\Service\Extension\BannerUtil;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\StateOfResidenceWebService\Services\Profile;
use MiamiOH\StateOfResidenceWebService\Services\ProfileService;

class ProfileGetServiceTest extends TestCase
{

    private $profileService;
    private $bannerId;


    private $mockModel = [];
    private $requestResourceParam = '';
    private $requestResourceParamMocks = [];

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp(): void
    {

        $this->mockModel = [];
        $this->requestResourceParam = '';
        $this->requestResourceParamMocks = [];

        //set up the mock api:
        $this->app = $this->getMockBuilder(App::class)
            ->setMethods(array('newResponse'))
            ->getMock();

        $this->app->method('newResponse')->willReturn(new Response());

        $this->bannerId = $this->getMockBuilder(BannerId::class)
            ->setMethods(array('getPidm'))
            ->getMock();

        $bannerUtil = $this->getMockBuilder(BannerUtil::class)
            ->setMethods(array('getId'))
            ->getMock();

        $bannerUtil->method('getId')->willReturn($this->bannerId);


        $this->profile = $this->getMockBuilder(Profile::class)
            ->setMethods(array('getProfile'))
            ->getMock();

        $this->profile->method('getProfile')
            ->will($this->returnCallback(array($this, 'getProfileMock')));

        $this->request = $this->getMockBuilder('\RESTng\Util\Request')
            ->setMethods(array('getResourceParam','getResourceParamKey'))
            ->getMock();
        $logger = $this->getMockBuilder('Logger')
            ->setConstructorArgs(array('logger'))
            ->setMethods(array('debug', 'info','error'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->profileService = new ProfileService();
        $reflection = new \ReflectionClass($this->profileService);
        $reflection_property = $reflection->getProperty('log');
        $reflection_property->setAccessible(true);
        $reflection_property->setValue($this->profileService, $logger);

        $this->profileService->setApp($this->app);
        $this->profileService->setProfile($this->profile);
        $this->profileService->setBannerUtil($bannerUtil);

    }

    /*************************/
    /**********Tests**********/
    /*************************/

    public function testGetProfile()
    {
        $this->mockModel = [
            'pidm' => 123456,
            'uniqueId' => 'doej',
            'bannerId' => '+00123456',
            'firstName' => 'John',
            'lastName' => 'doe',
            'term' => [
                [
                    'pidm' => 123456,
                    'termCode' => '201710',
                    'termDesc' => 'Fall Semester 2016-17',
                    'termStartDate' => '04-OCT-16',
                    'termEndDate' => '08-JAN-17',
                    'status' => 'complete',
                    'country' => 'US',
                    'state' => 'TX',
                    'lastSeenDate' => '10-OCT-16',
                    'courseList' => [
                        [
                            'subjectCode' => 'AES',
                            'courseNumber' => '110',
                            'courseSection' => 'A',
                            'campusDesc' => 'Hamilton',
                            'courseTitle' => 'Leadership Laboratory',

                        ]
                    ]
                ]
            ]
        ];

        $this->bannerId->method('getPidm')->willReturn('123456');

        $this->request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('muid'))->willReturn('doej');

        $this->request->expects($this->once())->method('getResourceParamKey')
            ->with($this->equalTo('muid'))->willReturn('uniqueId');

        $this->profileService->setRequest($this->request);

        $response = $this->profileService->getProfile();

        $payload = $response->getPayload();

        $this->assertEquals(App::API_OK, $response->getStatus());
        $this->assertEquals(123456, $payload['pidm']);
        $this->assertEquals("doej", $payload['uniqueId']);
        $this->assertEquals("+00123456", $payload['bannerId']);
        $this->assertEquals("John", $payload['firstName']);
        $this->assertEquals("doe", $payload['lastName']);


    }

    public function getProfileMock()
    {
        return $this->mockModel;

    }

}
