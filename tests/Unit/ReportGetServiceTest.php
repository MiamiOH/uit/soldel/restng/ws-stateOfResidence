<?php
namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;

use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\App;
use MiamiOH\StateOfResidenceWebService\Services\Report;
use MiamiOH\StateOfResidenceWebService\Services\ReportService;

class ReportGetServiceTest extends TestCase
{

    private $reportService;
    private $report;

    private $response;

    private $mockModel = [];
    private $requestResourceParam = '';
    private $requestResourceParamMocks = [];

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp(): void
    {

        $this->mockModel = [];
        $this->requestResourceParam = '';
        $this->requestResourceParamMocks = [];

        //set up the mock api:
        $this->app = $this->getMockBuilder(App::class)
            ->setMethods(array('newResponse'))
            ->getMock();

        $this->response = $this->app->method('newResponse')->willReturn(new Response());

        $this->report = $this->getMockBuilder(Report::class)
            ->setMethods(array('getEnrollmentReport', 'getTotalRecords'))
            ->getMock();

        $this->report->method('getEnrollmentReport')
            ->will($this->returnCallback(array($this, 'getEnrollmentReportMock')));

        $this->request = $this->getMockBuilder(Request::class)
            ->setMethods(array('getOptions', 'isPaged', 'getOffset', 'getLimit'))
            ->getMock();


        //set up the service with the mocked out resources:
        $this->reportService = new ReportService();

        $this->reportService->setApp($this->app);
        $this->reportService->setReport($this->report);

    }

    /*************************/
    /**********Tests**********/
    /*************************/

    public function testGetEnrollmentReportREST()
    {
        $this->mockModel = [
            [
                'pidm' => 987654,
                'termCode' => '201710',
                'status' => 'pending',
                'country' => 'US',
                'state' => 'TX',
                'lastSeenDate' => '10-OCT-16',
                'termDesc' => 'FallSemester 2016-17',
                'courseList' => [
                    [
                        'subjectCode' => 'AES',
                        'courseNumber' => '110',
                        'courseSection' => 'A',
                        'campusDesc' => 'Hamilton',
                        'courseTitle' => 'Leadership Laboratory',
                    ]
                ]
            ],
            [
                'pidm' => 123456,
                'termCode' => '201710',
                'status' => 'complete',
                'country' => 'US',
                'state' => 'OH',
                'lastSeenDate' => '10-OCT-16',
                'termDesc' => 'FallSemester 2016-17',
                'courseList' => [
                    [
                        'subjectCode' => 'AES',
                        'courseNumber' => '110',
                        'courseSection' => 'A',
                        'campusDesc' => 'Hamilton',
                        'courseTitle' => 'Leadership Laboratory',
                    ],
                    [
                        'subjectCode' => 'ACC',
                        'courseNumber' => '221',
                        'courseSection' => 'A',
                        'campusDesc' => 'Hamilton',
                        'courseTitle' => 'Intro To Financial Accounting',
                    ],
                ]
            ],

        ];
        $this->request->method('getOptions')
            ->willReturn([ 'key' => 'abc' ]);

        $this->request->method('isPaged')
            ->willReturn(1);

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(2);

        $this->reportService->setRequest($this->request);

        $response = $this->reportService->getEnrollmentReport();

        $this->report->method('getTotalRecords')
            ->willReturn(3);

        $payload = $response->getPayload();

        $this->assertEquals(App::API_OK, $response->getStatus());
        $this->assertEquals(123456, $payload[1]['pidm']);
        $this->assertEquals("201710", $payload[1]['termCode']);
        $this->assertEquals("OH", $payload[1]['state']);
        $this->assertEquals("US", $payload[1]['country']);
        $this->assertEquals("complete", $payload[1]['status']);


    }

    public function getEnrollmentReportMock()
    {
        return $this->mockModel;

    }

}
