<?php
/**
 * Created by PhpStorm.
 * User: ravendpp
 * Date: 11/3/16
 * Time: 9:36 AM
 */

namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;


use MiamiOH\RESTng\Connector\DatabaseFactory;
use MiamiOH\RESTng\Core\DB\DBH;
use MiamiOH\StateOfResidenceWebService\Services\Config;
use MiamiOH\StateOfResidenceWebService\Services\TermInfo;

class TermInfoGetTest extends TestCase
{

    private $termInfo;
    private $config;


    protected function setUp(): void
    {

        $this->mockModel = [
            [
                "stvterm_code" => "201715",
                "stvterm_desc" => "Winter Semester 2016-17",
                "stvterm_start_date" => "08-FEB-17",
                "stvterm_end_date" => "10-MAR-17",
            ]
        ];

        $this->dbh = $this->getMockBuilder(DBH::class)
            ->setMethods(array('queryall_array'))
            ->getMock();

        $db = $this->getMockBuilder(DatabaseFactory::class)
            ->setMethods(array('getHandle'))
            ->getMock();

        $this->config = $this->getMockBuilder(Config::class)
            ->setMethods(array('getConfiguration'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->termInfo = new TermInfo();

        $this->termInfo->setDatabase($db);

        $this->termInfo->setConfig($this->config);


    }

    public function testGetTermInfo()
    {

        $configuration = [
            "triggerValueBeforeTermStart" => "-10 days",
            "triggerValueBetweenEmails" => "+10 days",
            "triggerValueAfterTermStart" => "+14 days",
            "beginTerm" => "201710"
        ];

        $this->config->method('getConfiguration')
            ->with('ActionTrigger')
            ->willReturn($configuration);

        $this->dbh->expects($this->once())->method('queryall_array')
            ->will($this->returnCallback(array($this, 'queryall_arrayMockTermInfo')));

        $model = $this->termInfo->getTermInfo();

    }

    public function queryall_arrayMockTermInfo()
    {
        return $this->mockModel;

    }
}
