<?php
namespace MiamiOH\StateOfResidenceWebService\Tests\Unit;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\StateOfResidenceWebService\Services\Rule;
use MiamiOH\StateOfResidenceWebService\Services\RuleService;

class RuleDeleteServiceTest extends TestCase
{
    private $ruleService;

    private $rule;

    private $mockModel = [];

    private $requestResourceParam = '';

    private $requestResourceParamMocks = [];

    /**
     * set up method which is automatically called by PHPUnit before every test method:
     */
    protected function setUp(): void
    {

        $this->mockModel = [];
        $this->requestResourceParam = '';
        $this->requestResourceParamMocks = [];

        //set up the mock api:
        $this->app = $this->getMockBuilder(App::class)
            ->setMethods(array('newResponse'))
            ->getMock();

        $this->app->method('newResponse')->willReturn(new Response());

        $this->rule = $this->getMockBuilder(Rule::class)
            ->setMethods(array('deleteRule'))
            ->getMock();

        $this->request = $this->getMockBuilder(Request::class)
            ->setMethods(array('getData'))
            ->getMock();
        $logger = $this->getMockBuilder('Logger')
            ->setConstructorArgs(array('logger'))
            ->setMethods(array('debug', 'info','error'))
            ->getMock();
        //set up the service with the mocked out resources:
        $this->ruleService = new RuleService();
        $reflection = new \ReflectionClass($this->ruleService);
        $reflection_property = $reflection->getProperty('log');
        $reflection_property->setAccessible(true);
        $reflection_property->setValue($this->ruleService, $logger);

        $this->ruleService->setApp($this->app);
        $this->ruleService->setRule($this->rule);

    }

    public function testDeleteRuleList()
    {
        $data = [
            [
                'ruleID' => 2,
                'ruleType' => 'student',
                'attribute' => 'program',
                'ruleValue' => 'BS NURSING',
            ],
        ];
        $this->ruleService->setRequest($this->request);
        $this->request->expects($this->once())->method('getData')
            ->willReturn($data);

        $this->rule->method('deleteRule')
            ->with(2)
            ->willReturn(true);

        $response = $this->ruleService->deleteRuleList();

        $payload = $response->getPayload();

        $this->assertEquals(App::API_OK, $response->getStatus());
        $this->assertEquals(App::API_OK, $payload[0]['status']);
       
    }

}
