<?php

class TestAutoLoader
{
    protected static $paths = array();

    public static function addPath($path)
    {
        $path = realpath($path);
        if ($path) {
            self::$paths[] = $path;
        }
    }

    public static function load($className)
    {
        $namespace = str_replace("\\", DIRECTORY_SEPARATOR, __NAMESPACE__);
        $className = str_replace("\\", DIRECTORY_SEPARATOR, $className);

        $classPath = (empty($namespace) ? "" : $namespace . DIRECTORY_SEPARATOR) . "{$className}.class.php";

        if (strpos($classPath, 'RESTng/Service/StateOfResidence') !== false) {
            $classPath = str_replace('RESTng/Service/StateOfResidence', 'Service', $classPath);
        }

        foreach (self::$paths as $path) {
            if (is_file($path . DIRECTORY_SEPARATOR . $classPath)) {
                require_once $path . DIRECTORY_SEPARATOR . $classPath;
                return;
            }
        }
    }
}

