<?php

Logger::configure(array(
    'rootLogger' => array(
        'appenders' => array('apiLogFile'),
        'level' => 'warn',
    ),

    'loggers' => array(
        'api.bootstrap' => array(
            'appenders' => array('apiLogFile'),
            'level' => 'info',
        ),

        'api.resource' => array(
            'appenders' => array('apiLogFile'),
            'level' => 'info',
        ),

        'api.middleware' => array(
            'appenders' => array('apiLogFile'),
            'level' => 'info',
        ),

        'RESTng\Slim' => array(
            'appenders' => array('apiLogFile'),
            'level' => 'info',
        ),

        'RESTng\Util\User' => array(
            'appenders' => array('apiLogFile'),
            'level' => 'info',
        ),

        'RESTng\Core\API\Resource' => array(
            'appenders' => array('apiLogFile'),
            'level' => 'info',
        ),

        'RESTng\Core\Authorization\Authorization' => array(
            'appenders' => array('apiLogFile'),
            'level' => 'info',
        ),

        'RESTng\Service\Course\Section' => array(
            'appenders' => array('apiLogFile'),
            'level' => 'info',
        ),

    ),

    'appenders' => array(
        'consoleLog' => array(
            'class' => 'LoggerAppenderConsole',
            'layout' => array(
                'class' => 'LoggerLayoutSimple',
            ),
        ),
        'apiLogFile' => array(
            'class' => 'LoggerAppenderFile',
            'layout' => array(
                'class' => 'LoggerLayoutPattern',
                'params' => array(
                    'conversionPattern' => '%level %date [%logger] %message%newline%ex'
                )
            ),
            'params' => array(
                'file' => 'phpunit_api.log'
            )
        ),
    )
));
